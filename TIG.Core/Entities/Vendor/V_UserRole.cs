﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Entities.Vendor
{
    public class V_UserRole
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public virtual V_Users User { get; set; }
        public virtual V_Role Role { get; set; }

    }
}
