﻿using Microsoft.EntityFrameworkCore;

namespace TIG.Core.Entities.Vendor
{
    public partial class V_Context : DbContext
    {
        public V_Context(DbContextOptions<V_Context> options)
              : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<V_Permission>(e =>
            {
                e.ToTable("permission")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<V_Role>(e =>
            {
                e.ToTable("role")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();

                e.HasMany(x => x.Role_Permissions)
                .WithOne(x => x.Role)
                .HasForeignKey(j => j.RoleId);

                e.HasMany(j => j.UserRoles)
                .WithOne(j => j.Role)
                .HasForeignKey(j => j.RoleId);
            });

            modelBuilder.Entity<V_Role_Permission>(e =>
            {
                e.ToTable("role_permission")
                .HasKey(x => new { x.RoleId, x.PermissionId });

                e.HasOne<V_Role>(sc => sc.Role)
                .WithMany(s => s.Role_Permissions)
                .HasForeignKey(sc => sc.RoleId);

                e.HasOne<V_Permission>(sc => sc.Permission)
                .WithMany(s => s.Role_Permissions)
                .HasForeignKey(sc => sc.PermissionId);
            });

            modelBuilder.Entity<V_UserRole>(e =>
            {
                e.ToTable("user_role")
                .HasKey(x => new { x.UserId, x.RoleId });

                e.HasOne<V_Users>(sc => sc.User)
                .WithMany(s => s.UserRoles)
                .HasForeignKey(sc => sc.UserId);

                e.HasOne<V_Role>(sc => sc.Role)
                .WithMany(s => s.UserRoles)
                .HasForeignKey(sc => sc.RoleId);
            });

            modelBuilder.Entity<V_Users>(e =>
            {
                e.ToTable("users")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();

               
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);


        public virtual DbSet<V_Permission> Permission { get; set; }
        public virtual DbSet<V_Role> Role { get; set; }
        public virtual DbSet<V_Role_Permission> Role_Permission { get; set; }
        public virtual DbSet<V_UserRole> UserRole { get; set; }
        public virtual DbSet<V_Users> User { get; set; }
    }
}
