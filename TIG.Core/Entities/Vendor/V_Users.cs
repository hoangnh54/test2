﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TIG.Core.Entities.Vendor
{
    public class V_Users
    {
        [Column("id")]
        public int Id { get; set; }
        [Column("username")]
        public string UserName { get; set; }
        [Column("password")]
        public string Password { get; set; }
        //[Column("PasswordFormat")]
        //public int PasswordFormat { get; set; }
        [Column("PasswordSalt")]
        public string PasswordSalt { get; set; }
        [Column("FullName")]
        public string FullName { get; set; }
        [Column("Email")]
        public string Email { get; set; }
        //[Column("GroupName")]
        //public string GroupName { get; set; }
        //[Column("IsApproved")]
        //public bool IsApproved { get; set; }
        public virtual IList<V_UserRole> UserRoles { get; set; }
    }
}
