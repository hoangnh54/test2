﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TIG.Core.Entities.Vendor
{
    public class V_Role_Permission
    {
        [Column("roleid")]
        public int RoleId { get; set; }
        [Column("permissionid")]
        public int PermissionId { get; set; }
        public virtual V_Role Role { get; set; }
        public virtual V_Permission Permission { get; set; }
    }
}
