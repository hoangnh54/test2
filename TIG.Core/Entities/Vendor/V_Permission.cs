﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TIG.Core.Entities.Vendor
{
    public class V_Permission
    {
        [Column("permissionid")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        public string Description { get; set; }
        public virtual IList<V_Role_Permission> Role_Permissions { get; set; }
    }
}
