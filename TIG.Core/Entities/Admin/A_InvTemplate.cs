﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Entities.Admin
{
    public class A_InvTemplate
    {
        public int Id { get; set; }
        public int InvCateID { get; set; }
        public string InvCateName { get; set; }
        public string TemplateName { get; set; }
        public string XmlFile { get; set; }
        public string XsltFile { get; set; }
        public string ServiceType { get; set; }
        public string InvoiceType { get; set; }
        public string InvoiceView { get; set; }
        public string IViewer { get; set; }
        public string IGenerator { get; set; }
        public string ParseService { get; set; }
        public string CssData { get; set; }
        public string CssLogo { get; set; }
        public string CssBackgr { get; set; }
        public string ImagePath { get; set; }
        public Boolean IsPub { get; set; }
        public bool IsCertify { get; set; }
        public virtual IList<A_RegisterTemp> RegisterTemps { get; set; }
    }
}
