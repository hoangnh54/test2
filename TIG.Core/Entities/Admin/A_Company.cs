﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using TIG.Core.Common;

namespace TIG.Core.Entities.Admin
{
    public class A_Company
    {
        [Column("id")]
        public int Id { get; set; }
        public int ParentID { get; set; }
        public string AgentCode { get; set; }
        public string SalerBy { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string ComNameAlias { get; set; }
        public string ParentName { get; set; }
        public string Address { get; set; }
        public string BankName { get; set; }
        public string BankNumber { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string Phone { get; set; }
        public string RepresentPerson { get; set; }

        public string TaxCode { get; set; }
        public string AccountName { get; set; }
        public string Domain { get; set; }

        [IgnoreDataMember]
        public virtual IList<A_Config> Configs { get; set; } = new List<A_Config>();

        [IgnoreDataMember]
        public virtual IDictionary<string, string> Config
        {
            get
            {
                if (Configs.Count > 0) return Configs.ToDictionary(x => x.Code, x => x.Value);
                return new Dictionary<string, string>();
            }
        }
        public bool Approved { get; set; }

        public bool IsUsed { get; set; }
        public string PortalLink { get; set; }
        public string ContactPerson { get; set; }
        public string ContactEmail { get; set; }

        [IgnoreDataMember]
        public virtual CompanyType Type { get; set; }

        public int? RegisterTempId { get; set; }
        public virtual List<A_RegisterTemp> RegisterTemps { get; set; }
    }
}
