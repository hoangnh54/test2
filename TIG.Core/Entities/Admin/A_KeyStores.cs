﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace TIG.Core.Entities.Admin
{
    public class A_KeyStores
    {
        public int Id { get; set; }
        public int ComID { get; set; }
        public int KeyStoresOf { get; set; }
        public string SerialCert { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public Decimal Type { get; set; }
        public string Path { get; set; }
        public string Password { get; set; }
        public int SlotIndex { get; set; }

        #region CERT
        //private X509Certificate2 cert;

        //public virtual X509Certificate2 OpenSession()
        //{
        //    try
        //    {
        //        if (Type == 0)
        //            cert = new X509Certificate2(Path, Password, X509KeyStorageFlags.Exportable);
        //        else if (Type == 2 || Type == 4)
        //            cert = GetCertificateBySerial(SerialCert);
        //        if (cert == null)
        //        {
        //            return cert;
        //        }
        //        return cert;
        //    }
        //    catch (Exception ex)
        //    {
        //        ILog log = LogManager.GetLogger(typeof(KeyStores));
        //        log.Error(ex);
        //        return null;
        //    }
        //}

        //public virtual void CloseSession()
        //{
        //    try
        //    {
        //        if (cert != null)
        //        {
        //            //cert.Reset();                    
        //            cert = null;
        //        }
        //    }
        //    catch
        //    {

        //    }
        //}
        ////Lấy CTS theo HSM hoặc USB token
        //private X509Certificate2 GetCertificateBySerial(String serial)
        //{
        //    ILog log = LogManager.GetLogger(typeof(KeyStores));
        //    X509Store certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
        //    certStore.Open(OpenFlags.ReadOnly);
        //    foreach (X509Certificate2 certificate in certStore.Certificates)
        //        if (certificate.GetSerialNumberString().ToUpper().CompareTo(serial.ToUpper().Trim()) == 0)
        //        {
        //            try
        //            {
        //                if (Type == 4)
        //                {
        //                    log.Error("HasPrivateKey: " + certificate.HasPrivateKey);
        //                    CspKeyContainerInfo keyContainerInfo = ((RSACryptoServiceProvider)certificate.PrivateKey).CspKeyContainerInfo;
        //                    CspParameters parameters = new CspParameters
        //                    {
        //                        ProviderName = keyContainerInfo.ProviderName,
        //                        ProviderType = keyContainerInfo.ProviderType,
        //                        KeyContainerName = keyContainerInfo.KeyContainerName,
        //                        KeyNumber = (int)keyContainerInfo.KeyNumber,
        //                        Flags = CspProviderFlags.UseExistingKey | CspProviderFlags.NoPrompt,
        //                        KeyPassword = new System.Security.SecureString()
        //                    };
        //                    foreach (char c in Password.ToCharArray())
        //                        parameters.KeyPassword.AppendChar(c);
        //                    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(parameters);
        //                    X509Certificate2 x509 = new X509Certificate2(certificate.GetRawCertData())
        //                    {
        //                        PrivateKey = rsa
        //                    };
        //                    return x509;
        //                }
        //                else
        //                {
        //                    var a = certificate;
        //                    AsymmetricAlgorithm aa = certificate.PrivateKey;
        //                    return certificate;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                throw new Exception("Không lấy được private key, chọn chứng thư khác!");
        //            }
        //        }
        //    return null;
        //}
        #endregion
    }
}
