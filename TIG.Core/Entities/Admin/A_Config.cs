﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Entities.Admin
{
    public class A_Config
    {
        public int ID { get; set; }
        public int ComID { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
        public virtual A_Company Company { get; set; }
    }
}
