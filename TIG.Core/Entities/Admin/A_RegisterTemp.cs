﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TIG.Core.Entities.Admin
{
    public class A_RegisterTemp
    {
        public string NameInvoice { get; set; }
        public int Id { get; set; }
        public int ComId { get; set; }
        public int TempID { get; set; }
        public int InvCateID { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public Decimal PatternOrder { get; set; }
        public string InvPattern { get; set; }
        public string Name { get; set; }
        public string CssData { get; set; }
        public string CssLogo { get; set; }
        public string CssBackgr { get; set; }
        public bool IsCertify { get; set; }
        public string ICertifyProvider { get; set; }
        public bool IsUsed { get; set; }
        public virtual A_InvTemplate InvoiceTemp { get; set; }
        public virtual A_Company Company { get; set; }
    }
}
