﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TIG.Core.Entities.Admin
{
    public class A_Certificate
    {
        [Column("id")]
        public virtual int Id { get; set; }
        [Column("ComID")]
        public virtual int ComID { get; set; }
        public virtual string SerialCert { get; set; }
        public virtual string Cert { get; set; }
        public virtual Boolean Used { get; set; }
        public virtual string OwnCA { get; set; }
        public virtual string OrganizationCA { get; set; }
        public virtual DateTime ValidFrom { get; set; }
        public virtual DateTime ValidTo { get; set; }
        public virtual int KsID { get; set; }
    }
}
