﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Entities.Admin
{
    public partial class A_Context : DbContext
    {
        public A_Context(DbContextOptions<A_Context> options)
                : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<A_Company>(e =>
            {
                e.ToTable("Company")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();

                e.HasMany<A_Config>(j => j.Configs)
                .WithOne(j => j.Company)
                .HasForeignKey(x => x.ComID);

                e.HasMany<A_RegisterTemp>(x => x.RegisterTemps)
                .WithOne(x => x.Company)
                .HasForeignKey(k => k.ComId);
            });

            modelBuilder.Entity<A_Config>(e =>
            {
                e.ToTable("Config")
                .HasKey(k => k.ID);

                e.Property(x => x.ID)
                .ValueGeneratedOnAdd();

                e.HasOne<A_Company>(sc => sc.Company)
                .WithMany(s => s.Configs)
                .HasForeignKey(sc => sc.ComID);
            });

            modelBuilder.Entity<A_KeyStores>(e =>
            {
                e.ToTable("KeyStores")
                .HasKey(k => k.Id);

                e.Property(x => x.Id)
                .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<A_Certificate>(e =>
            {
                e.ToTable("Certificate")
                .HasKey(k => k.Id);

                e.Property(x => x.Id)
                .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<A_InvTemplate>(e =>
            {
                e.ToTable("InvTemplate")
                .HasKey(k => k.Id);

                e.Property(x => x.Id)
                .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<A_RegisterTemp>(e =>
            {
                e.ToTable("RegisterTemp")
                .HasKey(k => k.Id);

                e.Property(x => x.Id)
                .ValueGeneratedOnAdd();

                e.HasOne<A_InvTemplate>(x => x.InvoiceTemp)
                .WithMany(x => x.RegisterTemps)
                .HasForeignKey(j => j.TempID);

                e.HasOne<A_Company>(x => x.Company)
                .WithMany(x => x.RegisterTemps)
                .HasForeignKey(j => j.ComId);
            });




            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);


        public virtual DbSet<A_Company> Company { get; set; }
        public virtual DbSet<A_Config> Config { get; set; }
        public virtual DbSet<A_KeyStores> KeyStores { get; set; }
        public virtual DbSet<A_Certificate> Certificate { get; set; }
        public virtual DbSet<A_InvTemplate> InvTemplate { get; set; }
        public virtual DbSet<A_RegisterTemp> RegisterTemp { get; set; }

    }
}
