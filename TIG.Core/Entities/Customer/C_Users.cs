﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TIG.Core.Entities.Customer
{
    public class C_Users
    {
        [Column("userid")]
        public int Id { get; set; }
        [Column("username")]
        public string UserName { get; set; }
        [Column("password")]
        public string Password { get; set; }
        [Column("PasswordFormat")]
        public int PasswordFormat { get; set; }
        [Column("PasswordSalt")]
        public string PasswordSalt { get; set; }
        [Column("FullName")]
        public string FullName { get; set; }
        [Column("GroupName")]
        public string GroupName { get; set; }
        [Column("IsApproved")]
        public bool IsApproved { get; set; }
        public virtual IList<C_UserRole> UserRoles { get; set; }
    }
}
