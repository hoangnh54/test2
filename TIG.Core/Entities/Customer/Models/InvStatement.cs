﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using TIG.Core.Common;

namespace TIG.Core.Entities.Customer.Models
{
    [XmlType("HDonBRa")]
    public class InvStatement
    {
        [XmlAttribute("id")]
        public string ID { get; set; }
        [XmlIgnore]
        public InvoiceType type { get; set; }
        [XmlIgnore]
        public InvoiceStatus status { get; set; }
        //gianglt bo sung
        //start
        [XmlElement("soThamChieu")]
        public string soThamChieu { get; set; }

        [XmlElement("matHang")]
        public string matHang { get; set; }

        [XmlElement("khoXuatHang")]
        public string khoXuatHang { get; set; }

        [XmlElement("lit15")]
        public string lit15 { get; set; }
        [XmlElement("litTT")]
        public string litTT { get; set; }
        [XmlElement("phaiThu")]
        public string phaiThu { get; set; }

        //end

        [XmlElement("kyHieuHDon")]
        public string kyHieuHDon { get; set; }

        [XmlElement("kyHieuMauHDon")]
        public string kyHieuMauHDon { get; set; }

        [XmlElement("soHoaDon")]
        public decimal soHoaDon { get; set; }

        [XmlElement("ngayLapHD")]
        public string ngayLapHD { get; set; }

        [XmlElement("tenNguoiMua")]
        public string tenNguoiMua { get; set; }

        [XmlElement("masoThueNguoiMua")]
        public string masoThueNguoiMua { get; set; }

        public decimal Grossvalue { get; set; }
        public string Note { get; set; }

        public string ExchangeRate { get; set; }

        public decimal doanhThuChuaThueGTGT
        {

            get
            {
                if (status == InvoiceStatus.ReplacedInv || status == InvoiceStatus.CanceledInv)
                    return 0;
                else if (type == InvoiceType.ForAdjustReduce)
                {
                    return -Grossvalue;
                }
                else
                {
                    return Grossvalue;
                }
            }
        }
        public decimal VATAmount { get; set; }
        public decimal thueGTGT
        {
            get
            {
                if (status == InvoiceStatus.ReplacedInv || status == InvoiceStatus.CanceledInv)
                    return 0;
                else if (type == InvoiceType.ForAdjustReduce)
                {
                    return -VATAmount;
                }
                else
                {
                    return VATAmount;
                }
            }

        }

        [XmlElement("ghiChu")]
        public string ghiChu
        {
            get
            {
                if (status == InvoiceStatus.ReplacedInv || status == InvoiceStatus.CanceledInv)
                    return "Hóa đơn xóa bỏ";
                else if (type == InvoiceType.ForAdjustReduce)
                {
                    return "Điều chỉnh sai sót giảm";
                }
                else if (type == InvoiceType.ForAdjustAccrete)
                {
                    return "Điều chỉnh sai sót tăng";
                }
                else if (type == InvoiceType.ForAdjustInfo)
                {
                    return "Điều chỉnh thông tin";
                }
                else return Note;
            }
        }
        /// <summary>
        /// <0: Khong thue; =0: thue 0%; =5 thue 5%; =10 thue 10% 
        /// </summary>
        [XmlIgnore]
        public float VatRate { get; set; }
    }
}
