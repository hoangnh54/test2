﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Common;

namespace TIG.Core.Entities.Customer.Models
{
    public class InvReportTax
    {
        public int id { get; set; }
        public InvoiceStatus Status { get; set; }
        public InvoiceType Type { get; set; }
        public bool IsSum { get; set; }
        public string Pattern { get; set; }
        public string Serial { get; set; }
        public decimal No { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string CusName { get; set; }
        public string CusTaxCode { get; set; }
        public string ProdName { get; set; }
        public decimal ProdTotal { get; set; }
        public decimal ProdVatAmount { get; set; }
        public double VATRate { get; set; }
    }
}
