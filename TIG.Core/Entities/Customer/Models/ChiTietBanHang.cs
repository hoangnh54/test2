﻿using System;
using TIG.Core.Common;

namespace TIG.Core.Entities.Customer.Models
{
    public class ChiTietBanHang
    {
        public int id { get; set; }
        public InvoiceStatus Status { get; set; }
        public InvoiceType Type { get; set; }
        public string Pattern { get; set; }
        public string Serial { get; set; }
        public decimal No { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Buyer { get; set; }
        public string CusName { get; set; }
        public string CusTaxCode { get; set; }
        public bool IsSum { get; set; }
        public string MaHang { get; set; }
        public string TenHang { get; set; }
        public string DonVi { get; set; }
        public decimal SoLuong { get; set; }
        public decimal DonGia { get; set; }
        public decimal ProdTotal { get; set; }
        public decimal ProdVatAmount { get; set; }
        public decimal ProdAmount { get; set; }
        public Double VATRate { get; set; }

        public decimal VATAmount { get; set; }
    }
}
