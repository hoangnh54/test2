﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using TIG.Core.Entities.Customer.Interfaces;

namespace TIG.Core.Entities.Customer.Models
{
    [XmlType("Product")]
    [DataContract(Name = "Product")]
    public class ProductInv : IProductInv
    {
        [XmlElement("id")]
        [IgnoreDataMember]
        [Column("Id")]
        public virtual Guid Id { get; set; }

        [XmlIgnore]
        [IgnoreDataMember]
        public virtual int ComID { get; set; }

        [XmlElement("InvID")]
        [IgnoreDataMember]
        public virtual int InvID { get; set; }

        [XmlElement("ProdName")]
        [DataMember(Name = "ProdName")]
        public virtual string Name { get; set; }

        [XmlElement("ProdPrice")]
        [DataMember(Name = "ProdPrice")]
        public virtual Decimal Price { get; set; }

        [XmlElement("ProdQuantity")]
        [DataMember(Name = "ProdQuantity")]
        public virtual Decimal Quantity { get; set; }

        [XmlElement("ProdUnit")]
        [DataMember(Name = "ProdUnit")]
        public virtual string Unit { get; set; }

        [XmlElement("VATRate")]
        [DataMember(Name = "VATRate")]
        public virtual double VATRate { get; set; }

        [XmlElement("Discount")]
        [DataMember(Name = "Discount")]
        public virtual double Discount { get; set; }

        [XmlElement("VATAmount")]
        [DataMember(Name = "VATAmount")]
        public virtual Decimal VATAmount { get; set; }

        [XmlElement("DiscountAmount")]
        [DataMember(Name = "DiscountAmount")]
        public virtual Decimal DiscountAmount { get; set; }

        [XmlElement("Amount")]
        [DataMember(Name = "Amount")]
        public virtual Decimal Amount { get; set; }

        [XmlElement("ProdType")]
        [DataMember(Name = "ProdType")]
        public virtual int ProdType { get; set; } = 1;

        [XmlElement("IsSum")]
        [DataMember(Name = "IsSum")]
        public virtual bool IsSum { get; set; } = false;


        [XmlElement("IsPDV")]
        [DataMember(Name = "IsPDV")]
        public virtual bool IsPDV { get; set; } = false;

        [XmlElement("PDVRate")]
        [DataMember(Name = "PDVRate")]
        public virtual double PDVRate { get; set; }

        /// <summary>
        /// Ma san pham
        /// </summary>
        [XmlElement("Code")]
        [DataMember(Name = "Code")]
        public virtual string Code { get; set; }

        /// <summary>
        /// Ghi chu san pham
        /// </summary>
        [XmlElement("Remark")]
        [DataMember(Name = "Remark")]
        public virtual string Remark { get; set; }


        /// <summary>
        /// Ghi chu san pham
        /// </summary>
        [XmlElement("CreateDate")]
        [DataMember(Name = "CreateDate")]
        public virtual DateTime? CreateDate { get; set; } = DateTime.Now;


        [XmlElement("OrderBy")]
        [DataMember(Name = "OrderBy")]
        public virtual int OrderBy { get; set; }

        /// <summary>
        /// tong cong tien truoc thue
        /// </summary>
        [XmlElement("Total")]
        [DataMember(Name = "Total")]
        public virtual Decimal Total { get; set; }

        [XmlElement("Extra")]
        [DataMember(Name = "Extra")]
        public virtual string Extra { get; set; }

        [XmlElement("ProductId")]
        [DataMember(Name = "ProductId")]
        public virtual int? ProductId { get; set; }
        [XmlIgnore]
        public virtual InvoiceVAT InvoiceVAT { get; set; }
        //public virtual C_VATInvoice VATInvoice { get; set; }
    }
}
