﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer.Interfaces;

namespace TIG.Core.Entities.Customer.Models
{
    [XmlType("Invoice")]
    [DataContract(Name = "Invoice", Namespace = "")]
    public class InvoiceVAT : InvoiceBase
    {

        [XmlIgnore]
        public override int Id { get; set; }

        [XmlElement("Discount")]
        [DataMember(Name = "Discount")]
        public virtual decimal Discount { get; set; }

        [XmlElement("Extra")]
        [DataMember(Name = "Extra", Order = 28)]
        public virtual string Extra { get; set; }

        [XmlElement("VATRate")]
        [DataMember(Name = "VATRate")]
        public virtual Double VATRate { get; set; }

        public virtual int? NumQuantity { get; set; }
        public virtual int? NumPrice { get; set; }
        public virtual int? NumAmount { get; set; }

        [XmlElement("ComEmail")]
        [DataMember(Name = "ComEmail")]
        public virtual string ComEmail { get; set; }

        #region NewHope
        [XmlElement("Extra1")]
        [DataMember(Name = "Extra1")]
        public virtual string Extra1 { get; set; }
        #endregion

        [XmlElement("PortalLink")]
        [DataMember(Name = "PortalLink")]
        public virtual string PortalLink { get; set; }


        public override string SerializeToXML()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                //XmlNode nVersion = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
                //xmlDoc.AppendChild(nVersion);
                //van den Einvoice
                XmlNode ContentInv = null;
                if (this.Type == InvoiceType.ForReplace)
                    ContentInv = xmlDoc.CreateElement("ReplaceInv");
                else if (this.Type == InvoiceType.ForAdjustAccrete || this.Type == InvoiceType.ForAdjustInfo || this.Type == InvoiceType.ForAdjustReduce)
                    ContentInv = xmlDoc.CreateElement("AdjustInv");
                else
                    ContentInv = xmlDoc.CreateElement("Invoice");
                if (ContentInv != null) xmlDoc.AppendChild(ContentInv);

                XmlNode key = xmlDoc.CreateElement("key");
                key.InnerText = this.Fkey;
                ContentInv.AppendChild(key);
                XmlNode ComAddress = xmlDoc.CreateElement("ComAddress");
                ComAddress.InnerText = this.ComAddress;
                ContentInv.AppendChild(ComAddress);
                //van de ve customer 
                XmlNode CusCode = xmlDoc.CreateElement("CusCode");
                CusCode.InnerText = this.CusCode;
                ContentInv.AppendChild(CusCode);

                XmlNode CusName = xmlDoc.CreateElement("CusName");
                CusName.InnerText = this.CusName;
                ContentInv.AppendChild(CusName);

                XmlNode Buyer = xmlDoc.CreateElement("Buyer");
                Buyer.InnerText = this.Buyer;
                ContentInv.AppendChild(Buyer);

                XmlNode CusAddress = xmlDoc.CreateElement("CusAddress");
                CusAddress.InnerText = this.CusAddress;
                ContentInv.AppendChild(CusAddress);

                XmlNode CusPhone = xmlDoc.CreateElement("CusPhone");
                CusPhone.InnerText = this.CusPhone;
                ContentInv.AppendChild(CusPhone);

                XmlNode CusEmail = xmlDoc.CreateElement("CusEmail");
                CusEmail.InnerText = this.CusEmail;
                ContentInv.AppendChild(CusEmail);

                XmlNode CusTaxCode = xmlDoc.CreateElement("CusTaxCode");
                CusTaxCode.InnerText = this.CusTaxCode;
                ContentInv.AppendChild(CusTaxCode);

                #region thêm 2 trường năm sinh và mã bệnh nhân cho bệnh viện Củ Chi


                #endregion




                XmlNode PaymentMethod = xmlDoc.CreateElement("PaymentMethod");
                PaymentMethod.InnerText = this.PaymentMethod;
                ContentInv.AppendChild(PaymentMethod);

                XmlNode PortalLink = xmlDoc.CreateElement("PortalLink");
                PortalLink.InnerText = this.PortalLink;
                ContentInv.AppendChild(PortalLink);

                XmlNode ComFax = xmlDoc.CreateElement("ComFax");
                ComFax.InnerText = this.ComFax;
                ContentInv.AppendChild(ComFax);

                XmlNode ComEmail = xmlDoc.CreateElement("ComEmail");
                ComEmail.InnerText = this.ComEmail;
                ContentInv.AppendChild(ComEmail);

                XmlNode Trans = xmlDoc.CreateElement("Trans");
                Trans.InnerText = this.Trans;
                ContentInv.AppendChild(Trans);

                XmlNode ExchangeRate = xmlDoc.CreateElement("ExchangeRate");
                ExchangeRate.InnerText = this.ExchangeRate;
                ContentInv.AppendChild(ExchangeRate);

                XmlNode Currency = xmlDoc.CreateElement("Currency");
                Currency.InnerText = this.Currency;
                ContentInv.AppendChild(Currency);
                //end customer
                //product
                //van de ve san pham
                XmlNode Products = xmlDoc.CreateElement("Products");
                ContentInv.AppendChild(Products);
                //khoi tao du lieu da co
                //Insert tung phan tu vao product
                foreach (var el in this.Products)
                {
                    XmlNode Product = xmlDoc.CreateElement("Product");
                    Products.AppendChild(Product);

                    //begin product
                    XmlNode ProdCode = xmlDoc.CreateElement("Code");
                    ProdCode.InnerText = el.Code;
                    Product.AppendChild(ProdCode);

                    XmlNode ProdName = xmlDoc.CreateElement("ProdName");
                    ProdName.InnerText = el.Name;
                    Product.AppendChild(ProdName);

                    XmlNode Remark = xmlDoc.CreateElement("Remark");
                    Remark.InnerText = el.Remark;
                    Product.AppendChild(Remark);

                    XmlNode ExtraProd = xmlDoc.CreateElement("Extra");
                    ExtraProd.InnerText = el.Extra;
                    Product.AppendChild(ExtraProd);

                    XmlNode ProdPrice = xmlDoc.CreateElement("ProdPrice");
                    ProdPrice.InnerText = el.Price.ToString();
                    Product.AppendChild(ProdPrice);

                    XmlNode ProdQuantity = xmlDoc.CreateElement("ProdQuantity");
                    ProdQuantity.InnerText = el.Quantity.ToString();
                    Product.AppendChild(ProdQuantity);

                    XmlNode ProdUnit = xmlDoc.CreateElement("ProdUnit");
                    ProdUnit.InnerText = el.Unit;
                    Product.AppendChild(ProdUnit);

                    XmlNode Amount = xmlDoc.CreateElement("Amount");
                    Amount.InnerText = el.Amount.ToString();
                    Product.AppendChild(Amount);
                }
                //end product
                //other
                //tong tien dich vu
                XmlNode Total = xmlDoc.CreateElement("Total");
                Total.InnerText = this.Total.ToString();
                ContentInv.AppendChild(Total);

                //thue gia tri gia tang  VAT_Rate
                XmlNode VAT_Rate = xmlDoc.CreateElement("VATRate");
                VAT_Rate.InnerText = this.VATRate.ToString();
                ContentInv.AppendChild(VAT_Rate);

                // tong tien VAT_Amount
                XmlNode VAT_Amount = xmlDoc.CreateElement("VATAmount");
                VAT_Amount.InnerText = this.VATAmount.ToString();
                ContentInv.AppendChild(VAT_Amount);

                //Amount
                XmlNode amount = xmlDoc.CreateElement("Amount");
                amount.InnerText = this.Amount.ToString();
                ContentInv.AppendChild(amount);

                //hien thi so tien bang chu
                XmlNode Amount_words = xmlDoc.CreateElement("AmountInWords");
                Amount_words.InnerText = this.AmountInWords;
                ContentInv.AppendChild(Amount_words);

                //truong extra mở rộng
                XmlNode Extra = xmlDoc.CreateElement("Extra");
                Extra.InnerText = this.Extra;
                ContentInv.AppendChild(Extra);

                //kieu hoa don
                XmlNode Type = xmlDoc.CreateElement("Type");
                Type.InnerText = ((int)this.Type).ToString();
                ContentInv.AppendChild(Type);

                //ngay phat hanh hoa don
                XmlNode ArisingDate = xmlDoc.CreateElement("ArisingDate");
                ArisingDate.InnerText = this.ArisingDate.ToString("dd/MM/yyyy");
                ContentInv.AppendChild(ArisingDate);

                //Ghi chu hoa don
                //truong extra mở rộng
                XmlNode Note = xmlDoc.CreateElement("Note");
                Note.InnerText = this.Note;
                ContentInv.AppendChild(Note);

                //XElement xeles = XElement.Parse(xmlDoc.OuterXml);
                return xmlDoc.OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ProductInv deserializeProduct(XElement el)
        {
            ProductInv p = new ProductInv();
            if (el.Element("Code") != null) p.Code = el.Element("Code").Value;
            if (el.Element("ProdName") != null) p.Name = el.Element("ProdName").Value;
            if (el.Element("Extra") != null) p.Extra = el.Element("Extra").Value;
            if (el.Element("Remark") != null) p.Remark = el.Element("Remark").Value;
            if (el.Element("IsSum") != null && !string.IsNullOrWhiteSpace(el.Element("IsSum").Value))
                p.IsSum = Boolean.Parse(el.Element("IsSum").Value);
            if (el.Element("ProdPrice") != null && !string.IsNullOrWhiteSpace(el.Element("ProdPrice").Value))
                p.Price = Decimal.Parse(el.Element("ProdPrice").Value);
            if (el.Element("ProdQuantity") != null && !string.IsNullOrWhiteSpace(el.Element("ProdQuantity").Value))
                p.Quantity = Decimal.Parse(el.Element("ProdQuantity").Value);
            if (el.Element("ProdUnit") != null) p.Unit = el.Element("ProdUnit").Value;
            if (el.Element("Total") != null && !string.IsNullOrWhiteSpace(el.Element("Total").Value))
                p.Total = Decimal.Parse(el.Element("Total").Value);
            if (el.Element("Amount") != null && !string.IsNullOrWhiteSpace(el.Element("Amount").Value))
                p.Amount = Decimal.Parse(el.Element("Amount").Value);
            if (el.Element("VATRate") != null && !string.IsNullOrWhiteSpace(el.Element("VATRate").Value))
                p.VATRate = Convert.ToInt32(el.Element("VATRate").Value);
            if (el.Element("Discount") != null && !string.IsNullOrWhiteSpace(el.Element("Discount").Value))
                p.Discount = Convert.ToInt32(el.Element("Discount").Value);
            if (el.Element("VATAmount") != null && !string.IsNullOrWhiteSpace(el.Element("VATAmount").Value))
                p.VATAmount = Decimal.Parse(el.Element("VATAmount").Value);
            if (el.Element("DiscountAmount") != null && !string.IsNullOrWhiteSpace(el.Element("DiscountAmount").Value))
                p.DiscountAmount = Decimal.Parse(el.Element("DiscountAmount").Value);
            p.Total = p.Total > 0 ? p.Total : p.Amount;
            p.ProdType = 1;
            return p;
        }

        public override void DeserializeFromXML(string XML, A_Company company)
        {
            ComID = company.Id;
            ComName = company.Name;
            ComBankName = company.BankName;
            ComBankNo = company.BankNumber;
            ComTaxCode = company.TaxCode;
            ComPhone = company.Phone;
            ComFax = company.Fax;
            ComAddress = company.Address;
            PortalLink = company.PortalLink;
            ParentName = company.ParentName;
            ComEmail = company.Email;

            XElement elem = XElement.Parse(XML);
            if (elem.Element("Fkey") != null)
                Fkey = elem.Element("Fkey").Value;
            if (elem.Element("ComPhone") != null)
                ComPhone = elem.Element("ComPhone").Value;
            if (elem.Element("ComBankName") != null)
                ComBankName = elem.Element("ComBankName").Value;
            if (elem.Element("ComBankNo") != null)
                ComBankNo = elem.Element("ComBankNo").Value;

            if (elem.Element("CusName") != null)
                CusName = elem.Element("CusName").Value;
            if (elem.Element("Buyer") != null)
                Buyer = elem.Element("Buyer").Value;
            if (elem.Element("CusAddress") != null)
                CusAddress = elem.Element("CusAddress").Value;
            if (elem.Element("CusCode") != null)
                CusCode = elem.Element("CusCode").Value;
            if (elem.Element("CusTaxCode") != null)
                CusTaxCode = elem.Element("CusTaxCode").Value;
            CusCode ??= CusTaxCode;

            #region NewHope
            if (elem.Element("Extra1") != null)
                Extra1 = elem.Element("Extra1").Value;
            #endregion


            #region dược Thiên Thành

            if (elem.Element("TradeDiscount0") != null)
                TradeDiscount0 = Decimal.Parse(elem.Element("TradeDiscount0").Value);
            if (elem.Element("TradeDiscount5") != null)
                TradeDiscount5 = Decimal.Parse(elem.Element("TradeDiscount5").Value);
            if (elem.Element("TradeDiscount10") != null)
                TradeDiscount10 = Decimal.Parse(elem.Element("TradeDiscount10").Value);
            if (elem.Element("TradeDiscount0") != null)
                TradeDiscount0 = Decimal.Parse(elem.Element("TradeDiscount0").Value);
            if (elem.Element("TradeDiscount0") != null)
                TradeDiscount0 = Decimal.Parse(elem.Element("TradeDiscount0").Value);
            if (elem.Element("TradeDiscount0") != null)
                TradeDiscount0 = Decimal.Parse(elem.Element("TradeDiscount0").Value);
            if (elem.Element("TradeDiscount0") != null)
                TradeDiscount0 = Decimal.Parse(elem.Element("TradeDiscount0").Value);
            if (elem.Element("TradeDiscount0") != null)
                TradeDiscount0 = Decimal.Parse(elem.Element("TradeDiscount0").Value);
            if (elem.Element("TradeDiscount0") != null)
                TradeDiscount0 = Decimal.Parse(elem.Element("TradeDiscount0").Value);
            #endregion

            if (elem.Element("CusBankName") != null)
                CusBankName = elem.Element("CusBankName").Value;
            if (elem.Element("CusBankNo") != null)
                CusBankNo = elem.Element("CusBankNo").Value;
            if (elem.Element("CusPhone") != null)
                CusPhone = elem.Element("CusPhone").Value;
            if (elem.Element("CusEmail") != null)
                CusEmail = elem.Element("CusEmail").Value;
            if (elem.Element("PaymentMethod") != null)
                PaymentMethod = elem.Element("PaymentMethod").Value;
            if (elem.Element("AmountInWords") != null)
                AmountInWords = elem.Element("AmountInWords").Value;
            if (elem.Element("Name") != null)
                Name = elem.Element("Name").Value;

            if (elem.Element("Serial") != null)
                Serial = elem.Element("Serial").Value;
            if (elem.Element("Pattern") != null)
                Pattern = elem.Element("Pattern").Value;

            //if (elem.Element("CreateDate") != null)
            //    CreateDate = DateTime.Parse(elem.Element("CreateDate").Value);
            if (elem.Element("PublishDate") != null)
                PublishDate = DateTime.Parse(elem.Element("PublishDate").Value);
            if (elem.Element("PortalLink") != null)
                PortalLink = elem.Element("PortalLink").Value;

            if (elem.Element("Discount") != null && !string.IsNullOrWhiteSpace(elem.Element("Discount").Value))
                Discount = Decimal.Parse(elem.Element("Discount").Value);
            if (elem.Element("Total") != null && !string.IsNullOrWhiteSpace(elem.Element("Total").Value))
                Total = Decimal.Parse(elem.Element("Total").Value);
            if (elem.Element("VATRate") != null && !string.IsNullOrWhiteSpace(elem.Element("VATRate").Value))
                VATRate = (float)Convert.ToDecimal(elem.Element("VATRate").Value);
            if (elem.Element("VATAmount") != null && !string.IsNullOrWhiteSpace(elem.Element("VATAmount").Value))
                VATAmount = Decimal.Parse(elem.Element("VATAmount").Value);
            if (elem.Element("Amount") != null && !string.IsNullOrWhiteSpace(elem.Element("Amount").Value))
                Amount = Decimal.Parse(elem.Element("Amount").Value);

            if (elem.Element("Extra") != null)
                Extra = elem.Element("Extra").Value;
            if (elem.Element("Trans") != null)
                Trans = elem.Element("Trans").Value;

            if (elem.Element("Currency") != null)
                Currency = elem.Element("Currency").Value;
            if (elem.Element("ExchangeRate") != null)
                ExchangeRate = elem.Element("ExchangeRate").Value;

            if (elem.Element("PaymentStatus") != null && !string.IsNullOrWhiteSpace(elem.Element("PaymentStatus").Value))
                PaymentStatus = (Payment)Convert.ToInt32(elem.Element("PaymentStatus").Value);
            if (elem.Element("ArisingDate") != null && !string.IsNullOrWhiteSpace(elem.Element("ArisingDate").Value))
                ArisingDate = DateTime.ParseExact(elem.Element("ArisingDate").Value, new string[] { "dd/MM/yyyy", "dd/M/yyyy" }, new CultureInfo("en-US"), DateTimeStyles.None);
            else
                ArisingDate = DateTime.Now;

            if (elem.Element("Note") != null)
                Note = elem.Element("Note").Value;
            if (VATRate == -1)
                GrossValue = Total;
            if (VATRate == 0)
            {
                VatAmount0 = 0;
                GrossValue0 = Total;
            }
            if (VATRate == 5)
            {
                VatAmount5 = VATAmount;
                GrossValue5 = Total;
            }
            if (VATRate == 10)
            {
                VatAmount10 = VATAmount;
                GrossValue10 = Total;
            }
            XElement XeleProd = elem.Element("Products");
            IEnumerable<XElement> plist = (from c in XeleProd.Elements("Product") select c).ToList<XElement>();

            foreach (XElement el in plist)
            {
                ProductInv p = deserializeProduct(el);
                this.Products.Add(p);
            }
        }

        public override void DeserializeFromJSON(string Json, A_Company currentCom)
        {
            //A_Company currentCom = (A_Company)FXContext.Current.CurrentCompany;
            ComID = currentCom.Id;
            ComName = currentCom.Name;
            ComBankName = currentCom.BankName;
            ComBankNo = currentCom.BankNumber;
            ComTaxCode = currentCom.TaxCode;
            ComPhone = currentCom.Phone;
            PortalLink = currentCom.PortalLink;
            ComAddress = currentCom.Address;
            ComFax = currentCom.Fax;
            ComEmail = currentCom.Email;

            Dictionary<string, object> dictInvoice = JsonConvert.DeserializeObject<Dictionary<string, object>>(Json);
            if (dictInvoice.ContainsKey("Fkey"))
                Fkey = dictInvoice["Fkey"].ToString();

            if (dictInvoice.Keys.Contains("ComBankName") && !string.IsNullOrWhiteSpace(dictInvoice["ComBankName"].ToString()))
                ComBankName = dictInvoice["ComBankName"].ToString();
            if (dictInvoice.Keys.Contains("ComBankNo") && !string.IsNullOrWhiteSpace(dictInvoice["ComBankNo"].ToString()))
                ComBankNo = dictInvoice["ComBankNo"].ToString();

            if (dictInvoice.Keys.Contains("InvoiceNo"))
                No = Convert.ToDecimal(dictInvoice["InvoiceNo"].ToString());
            if (dictInvoice.ContainsKey("Name"))
                Name = dictInvoice["Name"].ToString();
            if (dictInvoice.ContainsKey("Serial"))
                Serial = dictInvoice["Serial"].ToString();
            if (dictInvoice.ContainsKey("Pattern"))
                Pattern = dictInvoice["Pattern"].ToString();

            if (dictInvoice.ContainsKey("CusName"))
                CusName = dictInvoice["CusName"].ToString().Trim();
            if (dictInvoice.ContainsKey("Buyer"))
                Buyer = dictInvoice["Buyer"].ToString().Trim();
            if (dictInvoice.ContainsKey("CusAddress"))
                CusAddress = dictInvoice["CusAddress"].ToString().Trim();
            if (dictInvoice.ContainsKey("CusCode"))
                CusCode = dictInvoice["CusCode"].ToString();
            if (dictInvoice.Keys.Contains("CusEmail"))
                CusEmail = dictInvoice["CusEmail"].ToString();
            if (dictInvoice.ContainsKey("CusTaxCode"))
                CusTaxCode = dictInvoice["CusTaxCode"].ToString();
            CusCode ??= CusTaxCode;
            if (dictInvoice.ContainsKey("CusBankName"))
                CusBankName = dictInvoice["CusBankName"].ToString();
            if (dictInvoice.ContainsKey("CusBankNo"))
                CusBankNo = dictInvoice["CusBankNo"].ToString();
            if (dictInvoice.ContainsKey("CusPhone"))
                CusPhone = dictInvoice["CusPhone"].ToString();

            if (dictInvoice.ContainsKey("PaymentMethod"))
                PaymentMethod = dictInvoice["PaymentMethod"].ToString();
            if (dictInvoice.ContainsKey("PaymentStatus"))
                PaymentStatus = (Payment)Convert.ToInt32(dictInvoice["PaymentStatus"].ToString());

            if (dictInvoice.Keys.Contains("ArisingDate"))
                ArisingDate = DateTime.ParseExact(dictInvoice["ArisingDate"].ToString(), new string[] { "dd/MM/yyyy", "MM/dd/yyyy", "dd-MM-yyyy", "MM/dd/yyyy hh:mm tt", "MM/dd/yyyy HH:mm:ss" }, new CultureInfo("en-US"), DateTimeStyles.None);

            if (dictInvoice.ContainsKey("Discount") && dictInvoice["Discount"] != null)
            {
                if (!Decimal.TryParse(dictInvoice["Discount"].ToString(), out decimal discount))
                    throw new Exception("Discount: Error Format-" + dictInvoice["Discount"].ToString());
                Discount = discount;
            }

            if (dictInvoice.ContainsKey("Total") && dictInvoice["Total"] != null)
            {
                if (!Decimal.TryParse(dictInvoice["Total"].ToString(), out decimal total))
                    throw new Exception("Total: Error Format-" + dictInvoice["Total"].ToString());
                Total = total;
            }

            if (dictInvoice.ContainsKey("VATAmount") && dictInvoice["VATAmount"] != null)
            {
                if (!Decimal.TryParse(dictInvoice["VATAmount"].ToString(), out decimal vatamount))
                    throw new Exception("VATAmount: Error Format-" + dictInvoice["VATAmount"].ToString());
                VATAmount = vatamount;
            }

            if (dictInvoice.ContainsKey("Amount") && dictInvoice["Amount"] != null)
            {
                if (!Decimal.TryParse(dictInvoice["Amount"].ToString(), out decimal amount))
                    throw new Exception("Amount: Error Format-" + dictInvoice["Amount"].ToString());
                Amount = amount;
            }

            if (dictInvoice.ContainsKey("AmountInWords"))
                AmountInWords = dictInvoice["AmountInWords"].ToString();

            if (dictInvoice.ContainsKey("Extra"))
                Extra = dictInvoice["Extra"].ToString();

            if (dictInvoice.ContainsKey("Currency"))
                Currency = dictInvoice["Currency"].ToString();
            if (dictInvoice.ContainsKey("ExchangeRate"))
                ExchangeRate = dictInvoice["ExchangeRate"].ToString();
            if (dictInvoice.ContainsKey("Trans"))
                Trans = dictInvoice["Trans"].ToString();

            if (dictInvoice.ContainsKey("Note"))
                Note = dictInvoice["Note"].ToString();

            IList<ProductInv> listProducts = JsonConvert.DeserializeObject<IList<ProductInv>>(dictInvoice["Products"].ToString());
            foreach (ProductInv p in listProducts)
            {
                p.Name = p.Name.Trim();
                p.Total = p.Total > 0 ? p.Total : p.Amount;
                p.ProdType = 1;
                this.Products.Add(p);
            }

            if (Total == 0)
                Total = Amount - VATAmount;
            if (dictInvoice.Keys.Contains("VATRate"))
                VATRate = Convert.ToInt32(dictInvoice["VATRate"]);

            if (VATRate == 5)
            {
                GrossValue5 = Total;
                VatAmount5 = VATAmount;
            }
            if (VATRate == 10)
            {
                GrossValue10 = Total;
                VatAmount10 = VATAmount;
            }
            if (VATRate == 0)
            {
                GrossValue0 = Total;
                VatAmount0 = VATAmount;
            }
            if (VATRate == -1)
                GrossValue = Total;
        }

        public override decimal GrossValue { get; set; }
        public override decimal GrossValue0 { get; set; }
        public override decimal VatAmount0 { get; set; }
        public override decimal GrossValue5 { get; set; }
        public override decimal VatAmount5 { get; set; }
        public override decimal GrossValue10 { get; set; }
        public override decimal VatAmount10 { get; set; }
        public virtual decimal TradeDiscount0 { get; set; }
        public virtual decimal TradeDiscount5 { get; set; }
        public virtual decimal TradeDiscount10 { get; set; }
        public virtual decimal PlusAmountAfterDiscount0 { get; set; }
        public virtual decimal PlusAmountAfterDiscount5 { get; set; }
        public virtual decimal PlusAmountAfterDiscount10 { get; set; }
        public virtual decimal AmountVAT0 { get; set; }
        public virtual decimal AmountVAT5 { get; set; }
        public virtual decimal AmountVAT10 { get; set; }
    }
}
