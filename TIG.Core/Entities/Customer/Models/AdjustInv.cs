﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Common;

namespace TIG.Core.Entities.Customer.Models
{
    public class AdjustInv
    {
        private int _id;
        private int _InvId;
        private int _AdjustInvId;
        private string _Description;
        private string _Pattern;
        private DateTime _ProcessDate = DateTime.Now;
        private ProcessingStatus _Status;
        private int _ComID;
        public virtual string Attachefile { get; set; }
        //private IList<RecordsInv> _RecordsInv = new List<RecordsInv>();

        public virtual int id
        {
            get { return _id; }
            set { _id = value; }
        }

        public virtual int InvId
        {
            get { return _InvId; }
            set { _InvId = value; }
        }

        public virtual int AdjustInvId
        {
            get { return _AdjustInvId; }
            set { _AdjustInvId = value; }
        }

        public virtual string Pattern
        {
            get { return _Pattern; }
            set { _Pattern = value; }
        }

        public virtual string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public virtual DateTime ProcessDate
        {
            get { return _ProcessDate; }
            set { _ProcessDate = value; }
        }

        public virtual ProcessingStatus Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public virtual int ComID
        {
            get { return _ComID; }
            set { _ComID = value; }
        }

        //public virtual IList<RecordsInv> RecordsInv
        //{
        //    get { return _RecordsInv; }
        //    set { _RecordsInv = value; }
        //}
    }
}
