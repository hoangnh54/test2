﻿using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer.Interfaces;

namespace TIG.Core.Entities.Customer.Models
{
    [XmlType("Invoice")]
    [DataContract(Name = "Invoice", Namespace = "")]
    public class ExtendsVATInvoice : InvoiceBase
    {
        public override Payment PaymentStatus { get; set; } = Payment.Paid;

        [XmlElement("ComEmail")]
        [DataMember(Name = "ComEmail")]
        public virtual string ComEmail { get; set; }

        [XmlElement("VATRate")]
        [DataMember(Name = "VATRate")]
        public virtual float VATRate { get; set; }

        //Tiền thuế
        [XmlElement("VATAmount")]
        [DataMember(Name = "VATAmount")]
        public virtual decimal VATAmount { get; set; }

        //Thuế khác
        [XmlElement("VATOther")]
        [DataMember(Name = "VATOther")]
        public virtual decimal VATOther { get; set; }

        //Phí khác
        [XmlElement("OtherFees")]
        [DataMember(Name = "OtherFees")]
        public virtual decimal OtherFees { get; set; }

        //Phụ thu dịch vụ
        [XmlElement("ChangeFees")]
        [DataMember(Name = "ChangeFees")]
        public virtual decimal ChangeFees { get; set; }

        //Phụ phí khác
        [XmlElement("ExtraFees")]
        [DataMember(Name = "ExtraFees")]
        public virtual decimal ExtraFees { get; set; }

        //Thuế GTGT phụ phí
        [XmlElement("VATRateOther")]
        [DataMember(Name = "VATRateOther")]
        public virtual decimal VATRateOther { get; set; }

        //Tiền thuế GTGT phụ phí
        [XmlElement("VATAmountOther")]
        [DataMember(Name = "VATAmountOther")]
        public virtual decimal VATAmountOther { get; set; }

        //Tổng tiền phí dịch vụ
        [XmlElement("AmountOther")]
        [DataMember(Name = "AmountOther")]
        public virtual decimal AmountOther { get; set; }

        [XmlElement("PortalLink")]
        [DataMember(Name = "PortalLink")]
        public virtual string PortalLink { get; set; }

        [XmlElement("Extra")]
        [DataMember(Name = "Extra")]
        public virtual string Extra { get; set; }

        //Tổng tiền phí dịch vụ
        [XmlElement("Extra1")]
        [DataMember(Name = "Extra1")]
        public virtual string Extra1 { get; set; }

        public virtual int? NumQuantity { get; set; }
        public virtual int? NumPrice { get; set; }
        public virtual int? NumAmount { get; set; }

        //[XmlIgnore]
        [XmlArray("Products")]
        [DataMember(Name = "Products", Order = 27)]
        public virtual List<ProductInv> ProductList
        {
            get
            {
                return (from pr in Products select pr as ProductInv).ToList();
            }
        }

        public override string SerializeToXML()
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                //van den Einvoice
                XmlNode ContentInv = null;
                if (this.Type == InvoiceType.ForReplace)
                    ContentInv = xmlDoc.CreateElement("ReplaceInv");
                else if (this.Type == InvoiceType.ForAdjustAccrete || this.Type == InvoiceType.ForAdjustInfo || this.Type == InvoiceType.ForAdjustReduce)
                    ContentInv = xmlDoc.CreateElement("AdjustInv");
                else
                    ContentInv = xmlDoc.CreateElement("Invoice");
                if (ContentInv != null) xmlDoc.AppendChild(ContentInv);
                XmlNode key = xmlDoc.CreateElement("key");
                key.InnerText = this.Fkey;
                ContentInv.AppendChild(key);
                XmlNode ComAddress = xmlDoc.CreateElement("ComAddress");
                ComAddress.InnerText = this.ComAddress;
                ContentInv.AppendChild(ComAddress);

                //van de ve customer 
                XmlNode CusCode = xmlDoc.CreateElement("CusCode");
                CusCode.InnerText = this.CusCode;
                ContentInv.AppendChild(CusCode);

                XmlNode CusName = xmlDoc.CreateElement("CusName");
                CusName.InnerText = this.CusName;
                ContentInv.AppendChild(CusName);

                XmlNode Buyer = xmlDoc.CreateElement("Buyer");
                Buyer.InnerText = this.Buyer;
                ContentInv.AppendChild(Buyer);

                XmlNode CusAddress = xmlDoc.CreateElement("CusAddress");
                CusAddress.InnerText = this.CusAddress;
                ContentInv.AppendChild(CusAddress);

                XmlNode CusPhone = xmlDoc.CreateElement("CusPhone");
                CusPhone.InnerText = this.CusPhone;
                ContentInv.AppendChild(CusPhone);

                XmlNode CusEmail = xmlDoc.CreateElement("CusEmail");
                CusEmail.InnerText = this.CusEmail;
                ContentInv.AppendChild(CusEmail);

                XmlNode CusTaxCode = xmlDoc.CreateElement("CusTaxCode");
                CusTaxCode.InnerText = this.CusTaxCode;
                ContentInv.AppendChild(CusTaxCode);

                XmlNode CusBankNo = xmlDoc.CreateElement("CusBankNo");
                CusBankNo.InnerText = this.CusBankNo;
                ContentInv.AppendChild(CusBankNo);

                XmlNode PaymentMethod = xmlDoc.CreateElement("PaymentMethod");
                PaymentMethod.InnerText = this.PaymentMethod;
                ContentInv.AppendChild(PaymentMethod);

                XmlNode Extra = xmlDoc.CreateElement("Extra");
                Extra.InnerText = this.Extra;
                ContentInv.AppendChild(Extra);

                XmlNode Extra1 = xmlDoc.CreateElement("Extra1");
                Extra1.InnerText = this.Extra1;
                ContentInv.AppendChild(Extra1);

                //end customer
                //product
                //van de ve san pham
                XmlNode Products = xmlDoc.CreateElement("Products");
                ContentInv.AppendChild(Products);
                //khoi tao du lieu da co
                //Insert tung phan tu vao product
                foreach (var el in this.ProductList)
                {
                    XmlNode Product = xmlDoc.CreateElement("Product");
                    Products.AppendChild(Product);

                    //begin product
                    XmlNode ProdCode = xmlDoc.CreateElement("Code");
                    ProdCode.InnerText = el.Code;
                    Product.AppendChild(ProdCode);

                    XmlNode ProdName = xmlDoc.CreateElement("ProdName");
                    ProdName.InnerText = el.Name;
                    Product.AppendChild(ProdName);

                    XmlNode ProdPrice = xmlDoc.CreateElement("ProdPrice");
                    ProdPrice.InnerText = el.Price.ToString();
                    Product.AppendChild(ProdPrice);

                    XmlNode ProdQuantity = xmlDoc.CreateElement("ProdQuantity");
                    ProdQuantity.InnerText = el.Quantity.ToString();
                    Product.AppendChild(ProdQuantity);

                    XmlNode ProdUnit = xmlDoc.CreateElement("ProdUnit");
                    ProdUnit.InnerText = el.Unit;
                    Product.AppendChild(ProdUnit);

                    XmlNode pTotal = xmlDoc.CreateElement("Total");
                    pTotal.InnerText = el.Total.ToString();
                    Product.AppendChild(pTotal);

                    XmlNode VATRate = xmlDoc.CreateElement("VATRate");
                    VATRate.InnerText = el.VATRate.ToString();
                    Product.AppendChild(VATRate);

                    XmlNode VATAmount = xmlDoc.CreateElement("VATAmount");
                    VATAmount.InnerText = el.VATAmount.ToString();
                    Product.AppendChild(VATAmount);

                    XmlNode Amount = xmlDoc.CreateElement("Amount");
                    Amount.InnerText = el.Amount.ToString();
                    Product.AppendChild(Amount);

                    XmlNode Remark = xmlDoc.CreateElement("Remark");
                    Remark.InnerText = el.Remark;
                    Product.AppendChild(Remark);

                    XmlNode ExtraProd = xmlDoc.CreateElement("Extra");
                    ExtraProd.InnerText = el.Extra;
                    Product.AppendChild(ExtraProd);
                }
                //end product
                //other
                //tong tien dich vu
                XmlNode Total = xmlDoc.CreateElement("Total");
                Total.InnerText = this.Total.ToString();
                ContentInv.AppendChild(Total);

                //thue gia tri gia tang  VAT_Rate
                XmlNode VATOther = xmlDoc.CreateElement("VATOther");
                VATOther.InnerText = this.VATOther.ToString();
                ContentInv.AppendChild(VATOther);

                XmlNode ChangeFees = xmlDoc.CreateElement("ChangeFees");
                ChangeFees.InnerText = this.ChangeFees.ToString();
                ContentInv.AppendChild(ChangeFees);

                XmlNode ExtraFees = xmlDoc.CreateElement("ExtraFees");
                ExtraFees.InnerText = this.ExtraFees.ToString();
                ContentInv.AppendChild(ExtraFees);

                XmlNode OtherFees = xmlDoc.CreateElement("OtherFees");
                OtherFees.InnerText = this.OtherFees.ToString();
                ContentInv.AppendChild(OtherFees);

                XmlNode VATRateOther = xmlDoc.CreateElement("VATRateOther");
                VATRateOther.InnerText = this.VATRateOther.ToString();
                ContentInv.AppendChild(VATRateOther);

                XmlNode VATAmountOther = xmlDoc.CreateElement("VATAmountOther");
                VATAmountOther.InnerText = this.VATAmountOther.ToString();
                ContentInv.AppendChild(VATAmountOther);

                XmlNode AmountOther = xmlDoc.CreateElement("AmountOther");
                AmountOther.InnerText = this.AmountOther.ToString();
                ContentInv.AppendChild(AmountOther);

                // tong tien VAT_Amount
                XmlNode VAT_Amount = xmlDoc.CreateElement("VATAmount");
                VAT_Amount.InnerText = this.VATAmount.ToString();
                ContentInv.AppendChild(VAT_Amount);

                //Amount
                XmlNode amount = xmlDoc.CreateElement("Amount");
                amount.InnerText = this.Amount.ToString();
                ContentInv.AppendChild(amount);

                //hien thi so tien bang chu
                XmlNode Amount_words = xmlDoc.CreateElement("AmountInWords");
                Amount_words.InnerText = AmountInWords;
                ContentInv.AppendChild(Amount_words);

                //kieu hoa don
                XmlNode Type = xmlDoc.CreateElement("Type");
                Type.InnerText = ((int)this.Type).ToString();
                ContentInv.AppendChild(Type);

                //ngay phat hanh hoa don
                XmlNode ArisingDate = xmlDoc.CreateElement("ArisingDate");
                ArisingDate.InnerText = this.ArisingDate.ToString("dd/MM/yyyy");
                ContentInv.AppendChild(ArisingDate);

                //Ghi chu hoa don                
                XmlNode Note = xmlDoc.CreateElement("Note");
                Note.InnerText = this.Note;
                ContentInv.AppendChild(Note);

                return xmlDoc.OuterXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ProductInv deserializeProduct(XElement el)
        {
            ProductInv p = new ProductInv();
            if (el.Element("Code") != null) p.Code = el.Element("Code").Value;
            if (el.Element("ProdName") != null) p.Name = el.Element("ProdName").Value;
            if (el.Element("ProdPrice") != null && !string.IsNullOrWhiteSpace(el.Element("ProdPrice").Value))
                p.Price = Decimal.Parse(el.Element("ProdPrice").Value);
            if (el.Element("ProdQuantity") != null && !string.IsNullOrWhiteSpace(el.Element("ProdQuantity").Value))
                p.Quantity = Decimal.Parse(el.Element("ProdQuantity").Value);
            if (el.Element("ProdUnit") != null) p.Unit = el.Element("ProdUnit").Value;
            if (el.Element("Total") != null && !string.IsNullOrWhiteSpace(el.Element("Total").Value))
                p.Total = Decimal.Parse(el.Element("Total").Value);
            if (el.Element("Amount") != null && !string.IsNullOrWhiteSpace(el.Element("Amount").Value))
                p.Amount = Decimal.Parse(el.Element("Amount").Value);
            if (el.Element("Discount") != null && !string.IsNullOrWhiteSpace(el.Element("Discount").Value))
                p.Discount = Convert.ToInt32(el.Element("Discount").Value);
            if (el.Element("VATAmount") != null && !string.IsNullOrWhiteSpace(el.Element("VATAmount").Value))
                p.VATAmount = Decimal.Parse(el.Element("VATAmount").Value);
            if (el.Element("DiscountAmount") != null && !string.IsNullOrWhiteSpace(el.Element("DiscountAmount").Value))
                p.DiscountAmount = Decimal.Parse(el.Element("DiscountAmount").Value);
            if (p.Total == 0)
                p.Total = p.Amount;
            if (el.Element("Extra") != null) p.Extra = el.Element("Extra").Value;
            if (el.Element("IsSum") != null && !string.IsNullOrWhiteSpace(el.Element("IsSum").Value))
                p.IsSum = Boolean.Parse(el.Element("IsSum").Value);
            if (el.Element("Remark") != null) p.Remark = el.Element("Remark").Value;
            p.ProdType = 1;
            return p;
        }

        public override void DeserializeFromXML(string XML, A_Company company)
        {
            ComID = company.Id;
            ComName = company.Name;
            ComNameAlias = company.ComNameAlias;
            ComBankName = company.BankName;
            ComBankNo = company.BankNumber;
            ComTaxCode = company.TaxCode;
            ComPhone = company.Phone;
            ComFax = company.Fax;
            ComAddress = company.Address;
            ComEmail = company.Email;
            PortalLink = company.PortalLink;

            XElement elem = XElement.Parse(XML);
            if (elem.Element("Fkey") != null)
                Fkey = elem.Element("Fkey").Value;

            if (elem.Element("ComPhone") != null)
                ComPhone = elem.Element("ComPhone").Value;
            if (elem.Element("ComBankName") != null)
                ComBankName = elem.Element("ComBankName").Value;
            if (elem.Element("ComBankNo") != null)
                ComBankNo = elem.Element("ComBankNo").Value;
            if (elem.Element("Fkey") != null)
                Fkey = elem.Element("Fkey").Value;

            if (elem.Element("CusName") != null)
                CusName = elem.Element("CusName").Value;
            if (elem.Element("Buyer") != null)
                Buyer = elem.Element("Buyer").Value;
            if (elem.Element("CusEmail") != null)
                CusEmail = elem.Element("CusEmail").Value;
            if (elem.Element("CusAddress") != null)
                CusAddress = elem.Element("CusAddress").Value;
            if (elem.Element("CusCode") != null)
                CusCode = elem.Element("CusCode").Value;
            if (elem.Element("CusTaxCode") != null)
                CusTaxCode = elem.Element("CusTaxCode").Value;
            CusCode = CusCode ?? CusTaxCode;
            if (elem.Element("CusBankName") != null)
                CusBankName = elem.Element("CusBankName").Value;
            if (elem.Element("CusBankNo") != null)
                CusBankNo = elem.Element("CusBankNo").Value;
            if (elem.Element("CusPhone") != null)
                CusPhone = elem.Element("CusPhone").Value;

            if (elem.Element("PaymentMethod") != null)
                PaymentMethod = elem.Element("PaymentMethod").Value;
            if (elem.Element("AmountInWords") != null)
                AmountInWords = elem.Element("AmountInWords").Value;
            if (elem.Element("Name") != null)
                Name = elem.Element("Name").Value;
            if (elem.Element("Serial") != null)
                Serial = elem.Element("Serial").Value;
            if (elem.Element("Pattern") != null)
                Pattern = elem.Element("Pattern").Value;
            if (elem.Element("CreateDate") != null)
                CreateDate = DateTime.Parse(elem.Element("CreateDate").Value);
            if (elem.Element("PublishDate") != null)
                PublishDate = DateTime.Parse(elem.Element("PublishDate").Value);

            if (elem.Element("VATAmount") != null && !string.IsNullOrWhiteSpace(elem.Element("VATAmount").Value))
                VATAmount = Decimal.Parse(elem.Element("VATAmount").Value);
            if (elem.Element("Amount") != null && !string.IsNullOrWhiteSpace(elem.Element("Amount").Value))
                Amount = Decimal.Parse(elem.Element("Amount").Value);
            if (elem.Element("Total") != null && !string.IsNullOrWhiteSpace(elem.Element("Total").Value))
                Total = Decimal.Parse(elem.Element("Total").Value);

            if (elem.Element("VATOther") != null && !string.IsNullOrWhiteSpace(elem.Element("VATOther").Value))
                VATOther = Decimal.Parse(elem.Element("VATOther").Value);

            if (elem.Element("ChangeFees") != null && !string.IsNullOrWhiteSpace(elem.Element("ChangeFees").Value))
                ChangeFees = Decimal.Parse(elem.Element("ChangeFees").Value);

            if (elem.Element("ExtraFees") != null && !string.IsNullOrWhiteSpace(elem.Element("ExtraFees").Value))
                ExtraFees = Decimal.Parse(elem.Element("ExtraFees").Value);

            if (elem.Element("OtherFees") != null && !string.IsNullOrWhiteSpace(elem.Element("OtherFees").Value))
                OtherFees = Decimal.Parse(elem.Element("OtherFees").Value);

            if (elem.Element("VATAmountOther") != null && !string.IsNullOrWhiteSpace(elem.Element("VATAmountOther").Value))
                VATAmountOther = Decimal.Parse(elem.Element("VATAmountOther").Value);

            if (elem.Element("VATRateOther") != null && !string.IsNullOrWhiteSpace(elem.Element("VATRateOther").Value))
                VATRateOther = Decimal.Parse(elem.Element("VATRateOther").Value);

            if (elem.Element("VATAmountOther") != null && !string.IsNullOrWhiteSpace(elem.Element("VATAmountOther").Value))
                VATAmountOther = Decimal.Parse(elem.Element("VATAmountOther").Value);

            if (elem.Element("PaymentStatus") != null && !string.IsNullOrWhiteSpace(elem.Element("PaymentStatus").Value))
                PaymentStatus = (Payment)Convert.ToInt32(elem.Element("PaymentStatus").Value);
            if (elem.Element("ArisingDate") != null && !string.IsNullOrWhiteSpace(elem.Element("ArisingDate").Value))
                ArisingDate = DateTime.ParseExact(elem.Element("ArisingDate").Value, "dd/MM/yyyy", new CultureInfo("en-US"));
            else
                ArisingDate = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", new CultureInfo("en-US"));

            if (elem.Element("Note") != null)
                Note = elem.Element("Note").Value;

            if (elem.Element("Extra") != null)
                Extra = elem.Element("Extra").Value;

            if (elem.Element("Extra1") != null)
                Extra1 = elem.Element("Extra1").Value;

            XElement XeleProd = elem.Element("Products");
            IEnumerable<XElement> plist = (from c in XeleProd.Elements("Product") select c).ToList<XElement>();

            foreach (XElement el in plist)
            {
                ProductInv p = deserializeProduct(el);
                this.Products.Add(p);
            }
            if (VATAmount == 0)
                VATAmount = Products.Sum(p => p.VATAmount);
            if (Total == 0)
                Total = Products.Sum(p => p.Total);
            if (elem.Element("VATRate") != null)
                VATRate = Convert.ToInt32(elem.Element("VATRate").Value);
            GrossValue5 = VatAmount5 = GrossValue10 = VatAmount10 = GrossValue0 = VatAmount0 = GrossValue = 0;

            if (VATRate == 5)
            {
                GrossValue5 = Total;
                VatAmount5 = VATAmount;
            }
            if (VATRate == 10)
            {
                GrossValue10 = Total;
                VatAmount10 = VATAmount;
            }
            if (VATRate == 0)
            {
                GrossValue0 = Total;
                VatAmount0 = VATAmount;
            }
            if (VATRate == -1)
                GrossValue = Total;
            //Thuế dịch vụ
            if (VATRateOther == 5)
            {
                GrossValue5 += ChangeFees;
                VatAmount5 += VATAmountOther;
            }
            if (VATRateOther == 10)
            {
                GrossValue10 += ChangeFees;
                VatAmount10 += VATAmountOther;
            }
            if (VATRateOther == 0)
            {
                GrossValue0 += ChangeFees;
                VatAmount0 += VATAmountOther;
            }
            if (VATRateOther == -1)
                GrossValue += ChangeFees;
        }

        public override void DeserializeFromJSON(string Json, A_Company currentCom)
        {
            ComID = currentCom.Id;
            ComName = currentCom.Name;
            ComNameAlias = currentCom.ComNameAlias;
            ComBankName = currentCom.BankName;
            ComBankNo = currentCom.BankNumber;
            ComTaxCode = currentCom.TaxCode;
            ComPhone = currentCom.Phone;
            ComFax = currentCom.Fax;
            ComEmail = currentCom.Email;
            ComAddress = currentCom.Address;
            PortalLink = currentCom.PortalLink;

            Dictionary<string, object> dictInvoice = JsonConvert.DeserializeObject<Dictionary<string, object>>(Json);
            if (dictInvoice.Keys.Contains("ComBankName") && !string.IsNullOrWhiteSpace(dictInvoice["ComBankName"].ToString()))
                ComBankName = dictInvoice["ComBankName"].ToString();
            if (dictInvoice.Keys.Contains("ComBankNo") && !string.IsNullOrWhiteSpace(dictInvoice["ComBankNo"].ToString()))
                ComBankNo = dictInvoice["ComBankNo"].ToString();

            if (dictInvoice.Keys.Contains("Fkey"))
                Fkey = dictInvoice["Fkey"].ToString();
            if (dictInvoice.Keys.Contains("InvoiceNo"))
                No = Decimal.Parse(dictInvoice["InvoiceNo"].ToString());
            if (dictInvoice.Keys.Contains("Name"))
                Name = dictInvoice["Name"].ToString();
            if (dictInvoice.Keys.Contains("Serial"))
                Serial = dictInvoice["Serial"].ToString();
            if (dictInvoice.Keys.Contains("Pattern"))
                Pattern = dictInvoice["Pattern"].ToString();

            if (dictInvoice.Keys.Contains("CusName"))
                CusName = dictInvoice["CusName"].ToString().Trim();
            if (dictInvoice.Keys.Contains("Buyer"))
                Buyer = dictInvoice["Buyer"].ToString().Trim();
            if (dictInvoice.Keys.Contains("CusAddress"))
                CusAddress = dictInvoice["CusAddress"].ToString().Trim();
            if (dictInvoice.Keys.Contains("CusEmail"))
                CusEmail = dictInvoice["CusEmail"].ToString();
            if (dictInvoice.Keys.Contains("CusCode"))
                CusCode = dictInvoice["CusCode"].ToString();
            if (dictInvoice.Keys.Contains("CusTaxCode"))
                CusTaxCode = dictInvoice["CusTaxCode"].ToString();
            CusCode = CusCode ?? CusTaxCode;
            if (dictInvoice.Keys.Contains("CusBankName"))
                CusBankName = dictInvoice["CusBankName"].ToString();
            if (dictInvoice.Keys.Contains("CusBankNo"))
                CusBankNo = dictInvoice["CusBankNo"].ToString();
            if (dictInvoice.Keys.Contains("CusPhone"))
                CusPhone = dictInvoice["CusPhone"].ToString();
            if (dictInvoice.Keys.Contains("PaymentMethod"))
                PaymentMethod = dictInvoice["PaymentMethod"].ToString();
            if (dictInvoice.Keys.Contains("AmountInWords"))
                AmountInWords = dictInvoice["AmountInWords"].ToString();

            if (dictInvoice.Keys.Contains("VATAmount"))
            {
                decimal vatamount = 0;
                if (!Decimal.TryParse(dictInvoice["VATAmount"].ToString(), out vatamount))
                    throw new Exception("VATAmount: Error Format-" + dictInvoice["VATAmount"].ToString());
                VATAmount = vatamount;
            }

            if (dictInvoice.Keys.Contains("Amount"))
            {
                decimal amount = 0;
                if (!Decimal.TryParse(dictInvoice["Amount"].ToString(), out amount))
                    throw new Exception("Amount: Error Format-" + dictInvoice["Amount"].ToString());
                Amount = amount;
            }

            if (dictInvoice.Keys.Contains("Total"))
            {
                decimal total = 0;
                if (!Decimal.TryParse(dictInvoice["Total"].ToString(), out total))
                    throw new Exception("Total: Error Format-" + dictInvoice["Total"].ToString());
                Total = total;
            }

            if (dictInvoice.Keys.Contains("VATOther"))
            {
                decimal vatother = 0;
                if (!Decimal.TryParse(dictInvoice["VATOther"].ToString(), out vatother))
                    throw new Exception("VATOther: Error Format-" + dictInvoice["VATOther"].ToString());
                VATOther = vatother;
            }

            if (dictInvoice.Keys.Contains("ChangeFees"))
            {
                decimal changeFees = 0;
                if (!Decimal.TryParse(dictInvoice["ChangeFees"].ToString(), out changeFees))
                    throw new Exception("ChangeFees: Error Format-" + dictInvoice["ChangeFees"].ToString());
                ChangeFees = changeFees;
            }

            if (dictInvoice.Keys.Contains("ExtraFees"))
            {
                decimal extraFees = 0;
                if (!Decimal.TryParse(dictInvoice["ExtraFees"].ToString(), out extraFees))
                    throw new Exception("ExtraFees: Error Format-" + dictInvoice["ExtraFees"].ToString());
                ExtraFees = extraFees;
            }

            if (dictInvoice.Keys.Contains("OtherFees"))
            {
                decimal otherFees = 0;
                if (!Decimal.TryParse(dictInvoice["OtherFees"].ToString(), out otherFees))
                    throw new Exception("OtherFees: Error Format-" + dictInvoice["OtherFees"].ToString());
                OtherFees = otherFees;
            }

            if (dictInvoice.Keys.Contains("VATRateOther"))
            {
                decimal vatRateOther = 0;
                if (!Decimal.TryParse(dictInvoice["VATRateOther"].ToString(), out vatRateOther))
                    throw new Exception("VATRateOther: Error Format-" + dictInvoice["VATRateOther"].ToString());
                VATRateOther = vatRateOther;
            }

            if (dictInvoice.Keys.Contains("VATAmountOther"))
            {
                decimal vatAmountOther = 0;
                if (!Decimal.TryParse(dictInvoice["VATAmountOther"].ToString(), out vatAmountOther))
                    throw new Exception("VATAmountOther: Error Format-" + dictInvoice["VATAmountOther"].ToString());
                VATAmountOther = vatAmountOther;
            }

            if (dictInvoice.Keys.Contains("AmountOther"))
            {
                decimal amountOther = 0;
                if (!Decimal.TryParse(dictInvoice["AmountOther"].ToString(), out amountOther))
                    throw new Exception("AmountOther: Error Format-" + dictInvoice["AmountOther"].ToString());
                AmountOther = amountOther;
            }

            if (dictInvoice.Keys.Contains("PaymentStatus"))
                PaymentStatus = (Payment)Convert.ToInt32(dictInvoice["PaymentStatus"].ToString());
            if (dictInvoice.Keys.Contains("ArisingDate"))
                ArisingDate = DateTime.ParseExact(dictInvoice["ArisingDate"].ToString(), new string[] { "dd/MM/yyyy", "MM/dd/yyyy", "dd-MM-yyyy", "MM/dd/yyyy hh:mm tt", "MM/dd/yyyy HH:mm:ss" }, new CultureInfo("en-US"), DateTimeStyles.None);

            if (dictInvoice.Keys.Contains("Note"))
                Note = dictInvoice["Note"].ToString();

            if (dictInvoice.Keys.Contains("Extra"))
                Extra = dictInvoice["Extra"].ToString();

            if (dictInvoice.Keys.Contains("Extra1"))
                Extra1 = dictInvoice["Extra1"].ToString();

            IList<ProductInv> products = JsonConvert.DeserializeObject<IList<ProductInv>>(dictInvoice["Products"].ToString());
            foreach (ProductInv p in products)
            {
                p.Name = p.Name.Trim();
                p.ProdType = 1;
                this.Products.Add(p);
            }

            if (VATAmount == 0)
                VATAmount = Products.Sum(p => p.VATAmount);
            if (Total == 0)
                Total = Products.Sum(p => p.Total);
            if (dictInvoice.Keys.Contains("VATRate"))
                VATRate = Convert.ToInt32(dictInvoice["VATRate"]);

            if (VATRate == 5)
            {
                GrossValue5 = Total;
                VatAmount5 = VATAmount;
            }
            if (VATRate == 10)
            {
                GrossValue10 = Total;
                VatAmount10 = VATAmount;
            }
            if (VATRate == 0)
            {
                GrossValue0 = Total;
                VatAmount0 = VATAmount;
            }
            if (VATRate == -1)
                GrossValue = Total;

            //Thuế dịch vụ
            if (VATRateOther == 5)
            {
                GrossValue5 += ChangeFees;
                VatAmount5 += VATAmountOther;
            }
            if (VATRateOther == 10)
            {
                GrossValue10 += ChangeFees;
                VatAmount10 += VATAmountOther;
            }
            if (VATRateOther == 0)
            {
                GrossValue0 += ChangeFees;
                VatAmount0 += VATAmountOther;
            }
            if (VATRateOther == -1)
                GrossValue += ChangeFees;
        }

        public override decimal GrossValue { get; set; }
        public override decimal GrossValue0 { get; set; }
        public override decimal VatAmount0 { get; set; }
        public override decimal GrossValue5 { get; set; }
        public override decimal VatAmount5 { get; set; }
        public override decimal GrossValue10 { get; set; }
        public override decimal VatAmount10 { get; set; }

        public override string GetXMLData(A_Company company)
        {
            ComID = company.Id;
            Fkey = !string.IsNullOrWhiteSpace(Fkey) ? Fkey : Utils.GenUnique();
            if (string.IsNullOrWhiteSpace(SearchKey))
                SearchKey = Utils.GenSearchKey(ComID);
            string urlPub = (company.Config != null && company.Config.ContainsKey("PublishDomain")) ?
                company.Config["PublishDomain"] : string.Empty;
            if (string.IsNullOrEmpty(urlPub))
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    urlPub = "http://localhost:8990";
                else
                    urlPub = string.Empty;
            }

            string PItext = urlPub + "/InvoiceTemplate/GetXSLTbyPattern?id=" + company.TaxCode + "&amp;pattern=" + Pattern;
            if (string.IsNullOrWhiteSpace(ComBankName))
                ComBankName = company.BankName;
            if (string.IsNullOrWhiteSpace(ComBankNo))
                ComBankNo = company.BankNumber;

            int NumTotal = Currency == "VND" ? 0 : 2;
            StringBuilder strbd = new StringBuilder();
            strbd.AppendFormat("<?xml-stylesheet type='text/xsl' href='{0}' ?>", PItext);
            strbd.Append("<Invoice><Content Id=\"SigningData\">");
            string arisingDate = this.ArisingDate == Enumerations.MinDate ? string.Empty : this.ArisingDate.ToString("dd/MM/yyyy");
            strbd.AppendFormat("<ArisingDate>{0}</ArisingDate>", arisingDate);
            strbd.AppendFormat("<ComFax>{0}</ComFax>", ComFax);
            strbd.AppendFormat("<SearchKey>{0}</SearchKey>", SearchKey);
            strbd.AppendFormat("<Fkey>{0}</Fkey>", Fkey);
            strbd.AppendFormat("<Note>{0}</Note>", ParseChar(Note));
            strbd.AppendFormat("<ParentName>{0}</ParentName>", ParentName);
            strbd.AppendFormat("<InvoiceName>{0}</InvoiceName>", Name);
            strbd.AppendFormat("<InvoicePattern>{0}</InvoicePattern>", Pattern);
            strbd.AppendFormat("<SerialNo>{0}</SerialNo>", Serial);
            strbd.AppendFormat("<InvoiceNo>{0}</InvoiceNo>", No);
            strbd.AppendFormat("<PaymentMethod>{0}</PaymentMethod>", PaymentMethod);
            strbd.AppendFormat("<ComName>{0}</ComName>", ParseChar(ComName));
            strbd.AppendFormat("<ComNameAlias>{0}</ComNameAlias>", ParseChar(ComNameAlias));
            strbd.AppendFormat("<ComTaxCode>{0}</ComTaxCode>", ComTaxCode);
            strbd.AppendFormat("<ComAddress>{0}</ComAddress>", ParseChar(ComAddress));
            strbd.AppendFormat("<ComPhone>{0}</ComPhone>", ComPhone);
            strbd.AppendFormat("<ComBankNo>{0}</ComBankNo>", ParseChar(ComBankNo));
            strbd.AppendFormat("<ComBankName>{0}</ComBankName>", ParseChar(ComBankName));

            strbd.AppendFormat("<BranchCode>{0}</BranchCode>", ParseChar(BranchCode));
            //strbd.AppendFormat("<BranchAddress>{0}</BranchAddress>", ParseChar(BranchAddress));
            //strbd.AppendFormat("<BranchName>{0}</BranchName>", ParseChar(BranchName));
            //strbd.AppendFormat("<BranchPhone>{0}</BranchPhone>", BranchPhone);
            strbd.AppendFormat("<Buyer>{0}</Buyer>", Buyer);
            strbd.AppendFormat("<CusCode>{0}</CusCode>", CusCode);
            strbd.AppendFormat("<CusName>{0}</CusName>", ParseChar(CusName));
            strbd.AppendFormat("<CusTaxCode>{0}</CusTaxCode>", CusTaxCode);
            strbd.AppendFormat("<CusPhone>{0}</CusPhone>", CusPhone);
            strbd.AppendFormat("<CusEmail>{0}</CusEmail>", CusEmail);
            strbd.AppendFormat("<CusAddress>{0}</CusAddress>", ParseChar(CusAddress));
            strbd.AppendFormat("<CusBankName>{0}</CusBankName>", ParseChar(CusBankName));
            strbd.AppendFormat("<CusBankNo>{0}</CusBankNo>", ParseChar(CusBankNo));

            strbd.AppendFormat("<Total>{0}</Total>", NumAmount.HasValue ? NumberFormat(Total, NumAmount) : Total.ToString());

            #region Sương Mai
            strbd.AppendFormat("<VATOther>{0}</VATOther>", VATOther);//Thuế khác
            strbd.AppendFormat("<ChangeFees>{0}</ChangeFees>", ChangeFees);
            strbd.AppendFormat("<ExtraFees>{0}</ExtraFees>", ExtraFees = Total + VATAmount + VATOther + OtherFees);

            strbd.AppendFormat("<VATRateOther>{0}</VATRateOther>", VATRateOther);
            strbd.AppendFormat("<VATAmountOther>{0}</VATAmountOther>", VATAmountOther);
            strbd.AppendFormat("<AmountOther>{0}</AmountOther>", AmountOther = ChangeFees + VATAmountOther);
            #endregion

            #region Binhnc: 29/05/2020 : Edit lởm
            strbd.AppendFormat("<TotalVATFeesOther>{0}</TotalVATFeesOther>", Amount = VATOther + OtherFees);
            strbd.AppendFormat("<AmountAfterTax>{0}</AmountAfterTax>", Amount = Total + VATAmount);
            #endregion


            strbd.AppendFormat("<VATRate>{0}</VATRate>", VATRate);
            strbd.AppendFormat("<VATAmount>{0}</VATAmount>", NumAmount.HasValue ? NumberFormat(VATAmount, NumAmount) : VATAmount.ToString());
            strbd.AppendFormat("<Amount>{0}</Amount>", NumAmount.HasValue ? NumberFormat(Amount, NumTotal) : Amount.ToString());
            strbd.AppendFormat("<AmountInWords>{0}</AmountInWords>", !string.IsNullOrEmpty(AmountInWords) ? AmountInWords.Split(';')[0] : "");
            strbd.AppendFormat("<AmountInWordsEnglish>{0}</AmountInWordsEnglish>", (!string.IsNullOrEmpty(AmountInWords) && AmountInWords.Split(';').Count() == 2) ? AmountInWords.Split(';')[1] : "");
            strbd.AppendFormat("<ExchangeRate>{0}</ExchangeRate>", ExchangeRate);
            //strbd.AppendFormat("<TyGia>{0}</TyGia>", TyGia);

            //Giám Định Thăng Long 0311795895
            strbd.AppendFormat("<Extra>{0}</Extra>", ParseChar(Extra));
            strbd.AppendFormat("<Extra1>{0}</Extra1>", ParseChar(Extra1));

            //Chiết khấu
            strbd.AppendFormat("<OtherFees>{0}</OtherFees>", OtherFees);//Phí khác

            strbd.Append("<Products>");

            foreach (var product in ProductList)
            {
                strbd.Append("<Product>");
                strbd.AppendFormat("<Code>{0}</Code>", product.Code);
                strbd.AppendFormat("<Extra>{0}</Extra>", ParseChar(product.Extra));
                strbd.AppendFormat("<ProdName>{0}</ProdName>", ParseChar(product.Name));
                strbd.AppendFormat("<ProdPrice>{0}</ProdPrice>", NumPrice.HasValue ? NumberFormat(product.Price, NumPrice) : product.Price.ToString());
                strbd.AppendFormat("<ProdQuantity>{0}</ProdQuantity>", NumQuantity.HasValue ? NumberFormat(product.Quantity, NumQuantity) : product.Quantity.ToString());
                strbd.AppendFormat("<ProdType>{0}</ProdType>", product.ProdType);
                strbd.AppendFormat("<ProdUnit>{0}</ProdUnit>", product.Unit);
                strbd.AppendFormat("<Remark>{0}</Remark>", product.Remark);
                strbd.AppendFormat("<Total>{0}</Total>", product.Total);
                strbd.AppendFormat("<VATAmount>{0}</VATAmount>", product.VATAmount);
                strbd.AppendFormat("<VATRate>{0}</VATRate>", product.VATRate);
                strbd.AppendFormat("<Discount>{0}</Discount>", product.Discount);
                strbd.AppendFormat("<DiscountAmount>{0}</DiscountAmount>", product.DiscountAmount);
                strbd.AppendFormat("<IsSum>{0}</IsSum>", product.IsSum);
                strbd.AppendFormat("<Amount>{0}</Amount>", NumAmount.HasValue ? NumberFormat(product.Amount, NumAmount) : product.Amount.ToString());
                strbd.Append("</Product>");
            }
            strbd.Append("</Products>");

            strbd.AppendFormat("<GrossValue>{0}</GrossValue>", GrossValue);
            strbd.AppendFormat("<GrossValue0>{0}</GrossValue0>", GrossValue0);
            strbd.AppendFormat("<VatAmount0>{0}</VatAmount0>", VatAmount0);
            strbd.AppendFormat("<GrossValue5>{0}</GrossValue5>", GrossValue5);
            strbd.AppendFormat("<VatAmount5>{0}</VatAmount5>", VatAmount5);
            strbd.AppendFormat("<GrossValue10>{0}</GrossValue10>", GrossValue10);
            strbd.AppendFormat("<VatAmount10>{0}</VatAmount10>", VatAmount10);
            strbd.Append("</Content></Invoice>");
            XmlDocument xdoc = new XmlDocument();
            xdoc.PreserveWhitespace = true;
            xdoc.LoadXml(strbd.ToString());


            if (this.Type == InvoiceType.ForReplace)
            {
                XmlNode xn = xdoc.GetElementsByTagName("Content")[0].AppendChild(xdoc.CreateElement("isReplace"));
                //xn.InnerText = this.ProcessInvNote;
                xn.InnerText = ProcessInvNote;
            }

            else if (this.Type == InvoiceType.ForAdjustAccrete || this.Type == InvoiceType.ForAdjustInfo || this.Type == InvoiceType.ForAdjustReduce)
            {
                XmlNode xn = xdoc.GetElementsByTagName("Content")[0].AppendChild(xdoc.CreateElement("isAdjust"));
                xn.InnerText = ProcessInvNote;
            }

            XmlNode xnd = xdoc.GetElementsByTagName("Invoice")[0].AppendChild(xdoc.CreateElement("qrCodeData"));
            var qrCodeData = string.Format("{0}|{1};{2};{3};{4};{5};{6};{7};{8};{9}", this.Fkey, this.Pattern, this.Serial, this.No, this.ComTaxCode, this.CusTaxCode, this.Amount, this.VATAmount, this.ArisingDate, this.ComID);
            xnd.InnerText = qrCodeData;

            return xdoc.OuterXml;
        }

        public static string NumberFormat(decimal number, int? countDecimal)
        {
            try
            {
                if (number == 0 && !countDecimal.HasValue)
                {
                    return "0";
                }
                else
                {
                    int count = countDecimal.HasValue ? countDecimal.Value : 2;
                    var format = Switch(count);
                    var newFormat = number.ToString(format);
                    //var result = string.Join("", newFormat.Select(x => x == ',' ? '.' :
                    //                                        x == '.' ? ',' :
                    //                                        x));
                    //return result;
                    return newFormat;
                }

            }
            catch (Exception ex)
            {
                return "Lỗi convert";
            }

        }

        private static string Switch(int countDecimal)
        {
            switch (countDecimal)
            {
                case 0:
                    return "##0";
                case 1:
                    return "##0.0";
                case 2:
                    return "##0.00";
                default:
                    string key = new string('#', countDecimal - 2);
                    return "##0.0" + key + "0";

            }
        }

        public static string ParseChar(string data)
        {
            if (!string.IsNullOrWhiteSpace(data))
            {
                data = data.Replace("&", "&amp;");
                data = data.Replace(">", "&gt;");
                data = data.Replace("<", "&lt;");
                data = data.Replace("%", "&#37;");
                data = data.Replace("'", "&apos;");
                return data;
            }
            return data;
        }
    }
}
