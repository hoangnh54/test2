﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Core.Entities.Customer.Interfaces
{
    [DataContract(Namespace = "")]
    public abstract class InvoiceBase : IInvoice
    {
        private DateTime _CreateDate = DateTime.Now;
        private DateTime _ModifiedDate = DateTime.Today;
        private DateTime _PublishDate = DateTime.Today;
        private IList<AdjustInv> _Adjusts = new List<AdjustInv>();
        private DateTime _ArisingDate = Enumerations.MinDate;

        /// <summary>
        /// id được gọi đến khi muốn sửa,thay thế hoặc hủy bỏ hóa đơn
        /// </summary>
        [XmlElement("id")]
        [IgnoreDataMember]
        public virtual int Id { get; set; }

        /// <summary>
        /// Id link đến Category của hóa đơn --- các loại hóa đơn khác nhau
        /// </summary>
        [IgnoreDataMember]
        [XmlIgnore]
        public virtual int InvCateID { get; set; }

        /// <summary>
        /// tên hóa đơn : VD Hóa đơn dịch vụ viễn thông
        /// </summary>    
        [XmlElement("Name")]
        [DataMember(Name = "InvoiceName")]
        public virtual string Name { get; set; }

        //[XmlElement("ArisingDate")]
        //[DataMember(Name = "ArisingDate", Order = 28)]
        public virtual DateTime ArisingDate
        {
            get { return _ArisingDate; }
            set { _ArisingDate = value; }
        }
        [DataMember(Name = "ArisingDate")]
        private string PerformanceDateSerialized { get; set; }

        [OnSerializing]
        void OnSerializing(StreamingContext context)
        {
            this.PerformanceDateSerialized = this.ArisingDate.ToString("dd/MM/yyyy");
        }

        [IgnoreDataMember]
        [XmlIgnore]
        public virtual string ProcessInvNote { get; set; }

        [XmlElement("Trans")]
        [DataMember(Name = "Trans")]
        public virtual string Trans { get; set; }

        [XmlElement("Note")]
        [DataMember(Name = "Note")]
        public virtual string Note { get; set; }

        /// <summary>
        /// Số hóa đơn VD: 3814976
        /// </summary>        
        [XmlElement("No")]
        [DataMember(Name = "InvoiceNo")]
        public virtual Decimal No { get; set; }

        [XmlIgnore]
        [IgnoreDataMember]
        public virtual DateTime ModifiedDate
        {
            get { return _ModifiedDate; }
            set { _ModifiedDate = value; }
        }
        /// <summary>
        /// Ngay tao hoa don
        /// </summary>        
        [XmlElement("CreateDate")]
        [IgnoreDataMember]
        public virtual DateTime CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }
        /// <summary>
        /// dữ liệu hóa đơn ở dạng XML, bao gồm chữ ký của đơn vị phát hành và khách hàng nếu có
        /// </summary>        
        [IgnoreDataMember]
        [XmlIgnore]
        public virtual string Data { get; set; }

        /// <summary>
        /// ký hiệu hóa đơn : VD  AB/11P
        /// </summary>        
        [XmlElement("Serial")]
        [DataMember(Name = "SerialNo")]
        public virtual string Serial { get; set; }

        /// <summary>
        /// Mẫu hóa đơn : VD 01GTKT2/001
        /// </summary>        
        [XmlElement("Pattern")]
        [DataMember(Name = "InvoicePattern")]
        public virtual string Pattern { get; set; }

        /// <summary>
        /// id link đến đối tượng company (công ty, tổ chức phát hành hóa đơn)
        /// </summary>
        [XmlIgnore]
        [IgnoreDataMember]
        public virtual int ComID { get; set; }

        /// <summary>
        /// tên đơn vị phát hành hóa đơn
        /// </summary>       
        [XmlElement("ComName")]
        [DataMember(Name = "ComName")]
        public virtual string ComName { get; set; }
        /// <summary>
        /// Tên công ty tiếng anh
        /// </summary>       
        [XmlElement("ComNameAlias")]
        [DataMember(Name = "ComNameAlias")]
        public virtual string ComNameAlias { get; set; }

        [XmlElement("ParentName")]
        [DataMember(Name = "ParentName")]
        public virtual string ParentName { get; set; }
        /// <summary>
        /// số điện thoại đơn vị phát hành hóa đơn
        /// </summary>        
        [XmlElement("ComPhone")]
        [DataMember(Name = "ComPhone")]
        public virtual string ComPhone { get; set; }

        [XmlElement("ComFax")]
        [DataMember(Name = "ComFax")]
        public virtual string ComFax { get; set; }
        /// <summary>
        /// địa chỉ đơn vị phát hành hóa đơn
        /// </summary>        
        [XmlElement("ComAddress")]
        [DataMember(Name = "ComAddress")]
        public virtual string ComAddress { get; set; }

        /// <summary>
        /// mã số thuế của đơn vị phát hành hóa đơn
        /// </summary>        
        [XmlElement("ComTaxCode")]
        [DataMember(Name = "ComTaxCode")]
        public virtual string ComTaxCode { get; set; }

        /// <summary>
        /// Tên ngân hàng -- đơn vị phát hành mở tài khoản
        /// </summary>        
        [XmlElement("ComBankName")]
        [DataMember(Name = "ComBankName")]
        public virtual string ComBankName { get; set; }
        /// <summary>
        /// số tài khoản ngân hàng của đơn vị phát hành
        /// </summary>        
        [XmlElement("ComBankNo")]
        [DataMember(Name = "ComBankNo")]
        public virtual string ComBankNo { get; set; }

        [XmlElement("Buyer")]
        [DataMember(Name = "Buyer")]
        public virtual string Buyer { get; set; }
        /// <summary>
        /// mã khách hàng
        /// </summary>        
        [XmlElement("CusCode")]
        [DataMember(Name = "CusCode")]
        public virtual string CusCode { get; set; }

        [XmlElement("CusEmail")]
        [DataMember(Name = "CusEmail")]
        public virtual string CusEmail { get; set; }

        /// <summary>
        /// tên khách hàng
        /// </summary>        
        [XmlElement("CusName")]
        [DataMember(Name = "CusName")]
        public virtual string CusName { get; set; }

        /// <summary>
        /// địa chỉ khách hàng
        /// </summary>       
        [XmlElement("CusAddress")]
        [DataMember(Name = "CusAddress")]
        public virtual string CusAddress { get; set; }
        /// <summary>
        /// số điện thoại khách hàng
        /// </summary>        
        [XmlElement("CusPhone")]
        [DataMember(Name = "CusPhone")]
        public virtual string CusPhone { get; set; }
        /// <summary>
        /// tên ngân hàng - khách hàng mở tài khoản
        /// </summary>        
        [XmlElement("CusBankName")]
        [DataMember(Name = "CusBankName")]
        public virtual string CusBankName { get; set; }
        /// <summary>
        /// số tài khoản ngân hàng của khách hàng
        /// </summary>        
        [XmlElement("CusBankNo")]
        [DataMember(Name = "CusBankNo")]
        public virtual string CusBankNo { get; set; }
        /// <summary>
        /// Mã số thuế của khách hàng
        /// </summary>        
        [XmlElement("CusTaxCode")]
        [DataMember(Name = "CusTaxCode")]
        public virtual string CusTaxCode { get; set; }
        /// <summary>
        /// loại hóa đơn : 0: hoa don thong thuong 1: Hoa don thay the 2: hoa don dieu chinh sai soi
        /// </summary>
        [XmlElement("Type")]
        [IgnoreDataMember]
        public virtual InvoiceType Type { get; set; }

        /// <summary>
        /// trạng thái hóa đơn
        /// </summary>
        [XmlElement("Status")]
        [IgnoreDataMember]
        public virtual InvoiceStatus Status { get; set; }
        /// <summary>
        /// Trạng thái chuyển đổi từ hóa đơn điện tử sang hóa đơn giấy
        /// </summary>
        [XmlElement("ConversionStatus")]
        [IgnoreDataMember]
        public virtual int Converted { get; set; } = 0;
        /// <summary>
        /// phương thức thanh toán hóa đơn
        /// </summary>        
        [XmlElement("PaymentMethod")]
        [DataMember(Name = "PaymentMethod")]
        public virtual string PaymentMethod { get; set; }
        /// <summary>
        /// ngày phát hành hóa đơn
        /// </summary>        
        [XmlElement("PublishDate")]
        [IgnoreDataMember]
        public virtual DateTime PublishDate
        {
            get { return _PublishDate; }
            set { _PublishDate = value; }
        }

        /// <summary>
        /// tình trạng thanh toán
        /// </summary>
        [XmlElement("PaymentStatus")]
        [IgnoreDataMember]
        public virtual Payment PaymentStatus { get; set; }

        /// <summary>
        /// danh sách thay thế
        /// </summary>
        [XmlIgnore]
        [IgnoreDataMember]
        [JsonIgnoreAttribute]
        public virtual IList<AdjustInv> Adjusts
        {
            get { return _Adjusts; }
            set { _Adjusts = value; }
        }
        /// <summary>
        /// Trạng thái ký của khách hàng
        /// </summary>
        [XmlIgnore]
        [IgnoreDataMember]
        public virtual CusSignStatus CusSignStatus { get; set; }

        /// <summary>
        /// Người tạo hóa đơn
        /// </summary>
        [XmlElement("CreateBy")]
        [IgnoreDataMember]
        public virtual string CreateBy { get; set; }
        /// <summary>
        /// Người phát hành hóa đơn
        /// </summary>
        [XmlIgnore]
        [IgnoreDataMember]
        public virtual string PublishBy { get; set; }


        [XmlElement("SearchKey")]
        [DataMember(Name = "SearchKey")]
        public virtual string SearchKey { get; set; }

        [XmlElement("Fkey")]
        [DataMember(Name = "Fkey")]
        public virtual string Fkey { get; set; }

        [XmlElement("ExchangeRate")]
        [DataMember(Name = "ExchangeRate")]
        public virtual string ExchangeRate { get; set; }

        [XmlElement("Currency")]
        [DataMember(Name = "Currency")]
        public virtual string Currency { get; set; }

        /// <summary>
        /// Tổng tiền trước thuế
        /// </summary>    
        [XmlElement("Total")]
        [DataMember(Name = "Total")]
        public virtual Decimal Total { get; set; }

        [XmlElement("VATAmount")]
        [DataMember(Name = "VATAmount")]
        public virtual decimal VATAmount { get; set; }
        /// <summary>
        /// tổng tiền hóa đơn (sau thuế)
        /// </summary>        
        [XmlElement("Amount")]
        [DataMember(Name = "Amount")]
        public virtual Decimal Amount { get; set; }
        /// <summary>
        /// tổng tiền bằng chữ
        /// </summary>        
        [XmlElement("AmountInWords")]
        [DataMember(Name = "AmountInWords")]
        public virtual string AmountInWords { get; set; }

        /// <summary>
        /// Xây dựng dữ liệu XML biểu diễn thông tin hóa đơn, không bao gồm chữ ký số
        /// Với mỗi loại Hóa đơn khác nhau với thành phần thông tin khác nhau nên viết lại hàm này
        /// </summary>
        /// <returns>XML string biểu diễn thông tin hóa đơn</returns>
        public virtual string GetXMLData(A_Company company)
        {
            List<Type> knownTypeList = new List<Type>();
            knownTypeList.Add(typeof(ProductInv));
            Fkey = !string.IsNullOrWhiteSpace(Fkey) ? Fkey : Utils.GenUnique();
            if (string.IsNullOrWhiteSpace(SearchKey))
                SearchKey = Utils.GenSearchKey(company.Id);

            DataContractSerializer dcs = new DataContractSerializer(this.GetType(), knownTypeList);

            MemoryStream ms = new MemoryStream();
            dcs.WriteObject(ms, this);

            XmlDocument xdoc = new XmlDocument();
            xdoc.XmlResolver = null;
            xdoc.PreserveWhitespace = true;
            xdoc.LoadXml(Encoding.UTF8.GetString(ms.GetBuffer()));

            XmlProcessingInstruction newPI;
            String PItext = "type='text/xsl' href='";
            string urlPub = string.Empty;
            if (System.Diagnostics.Debugger.IsAttached)
                urlPub = "http://localhost:8990";
            else
                //urlPub = UrlUtil.GetSiteUrl();
                urlPub = string.Empty;

            PItext += urlPub + "/InvoiceTemplate/GetXSLTbyPattern?id=" + company.TaxCode + "&amp;pattern=" + Pattern + "'";
            newPI = xdoc.CreateProcessingInstruction("xml-stylesheet", PItext);
            xdoc.InsertBefore(newPI, xdoc.DocumentElement);

            XDocument xd = Utils.GetXDocument(xdoc);
            Utils.RemoveAttribte(xd);
            Utils.RemoveNamespace(xd);

            xdoc = Utils.GetXmlDocument(xd);

            XmlNodeList xlist = xdoc.DocumentElement.ChildNodes;
            XmlNode newnode = xdoc.DocumentElement.AppendChild(xdoc.CreateElement("Content"));
            XmlAttribute xa1 = xdoc.CreateAttribute("Id");
            xa1.Value = "SigningData";
            newnode.Attributes.Append(xa1);
            for (int i = 0; i < xlist.Count - 1; i++)
            {
                newnode.AppendChild(xlist[i]);
                i--;
            }
            xdoc.DocumentElement.RemoveAll();
            xdoc.DocumentElement.AppendChild(newnode);

            if (this.Type == InvoiceType.ForReplace)
            {
                XmlNode xn = xdoc.GetElementsByTagName("Content")[0].AppendChild(xdoc.CreateElement("isReplace"));
                xn.InnerText = this.ProcessInvNote;
            }

            else if (this.Type == InvoiceType.ForAdjustAccrete || this.Type == InvoiceType.ForAdjustInfo || this.Type == InvoiceType.ForAdjustReduce)
            {
                XmlNode xn = xdoc.GetElementsByTagName("Content")[0].AppendChild(xdoc.CreateElement("isAdjust"));
                xn.InnerText = this.ProcessInvNote;
            }
            XmlNode xnd = xdoc.GetElementsByTagName("Invoice")[0].AppendChild(xdoc.CreateElement("qrCodeData"));
            var qrCodeData = string.Format("{0}|{1};{2};{3};{4};{5};{6};{7};{8};{9}", this.Fkey, this.Pattern, this.Serial, this.No, this.ComTaxCode, this.CusTaxCode, this.Amount, this.VATAmount, this.ArisingDate, this.ComID);
            xnd.InnerText = qrCodeData;
            return xdoc.OuterXml;
        }

        public virtual string SerializeToXML() { return ""; }

        public virtual void DeserializeFromXML(string XML, A_Company company)
        {
            throw new Exception("this function is not implement");
        }

        public virtual void DeserializeFromJSON(string Json, A_Company company)
        {
            throw new Exception("this function is not implement");
        }

        public abstract decimal GrossValue { get; set; }
        public abstract decimal GrossValue0 { get; set; }
        public abstract decimal VatAmount0 { get; set; }
        public abstract decimal GrossValue5 { get; set; }
        public abstract decimal VatAmount5 { get; set; }
        public abstract decimal GrossValue10 { get; set; }

        public abstract decimal VatAmount10 { get; set; }

        /*
        public abstract decimal TradeDiscount0 { get; set; }
        public abstract decimal TradeDiscount5 { get; set; }
        public abstract decimal TradeDiscount10 { get; set; }
        public abstract decimal PlusAmountAfterDiscount0 { get; set; }
        public abstract decimal PlusAmountAfterDiscount5 { get; set; }
        public abstract decimal PlusAmountAfterDiscount10 { get; set; }
        public abstract decimal AmountVAT0 { get; set; }
        public abstract decimal AmountVAT5 { get; set; }
        public abstract decimal AmountVAT10 { get; set; }
        */

        public virtual bool Certified { get; set; }
        public virtual string CertifiedID { get; set; }
        public virtual string CertifiedData { get; set; }

        [XmlIgnore]
        [IgnoreDataMember]
        public virtual string AttachPath { get; set; }

        [XmlElement("BranchCode")]
        [IgnoreDataMember]
        public virtual string BranchCode { get; set; }

        [XmlIgnore]
        public virtual IList<ProductInv> Products { get; set; }
    }
}
