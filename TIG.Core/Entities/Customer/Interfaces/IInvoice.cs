﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Core.Entities.Customer.Interfaces
{
    public interface IInvoice
    {
        int Id { get; set; }
        int InvCateID { get; set; }
        int ComID { get; set; }
        string Name { get; set; }
        Decimal No { get; set; }
        string Data { get; set; }
        string Serial { get; set; }
        string Pattern { get; set; }
        DateTime CreateDate { get; set; }
        DateTime ModifiedDate { get; set; }
        DateTime PublishDate { get; set; }
        string Buyer { get; set; }
        string CusName { get; set; }
        string CusCode { get; set; }
        string CusPhone { get; set; }
        string CusAddress { get; set; }
        string CusTaxCode { get; set; }
        string CusEmail { get; set; }
        string PaymentMethod { get; set; }
        InvoiceStatus Status { get; set; }
        Payment PaymentStatus { get; set; }
        int Converted { get; set; }
        IList<ProductInv> Products { get; set; }
        CusSignStatus CusSignStatus { get; set; }
        string CreateBy { get; set; }
        string PublishBy { get; set; }
        string Note { get; set; }
        string AttachPath { get; set; }
        string ProcessInvNote { get; set; }
        string BranchCode { get; set; }
        /// <summary>
        /// Tổng tiền hóa đơn sau thuế
        /// </summary>
        Decimal Amount { get; set; }
        /// <summary>
        /// Tổng tiền hóa đơn trước thuế
        /// </summary>
        decimal Total { get; set; }
        /// <summary>
        /// Tổng tiền bằng chữ
        /// </summary>
        string AmountInWords { get; set; }
        /// <summary>
        /// Tổng tiền VAT các mức
        /// </summary>
        decimal VATAmount { get; set; }
        /// <summary>
        /// Số tiền không chịu thuế
        /// </summary>
        decimal GrossValue { get; set; }
        /// <summary>
        /// Tổng tiền trước VAT 0
        /// </summary>
        decimal GrossValue0 { get; set; }
        /// <summary>
        /// Số tiền VAT 0
        /// </summary>
        decimal VatAmount0 { get; set; }
        /// <summary>
        /// Tổng tiền trước VAT 5
        /// </summary>
        decimal GrossValue5 { get; set; }
        /// <summary>
        /// Số tiền VAT 5
        /// </summary>
        decimal VatAmount5 { get; set; }
        /// <summary>
        /// Tổng tiền trước VAT 10
        /// </summary>
        decimal GrossValue10 { get; set; }
        /// <summary>
        /// Số tiền VAT 10
        /// </summary>
        decimal VatAmount10 { get; set; }

        InvoiceType Type { get; set; }
        DateTime ArisingDate { get; set; }
        string Fkey { get; set; }
        string SearchKey { get; set; }

        string GetXMLData(A_Company company);
        string SerializeToXML();
        void DeserializeFromXML(string XML, A_Company company);
        void DeserializeFromJSON(string Json, A_Company company);

        /// <summary>
        /// Thuoc tinh them vao cho phan xac thuc voi cuc thue
        /// </summary>
        bool Certified { get; set; }
        string CertifiedID { get; set; }
        string CertifiedData { get; set; }
    }
}
