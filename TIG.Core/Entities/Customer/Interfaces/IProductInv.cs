﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Core.Entities.Customer.Interfaces
{
    public interface IProductInv
    {
        int ComID { get; set; }
        Guid Id { get; set; }
        string Code { get; set; }
        string Name { get; set; }
        string Unit { get; set; }
        Decimal Quantity { get; set; }
        Decimal Price { get; set; }
        Decimal Amount { get; set; }
        double VATRate { get; set; }
        Decimal VATAmount { get; set; }
        Decimal Total { get; set; }
        bool IsSum { get; set; }
        DateTime? CreateDate { get; set; }
        int OrderBy { get; set; }
        int InvID { get; set; }
    }
}
