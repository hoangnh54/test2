﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Entities.Customer
{
    public class C_UserRole
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public virtual C_Users User { get; set; }
        public virtual C_Role Role { get; set; }

    }
}
