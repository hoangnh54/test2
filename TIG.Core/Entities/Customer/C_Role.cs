﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TIG.Core.Entities.Customer
{
    public class C_Role
    {
        [Column("roleid")]
        public int Id { get; set; }
        [Column("name")]
        public string Name { get; set; }
        [Column("Title")]
        public string Title { get; set; }
        [Column("GroupId")]
        public string Group { get; set; }
        public Boolean IsSysAdmin { get; set; }
        public virtual IList<C_Role_Permission> Role_Permissions { get; set; }
        public virtual IList<C_UserRole> UserRoles { get; set; }
    }
}
