﻿using System;
using System.Collections.Generic;
using TIG.Core.Common;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Core.Entities.Customer
{
    public class C_VATInvoice
    {
        public int Id { get; set; }
        public int InvCateID { get; set; }
        public int ComID { get; set; }
        public string Name { get; set; }
        public Decimal No { get; set; }
        public string Data { get; set; }
        public string Serial { get; set; }
        public string Pattern { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public DateTime PublishDate { get; set; }
        public string Buyer { get; set; }
        public string CusName { get; set; }
        public string CusCode { get; set; }
        public string CusPhone { get; set; }
        public string CusAddress { get; set; }
        public string CusTaxCode { get; set; }
        public string CusEmail { get; set; }
        public string PaymentMethod { get; set; }
        public virtual InvoiceStatus Status { get; set; }
        public virtual Payment PaymentStatus { get; set; }
        public bool Converted { get; set; }
        public virtual IList<ProductInv> Products { get; set; }
        public CusSignStatus CusSignStatus { get; set; }
        public string CreateBy { get; set; }
        public string PublishBy { get; set; }
        public string Note { get; set; }
        public string AttachPath { get; set; }
        public string ProcessInvNote { get; set; }
        public string BranchCode { get; set; }
        /// <summary>
        /// Tổng tiền hóa đơn sau thuế
        /// </summary>
        public Decimal Amount { get; set; }
        /// <summary>
        /// Tổng tiền hóa đơn trước thuế
        /// </summary>
        public decimal Total { get; set; }
        /// <summary>
        /// Tổng tiền bằng chữ
        /// </summary>
        public string AmountInWords { get; set; }
        /// <summary>
        /// Tổng tiền VAT các mức
        /// </summary>
        public decimal VATAmount { get; set; }
        /// <summary>
        /// Số tiền không chịu thuế
        /// </summary>
        public decimal GrossValue { get; set; }
        /// <summary>
        /// Tổng tiền trước VAT 0
        /// </summary>
        public decimal GrossValue0 { get; set; }
        /// <summary>
        /// Số tiền VAT 0
        /// </summary>
        public decimal VatAmount0 { get; set; }
        /// <summary>
        /// Tổng tiền trước VAT 5
        /// </summary>
        public decimal GrossValue5 { get; set; }
        /// <summary>
        /// Số tiền VAT 5
        /// </summary>
        public decimal VatAmount5 { get; set; }
        /// <summary>
        /// Tổng tiền trước VAT 10
        /// </summary>
        public decimal GrossValue10 { get; set; }
        /// <summary>
        /// Số tiền VAT 10
        /// </summary>
        public decimal VatAmount10 { get; set; }

        public InvoiceType Type { get; set; }
        public DateTime ArisingDate { get; set; }
        public string Fkey { get; set; }
        public string SearchKey { get; set; }

        /// <summary>
        /// Thuoc tinh them vao cho phan xac thuc voi cuc thue
        /// </summary>
        public bool Certified { get; set; }
        public string CertifiedID { get; set; }
        public string CertifiedData { get; set; }

        /// <summary>
        /// For Certificate only, not for save to Database
        /// </summary>
        public string CertifiedOriginalID { get; set; }
        public string ComNameAlias { get; set; }
        public string OfficeAddress { get; set; }
    }
}
