﻿using Microsoft.EntityFrameworkCore;
using TIG.Core.Entities.Customer.Interfaces;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Core.Entities.Customer
{
    public partial class C_Context : DbContext
    {
        public C_Context(DbContextOptions<C_Context> options)
              : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<C_Permission>(e =>
            {
                e.ToTable("permission")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<C_Role>(e =>
            {
                e.ToTable("role")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();

                e.HasMany(x => x.Role_Permissions)
                .WithOne(x => x.Role)
                .HasForeignKey(j => j.RoleId);

                e.HasMany(j => j.UserRoles)
                .WithOne(j => j.Role)
                .HasForeignKey(j => j.RoleId);
            });

            modelBuilder.Entity<C_Role_Permission>(e =>
            {
                e.ToTable("role_permission")
                .HasKey(x => new { x.RoleId, x.PermissionId });

                e.HasOne<C_Role>(sc => sc.Role)
                .WithMany(s => s.Role_Permissions)
                .HasForeignKey(sc => sc.RoleId);

                e.HasOne<C_Permission>(sc => sc.Permission)
                .WithMany(s => s.Role_Permissions)
                .HasForeignKey(sc => sc.PermissionId);
            });

            modelBuilder.Entity<C_UserRole>(e =>
            {
                e.ToTable("user_role")
                .HasKey(x => new { x.UserId, x.RoleId });

                e.HasOne<C_Users>(sc => sc.User)
                .WithMany(s => s.UserRoles)
                .HasForeignKey(sc => sc.UserId);

                e.HasOne<C_Role>(sc => sc.Role)
                .WithMany(s => s.UserRoles)
                .HasForeignKey(sc => sc.RoleId);
            });

            modelBuilder.Entity<C_Users>(e =>
            {
                e.ToTable("userdata")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();

                e.HasMany(j => j.UserRoles)
                .WithOne(j => j.User)
                .HasForeignKey(j => j.UserId);
            });

            modelBuilder.Entity<C_PublishInvoice>(e =>
            {
                e.ToTable("PublishInvoice")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();
            });

            //modelBuilder.Entity<C_VATInvoice>(e =>
            //{
            //    e.ToTable("VATInvoice")
            //    .HasKey(k => k.Id);

            //    e.Property(p => p.Id)
            //    .ValueGeneratedOnAdd();

            //    e.HasMany<ProductInv>(x => x.Products)
            //    .WithOne(x => x.VATInvoice)
            //    .HasForeignKey(k => k.IDInv);
            //});

            #region VATInvoice base
            modelBuilder.Entity<InvoiceVAT>(e =>
            {
                e.ToTable("VATInvoice")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();

                e.HasMany<ProductInv>(x => x.Products)
                .WithOne(x => x.InvoiceVAT)
                .HasForeignKey(k => k.InvID);
            });


            modelBuilder.Entity<ProductInv>(e =>
            {
                e.ToTable("ProductInv")
                .HasKey(k => k.Id);

                e.Property(p => p.Id)
                .ValueGeneratedOnAdd();

                //e.HasOne<InvoiceVAT>(x => x.InvoiceVAT)
                //.WithMany(x => x.Products)
                //.HasForeignKey(k => k.InvID);
            });
            #endregion

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);


        public virtual DbSet<C_Permission> Permission { get; set; }
        public virtual DbSet<C_Role> Role { get; set; }
        public virtual DbSet<C_Role_Permission> Role_Permission { get; set; }
        public virtual DbSet<C_UserRole> UserRole { get; set; }
        public virtual DbSet<C_Users> User { get; set; }
        //public virtual DbSet<C_VATInvoice> VATInvoice { get; set; }
        public virtual DbSet<C_PublishInvoice> PublishInvoice { get; set; }

        public virtual DbSet<InvoiceVAT> InvoiceVAT { get; set; }
        //public virtual DbSet<ExtendsVATInvoice> ExtendsVATInvoice { get; set; }

        public virtual DbSet<ProductInv> ProductInv { get; set; }


    }
}
