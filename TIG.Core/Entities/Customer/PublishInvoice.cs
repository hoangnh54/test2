﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Text.Json.Serialization;
using TIG.Core.Common;

namespace TIG.Core.Entities.Customer
{
    public class C_PublishInvoice
    {
        private DateTime _StartDate = DateTime.Now;
        private DateTime _EndDate = Enumerations.MinDate;

        public virtual int Status { get; set; }

        public virtual string InvSerialSuffix { get; set; }

        public virtual string InvSerial { get; set; }

        public virtual string InvSerialPrefix { get; set; }

        public virtual Decimal CurrentNo { get; set; }
        [Column("id")]
        public virtual int Id { get; set; }

        public virtual int PublishID { get; set; }

        public virtual Decimal Quantity { get; set; }

        public virtual Decimal FromNo { get; set; }

        public virtual Decimal ToNo { get; set; }

        public virtual DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }

        public virtual int RegisterID { get; set; }

        public virtual string RegisterName { get; set; }

        public virtual DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        [JsonIgnoreAttribute]
        public virtual int ComId { get; set; }

        public virtual string InvPattern { get; set; }

        public virtual string InvCateName { get; set; }

        public virtual string TIGName { get; set; } = "Công ty cổ phần Hóa đơn điện tử TIG Thăng Long";

        public virtual string TIGTax { get; set; } = "0102720409";
    }
}
