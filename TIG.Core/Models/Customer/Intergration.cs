﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Models.Customer
{
    public class RegisterCertificateModel
    {
        public string CertBase64 { get; set; }
        public string CertSerial { get; set; }
    }

    public class LaunchModels
    {
        public int[] Ids { get; set; }
        public string Pattern { get; set; }
        public string Serial { get; set; }
        public string Certificate { get; set; }
    }
    public class HashData
    {
        public string Key { get; set; }
        public string Hash { get; set; }
    }
}
