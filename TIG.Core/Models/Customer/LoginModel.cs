﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Models.Customer
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string TaxCode { get; set; }
    }
}
