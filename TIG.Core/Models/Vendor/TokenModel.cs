﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Models.Vendor
{
    public class TokenModel
    {
        public string Id { get; set; }
        public string Accesstoken { get; set; }
        public DateTime? ExpiredAt { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
    }
}
