﻿using System.Collections.Generic;
using System.Dynamic;

namespace TIG.Core.Models.Vendor
{
    public class UserModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public List<string> Permissions { get; set; }
    }

    public class Company_UserModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TaxCode { get; set; }
    }

    public class Company_RegisterTemp
    {
        public int Id { get; set; }
        public string InvPattern { get; set; }
        public int TempId { get; set; }
    }
}
