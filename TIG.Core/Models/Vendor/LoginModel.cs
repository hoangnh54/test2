﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Core.Models.Vendor
{
    public class VendorLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
