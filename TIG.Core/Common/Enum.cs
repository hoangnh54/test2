﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TIG.Core.Common
{
    public enum InvoiceType
    {
        /// <summary>
        /// hóa đơn thông thường
        /// </summary>
        [Description("Hóa đơn thông thường")]
        Nomal,
        /// <summary>
        /// hóa đơn thay thế
        /// </summary>
        [Description("Hóa đơn thay thế")]
        ForReplace,
        /// <summary>
        /// hóa đơn điều chỉnh sai sót
        /// </summary>
        /// <summary>
        /// hóa đơn điều chỉnh sai sót tăng
        /// </summary>
        [Description("Hóa đơn điều chỉnh tăng")]
        ForAdjustAccrete,
        /// <summary>
        /// Điều chỉnh sai sót giảm
        /// </summary>
        [Description("Hóa đơn điều chỉnh giảm")]
        ForAdjustReduce,
        /// <summary>
        /// Điều chỉnh thông tin
        /// </summary>
        [Description("Hóa đơn điều chỉnh thông tin")]
        ForAdjustInfo,
        /// <summary>
        /// Hóa đơn gửi đi phát hành
        /// </summary>
        [Description("Hóa đơn xóa bỏ")]
        ForCancel,
        /// <summary>
        /// truong hop null
        /// </summary>
        Null = -1,
    }

    public enum CusSignStatus
    {
        /// <summary>
        /// Chua co chu ky
        /// </summary>
        [Description("Chưa ký hóa đơn")]
        NoSignStatus,
        /// <summary>
        /// Co chu ky
        /// </summary>
        [Description("Đã ký hóa đơn")]
        SignStatus,
        /// <summary>
        /// khach hang khong can ky
        /// </summary>
        [Description("Khách hàng không cần ký")]
        NocusSignStatus,
        /// <summary>
        /// Khách hàng đã xem hóa đơn nhưng chưa ký
        /// </summary>
        [Description("Khách hàng đã xem hóa đơn nhưng chưa ký")]
        ViewNoSignStatus,
        /// <summary>
        /// Đã xem hóa đơn nhưng thuộc loại không cần ký
        /// </summary>
        [Description("Khách hàng đã xem hóa đơn")]
        ViewNocusSignStatus
    }

    public enum Payment
    {
        /// <summary>
        /// chưa thanh toán
        /// </summary>
        [Description("Chưa thanh toán")]
        Unpaid = 0,
        /// <summary>
        ///  đã thanh toán
        /// </summary>
        [Description("Đã thanh toán")]
        Paid = 1
    }

    public enum InvoiceStatus
    {
        /// <summary>
        /// hóa đơn mới tạo lập chưa có chữ ký của nhà phát hành
        /// </summary>
        [Description("Hóa đơn mới tạo lập")]
        NewInv,
        /// <summary>
        /// hóa đơn đã có chứ ký của nhà phát hành
        /// </summary>
        [Description("Hóa đơn có chữ ký số")]
        SignedInv,
        /// <summary>
        /// hóa đơn đã được kê khai thuế cũng như đưa vào các phần mêm kế toán
        /// </summary>
        [Description("Hóa đơn đã khai báo thuế")]
        InUseInv,
        /// <summary>
        /// hóa đơn sai sót bị thay thế
        /// </summary>
        [Description("Hóa đơn bị thay thế")]
        ReplacedInv,
        /// <summary>
        /// hóa đơn sai sót bị điều chỉnh
        /// </summary>
        [Description("Hóa đơn bị điều chỉnh")]
        AdjustedInv,
        /// <summary>
        /// hóa đơn bị hủy
        /// </summary>
        [Description("Hóa đơn bị hủy")]
        CanceledInv,
        ///// <summary>
        ///// Hóa đơn bị hủy do hết thời hạn lưu trữ
        ///// </summary>
        //ExpaireInv
        Null = -1,

    }
    public enum ProcessingStatus
    {
        /// <summary>
        /// hóa đơn sai sót bị thay thế
        /// </summary>
        ReplacedInv,
        /// <summary>
        /// hóa đơn sai sót bị điều chỉnh
        /// </summary>
        AdjustedInv,
        /// <summary>
        /// truong hop null
        /// </summary>
        Null = -1,

    }
    public enum PaymentTransactionStatus
    {
        /// <summary>
        /// Trang thai moi duoc upload file
        /// </summary>
        NewUpload,
        ///<summary>
        ///Trang thai dang xu ly
        ///</summary>
        Processing,
        ///<summary>
        ///Xu ly thanh cong
        ///</summary>
        Completed,
        ///<summary>
        ///Xu ly co chua loi
        ///</summary>
        NotComplete,
        ///<summary>
        ///Loi xu ly
        ///</summary>
        Failed,
        /// <summary>
        /// truong hop null
        /// </summary>
        Null = -1
    }
    public enum PublishStatus
    {
        /// <summary>
        /// mới tạo lập --- có thể sửa xóa
        /// </summary>
        [Description("Mới lập")]
        Newpub,
        /// <summary>
        /// đang đợi phê duyệt từ cơ quan thuế
        /// </summary>
        [Description("Chờ phê duyệt")]
        Waiting,
        /// <summary>
        /// đã được chấp thuận và đưa vào sử dụng
        /// </summary>
        [Description("Được chấp nhận")]
        InUse
    }
    public enum Custype
    {
        /// <summary>
        /// Không phải là đơn vị kế toán
        /// </summary>
        NoAccountant,
        /// <summary>
        /// Là đơn vị kế toán
        /// </summary>
        Accountant
    }
    public enum TranSactionStatus
    {
        /// <summary>
        /// Trang thai moi duoc upload file
        /// </summary>
        NewUpload,
        ///<summary>
        ///Trang thai dang xu ly
        ///</summary>
        Processing,
        ///<summary>
        ///Xu ly thanh cong
        ///</summary>
        Completed,
        ///<summary>
        ///Xu ly co chua loi
        ///</summary>
        NotComplete,
        ///<summary>
        ///Loi xu ly
        ///</summary>
        Failed,
        /// <summary>
        /// truong hop null
        /// </summary>
        Null = -1,
    }
    public enum CompanyType
    {
        [Description("Khách hàng sử dụng tên miền riêng")]
        OnlyDomain = 0,
        [Description("Khách hàng sử dụng tool")]
        Tool = 1,
        [Description("Mô hình công ty mẹ, công ty con")]
        BigDomain = 2,
        [Description("Khách hàng sử dụng web chung")]
        WebCommon = 3
    }

    public class Enumerations
    {
        public static DateTime MinDate
        {
            get { return new DateTime(1957, 1, 1); }
        }
        public static DateTime MaxDate
        {
            get { return DateTime.MaxValue; }
        }
    }
}
