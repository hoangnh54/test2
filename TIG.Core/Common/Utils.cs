﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Reflection;
using System.ComponentModel;

namespace TIG.Core.Common
{
    public static class Utils
    {
        public static string GetEnumDescription(Enum value)
        {
            // Get the Description attribute value for the enum value
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        private const string chars = "abcdefghijklmnopqrtuvwxzy0123456789";
        public static string GenerationUnique(string invPattern, string invSerial, decimal invNo, string comTaxCode)
        {
            string sss = string.Format("{0}{1}{2}{3}", invPattern, invSerial, invNo.ToString("0000000"), comTaxCode);
            return Math.Abs(sss.GetHashCode()).ToString();
        }

        public static string GenUnique()
        {
            Guid g = Guid.NewGuid();
            string guidString = Convert.ToBase64String(g.ToByteArray());
            guidString = guidString.Replace("=", "").Replace("+", "");
            return guidString;
        }

        public static string GenSearchKey(int comid)
        {
            var random = new Random();
            var randomtxt = new string(Enumerable.Repeat(chars, 2).Select(s => s[random.Next(s.Length)]).ToArray());
            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = DateTime.UtcNow - epochStart;
            string timestamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();
            return string.Format("{0}{1}{2}", randomtxt, comid, timestamp);
        }

        public static string GenerationToken(string pattern, string serial, decimal no, string taxcode)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{0}_{1}_{2}|{3}", pattern, serial, no, taxcode)));
        }

        public static byte[] Compress(byte[] data, string filename)
        {
            // Compress
            using MemoryStream fsOut = new MemoryStream();
            using (ICSharpCode.SharpZipLib.Zip.ZipOutputStream zipStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(fsOut))
            {
                zipStream.SetLevel(3);
                ICSharpCode.SharpZipLib.Zip.ZipEntry newEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(filename)
                {
                    DateTime = DateTime.UtcNow,
                    Size = data.Length
                };
                zipStream.PutNextEntry(newEntry);
                zipStream.Write(data, 0, data.Length);
                zipStream.Finish();
                zipStream.Close();
            }
            return fsOut.ToArray();
        }

        public static byte[] Decompress(byte[] dataZip)
        {
            try
            {
                using Stream zipFile = new MemoryStream(dataZip);
                using (ICSharpCode.SharpZipLib.Zip.ZipInputStream ZipStream = new ICSharpCode.SharpZipLib.Zip.ZipInputStream(zipFile))
                {
                    ICSharpCode.SharpZipLib.Zip.ZipEntry theEntry;
                    theEntry = ZipStream.GetNextEntry();
                    if (theEntry.IsFile)
                    {
                        if (theEntry.Name != "")
                        {
                            MemoryStream outputStream = new MemoryStream();
                            ZipStream.CopyTo(outputStream);
                            ZipStream.Close();
                            byte[] output = outputStream.ToArray();
                            outputStream.Close();
                            return output;
                        }
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetTransactionStatus(TranSactionStatus type)
        {
            switch (type)
            {
                case TranSactionStatus.NewUpload:
                    return ("Mới upload");
                case TranSactionStatus.Processing:
                    return ("Đang xử lý");
                case TranSactionStatus.Completed:
                    return ("Thành công");
                case TranSactionStatus.Failed:
                    return ("Lỗi phát hành");
                default:
                    return ("Lỗi phát hành");
            }
        }
        public enum CertificateStatus { Good = 0, Revoked = 1, Unknown = 2, Fail = 3 };
        //0:true 
        //1:chain co revoke hoac isrevoked true
        //2:chain co unknown
        //3:cert verify fail
        public static CertificateStatus VerifyCRL(X509Certificate2 _Cert)
        {
            try
            {
                //messageErr = string.Empty;
                //X509Certificate2 _Cert = new X509Certificate2(CertData);
                X509Chain Chain = new X509Chain();
                Chain.ChainPolicy.RevocationFlag = X509RevocationFlag.ExcludeRoot;
                Chain.ChainPolicy.RevocationMode = X509RevocationMode.Offline;
                //Chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(1000);
                Chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                Chain.ChainPolicy.VerificationTime = DateTime.Now;
                bool _BuildChain = Chain.Build(_Cert);
                if (!_BuildChain)
                {
                    //messageErr = "Cannot build certificates chain";
                }

                X509Store store = new X509Store(StoreName.Disallowed, StoreLocation.LocalMachine);
                store.Open(OpenFlags.ReadWrite);
                bool isRevoked = store.Certificates.Contains(_Cert);
                store.Close();

                bool chainRevoke = false;
                bool chainUnknown = false;
                X509ChainStatus[] certStatus = Chain.ChainStatus;
                foreach (X509ChainStatus x in certStatus)
                {
                    if (x.Status == X509ChainStatusFlags.Revoked)
                    {
                        chainRevoke = true;
                        break;
                    }
                    if (x.Status == X509ChainStatusFlags.Cyclic || x.Status == X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        chainUnknown = true;
                        break;
                    }
                }

                if (_Cert.Verify() && !isRevoked && certStatus.Length == 0)
                    return CertificateStatus.Good;
                else if (isRevoked || chainRevoke)
                    return CertificateStatus.Revoked;
                else if (chainUnknown)
                    return CertificateStatus.Unknown;
                return CertificateStatus.Fail;
            }
            catch (Exception)
            {

            }
            return CertificateStatus.Fail;

        }

        public static string GetNameInvoiceType(InvoiceType type)
        {
            switch (type)
            {
                case InvoiceType.Nomal:
                    return ("Thông thường");
                case InvoiceType.ForReplace:
                    return ("Thay thế");
                case InvoiceType.ForAdjustAccrete:
                    return ("Điều chỉnh tăng");
                case InvoiceType.ForAdjustReduce:
                    return ("Điều chỉnh giảm");
                case InvoiceType.ForAdjustInfo:
                    return ("Điều chỉnh thông tin");
                default:
                    return "";
            }
        }

        public static string GetNameInvoiceStatus(InvoiceStatus type)
        {
            switch (type)
            {
                case InvoiceStatus.NewInv:
                    return "Mới tạo lập";
                case InvoiceStatus.SignedInv:
                    return "Đã phát hành";
                case InvoiceStatus.InUseInv:
                    return "Đã kê khai thuế";
                case InvoiceStatus.ReplacedInv:
                    return "Bị thay thế";
                case InvoiceStatus.AdjustedInv:
                    return "Bị điều chỉnh";
                case InvoiceStatus.CanceledInv:
                    return "Bị xóa bỏ";
                default:
                    return "";
            }
        }


        public static string GetNameInvoiceStatusView(InvoiceStatus type)
        {
            switch (type)
            {
                case InvoiceStatus.NewInv:
                    return ("Mới tạo lập");
                case InvoiceStatus.SignedInv:
                    return ("Phát hành");
                case InvoiceStatus.InUseInv:
                    return ("Đã kê khai thuế");
                case InvoiceStatus.ReplacedInv:
                    return ("Bị thay thế");
                case InvoiceStatus.AdjustedInv:
                    return ("Bị điều chỉnh");
                //case InvoiceStatus.CanceledInv:
                //    return ("Bị hủy");
                //case InvoiceStatus.ExpaireInv:
                //    return ("Hủy hết TG");
                default:
                    return "";
            }
        }


        public static string GetNameInvoicePaymentStatus(Payment type)
        {
            switch (type)
            {
                case Payment.Unpaid:
                    return ("Chưa thanh toán");
                case Payment.Paid:
                    return ("Đã thanh toán");
                default:
                    return "";
            }
        }

        public static string GetNameCusType(Custype custypes)
        {
            switch (custypes)
            {
                case Custype.NoAccountant:
                    return ("Khách hàng không phải là đơn vị kế toán");
                case Custype.Accountant:
                    return ("Khách hàng là đơn vị kế toán ");
                default:
                    return "";
            }
        }

        public static string GetSignconvertonStatus(CusSignStatus type)
        {
            switch (type)
            {
                case CusSignStatus.NoSignStatus:
                    return ("Chưa ký");
                case CusSignStatus.SignStatus:
                    return ("Đã ký");
                case CusSignStatus.NocusSignStatus:
                    return ("Không cần ký");
                default:
                    return "";
            }
        }

        public static string GetNamePublishStatus(PublishStatus pub)
        {
            switch (pub)
            {
                case PublishStatus.Newpub:
                    return ("Mới lập");
                case PublishStatus.Waiting:
                    return ("Đã gửi");
                case PublishStatus.InUse:
                    return ("Chấp nhận");
                default:
                    return "";
            }
        }

        public static string GetNameStatusDec(int status)
        {
            switch (status)
            {
                case 0: return ("Mới lập");
                case 1: return ("Đã gửi");
                case 2: return ("Chấp nhận");
                default: return "";
            }
        }

        public const string MAX_NO_LENGTH = "0000000";
        /// <summary>
        /// Mo ta chuoi so hoa don dang "102,305,1020-5687,6001-7000,7689,10000"
        /// Create by Quanglt
        /// </summary>
        /// <param name="noLst">mang so</param>
        /// <returns>chuoi mo ta</returns>
        //gianglt
        /// Mo ta chuoi so hoa don dang "102;305;1020-5687;6001-7000;7689;10000"
        /// "{0}," -> "{0};"
        /// "{0}, {1}, " -> "{0};{1};"
        /// "{0}-{1}, " -> "{0}-{1};"
        public static string GetNoDescription(decimal[] noLst)
        {
            if (noLst == null || noLst.Length == 0) return "";
            StringBuilder rv = new StringBuilder();
            Array.Sort(noLst);
            decimal firstNo = noLst[0];

            for (int i = 1; i < noLst.Length; i++)
            {
                if (noLst[i] != (noLst[i - 1] + 1))
                {
                    if (firstNo == noLst[i - 1])
                        rv.AppendFormat("{0}; ", firstNo.ToString("0000000"));
                    else
                    {
                        if (firstNo == noLst[i - 1] - 1)
                            rv.AppendFormat("{0};{1}; ", firstNo.ToString("0000000"), noLst[i - 1].ToString("0000000"));
                        else
                            rv.AppendFormat("{0}-{1}; ", firstNo.ToString("0000000"), noLst[i - 1].ToString("0000000"));
                    }
                    firstNo = noLst[i];
                }
            }
            if (firstNo == noLst[noLst.Length - 1]) rv.AppendFormat("{0}", firstNo.ToString("0000000"));
            else
            {
                if (firstNo == noLst[noLst.Length - 1] - 1) rv.AppendFormat("{0}; {1}", firstNo.ToString("0000000"), noLst[noLst.Length - 1].ToString("0000000"));
                else rv.AppendFormat("{0}-{1}", firstNo.ToString("0000000"), noLst[noLst.Length - 1].ToString("0000000"));
            }
            return rv.ToString();
            //gianglt
        }
        public static int GetInt(char ss)
        {
            int a = 0;
            a = Convert.ToInt32(ss) - 48;
            return a;
        }

        public static bool CheckTaxcode(string taxcode)
        {
            if (string.IsNullOrWhiteSpace(taxcode)) return true;
            bool istrue = false;
            if (taxcode.Length != 14 && taxcode.Length != 10 && taxcode.Length != 13) return false;
            if (taxcode.Length == 14 && !taxcode[10].Equals('-')) return false;
            int index = taxcode.Length;
            if (!char.IsNumber(taxcode[index - 1]) || !char.IsNumber(taxcode[index - 2]) || !char.IsNumber(taxcode[index - 3])) return false;
            int value = GetInt(taxcode[0]) * 31 + GetInt(taxcode[1]) * 29 + GetInt(taxcode[2]) * 23 + GetInt(taxcode[3]) * 19 + GetInt(taxcode[4]) * 17 + GetInt(taxcode[5]) * 13 + GetInt(taxcode[6]) * 7 + GetInt(taxcode[7]) * 5 + GetInt(taxcode[8]) * 3;
            int mod = 10 - value % 11;
            if (Math.Abs(mod) == GetInt(taxcode[9]))
                istrue = true;
            return istrue;
        }

        public static XDocument GetXDocument(this XmlDocument document)
        {
            XDocument xDoc = new XDocument();
            using (XmlWriter xmlWriter = xDoc.CreateWriter())
                document.WriteTo(xmlWriter);
            XmlDeclaration decl =
                document.ChildNodes.OfType<XmlDeclaration>().FirstOrDefault();
            if (decl != null)
                xDoc.Declaration = new XDeclaration(decl.Version, decl.Encoding,
                    decl.Standalone);
            return xDoc;
        }


        public static XmlDocument GetXmlDocument(XDocument document)
        {
            using XmlReader xmlReader = document.CreateReader();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(xmlReader);
            if (document.Declaration != null)
            {
                XmlDeclaration dec = xmlDoc.CreateXmlDeclaration(document.Declaration.Version,
                    document.Declaration.Encoding, document.Declaration.Standalone);
                xmlDoc.InsertBefore(dec, xmlDoc.FirstChild);
            }
            return xmlDoc;
        }

        public static void RemoveEmptyNode(XDocument xdoc, string XNodeName)
        {
            var emptyElements = from element in xdoc.Descendants()
                                where element.IsEmpty && element.Name == XNodeName
                                select element;
            while (emptyElements.Any())
                emptyElements.Remove();
        }

        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;

            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }

            return new string(chars);
        }

        public static string CreatePassword(string username, string setPassConfig = null)
        {
            string password = "";
            if (string.IsNullOrWhiteSpace(setPassConfig))
                password = CreateRandomPassword(8);
            else
                password = (setPassConfig == "1") ? username : setPassConfig;
            return password;
        }


        public static XDocument RemoveNamespace(XDocument xdoc)
        {
            foreach (XElement e in xdoc.Root.DescendantsAndSelf())
            {
                if (e.Name.Namespace != XNamespace.None)
                {
                    e.Name = XNamespace.None.GetName(e.Name.LocalName);
                }
                if (e.Attributes().Where(a => a.IsNamespaceDeclaration || a.Name.Namespace != XNamespace.None).Any())
                {
                    e.ReplaceAttributes(e.Attributes().Select(a => a.IsNamespaceDeclaration ? null : a.Name.Namespace != XNamespace.None ? new XAttribute(XNamespace.None.GetName(a.Name.LocalName), a.Value) : a));
                }
            }

            return xdoc;
        }

        //
        public static XDocument RemoveAttribte(XDocument xdoc)
        {
            foreach (XElement e in xdoc.Root.DescendantsAndSelf())
            {
                foreach (XElement ex in e.Descendants())
                    if (ex.HasAttributes)
                    {
                        foreach (XAttribute xa in ex.Attributes())
                            if (xa.Name.LocalName == "nil")
                                xa.Remove();
                    }
            }
            return xdoc;
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        //truongnx
        public static X509Certificate2 GetCertificateBySerial(String serial)
        {
            //X509Store certStore = new X509Store(StoreLocation.CurrentUser);
            X509Store certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            certStore.Open(OpenFlags.ReadOnly);
            foreach (X509Certificate2 certificate in certStore.Certificates)
                if (certificate.GetSerialNumberString().ToUpper().CompareTo(serial.ToUpper().Trim()) == 0)
                {
                    try
                    {
                        System.Security.Cryptography.AsymmetricAlgorithm aa = certificate.PrivateKey;
                    }
                    catch (Exception)
                    {
                        throw new Exception("Không lấy được private key, chọn chứng thư khác!");
                    }
                    return certificate;
                }
            return null;
        }

        /// <summary>
        /// parse chuoi token de lay thong tin id, pattern cua hoa don, thong tin comid va user id
        /// </summary>
        /// <param name="tokenString">chuoi token</param>
        /// <param name="id">id hoa don</param>
        /// <param name="patternString">pattern hoa don</param>
        /// <param name="comid">comid cua company</param>
        /// <returns>true neu chuoi token dung dinh dang</returns>
        //public static bool parseInvoiceTokenString(string tokenString, out string id, out string patternString, out string comid)
        //{
        //    id = "";
        //    patternString = "";
        //    comid = "";
        //    if (string.IsNullOrEmpty(tokenString))
        //        return false;
        //    tokenString = Encoding.UTF8.GetString(Convert.FromBase64String(tokenString));
        //    string[] temp = tokenString.Split(';');
        //    if (temp.Length != 4)
        //        return false;
        //    IuserService _userSVC = ServiceFactory.Resolve<IuserService>();
        //    user us = null;
        //    us = _userSVC.GetByName(temp[temp.Length - 1]);
        //    id = temp[0];
        //    patternString = temp[1];
        //    comid = temp[2];
        //    if (null == us || !us.GroupName.Equals(comid))
        //        return false;
        //    return true;
        //}

        public static string reateInvoiceDownloadLink(string weblink, string id, string patternString, string comid, string username)
        {
            string rv = weblink + "/invoice/getinvoice?";
            //string temp = "id=" + HttpUtility.UrlEncode(id) + "&patternString=" + HttpUtility.UrlEncode(patternString) + "&comid=" + HttpUtility.UrlEncode(comid) + "&userid=" + HttpUtility.UrlEncode(userid);
            //rv = rv + temp;
            string temp = id + ";" + patternString + ";" + comid + ";" + username;
            temp = Convert.ToBase64String(Encoding.UTF8.GetBytes(temp));
            rv = rv + "token=" + HttpUtility.UrlEncode(temp);
            return rv;
        }

        /// <summary>
        /// dang ky de download bien ban
        /// </summary>
        /// <param name="tokenString"></param>
        /// <param name="id"></param>
        /// <param name="patternString"></param>
        /// <param name="comid"></param>
        /// <returns></returns>
        //public static bool parseAjustInvTokenString(string tokenString, out string OInvid, out string NInvid, out string patternString, out string comid)
        //{
        //    OInvid = "";
        //    NInvid = "";
        //    patternString = "";
        //    comid = "";
        //    if (string.IsNullOrEmpty(tokenString))
        //        return false;
        //    tokenString = Encoding.UTF8.GetString(Convert.FromBase64String(tokenString));
        //    string[] temp = tokenString.Split(';');
        //    if (temp.Length != 5)
        //        return false;
        //    IuserService _userSVC = ServiceFactory.Resolve<IuserService>();
        //    user us = null;
        //    us = _userSVC.GetByName(temp[temp.Length - 1]);
        //    OInvid = temp[0];
        //    NInvid = temp[1];
        //    patternString = temp[2];
        //    comid = temp[3];
        //    if (null == us || !us.GroupName.Equals(comid))
        //        return false;
        //    return true;
        //}

        /// <summary>
        /// tao duong link gui vien ban thay the sua doi
        /// </summary>
        /// <param name="weblink"></param>
        /// <param name="id"></param>
        /// <param name="patternString"></param>
        /// <param name="comid"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static string recordsDownloadLink(string weblink, string OInvId, string NInvId, string PatternString, string comid, string username)
        {
            string rv = weblink + "/invoice/Dowloadrecords?";
            string temp = OInvId + ";" + NInvId + ";" + PatternString + ";" + comid + ";" + username;
            temp = Convert.ToBase64String(Encoding.UTF8.GetBytes(temp));
            rv = rv + "token=" + HttpUtility.UrlEncode(temp);
            return rv;
        }

        public static string GetPaymentTransactionStatus(PaymentTransactionStatus type)
        {
            switch (type)
            {
                case PaymentTransactionStatus.NewUpload:
                    return ("Mới upload");
                case PaymentTransactionStatus.Processing:
                    return ("Đang xử lý");
                case PaymentTransactionStatus.Completed:
                    return ("Phát hành thành công");
                case PaymentTransactionStatus.NotComplete:
                    return ("Không hoàn thành");
                case PaymentTransactionStatus.Failed:
                    return ("Lỗi phát hành");
                default:
                    return ("Lỗi phát hành");
            }
        }

        public static string GetContent(string filePath)
        {
            var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            //System.IO.FileStream fs = System.IO.File.OpenRead(filePath);
            byte[] data = new byte[fs.Length];
            int br = fs.Read(data, 0, data.Length);
            if (br != fs.Length)
                throw new System.IO.IOException(filePath);
            var str = System.Text.Encoding.Default.GetString(data);
            return str;
        }

        public static string FormatTaxcode(string code)
        {
            if (code != null && code.Length == 13) return string.Format("{0}-{1}", code.Substring(0, 10), code.Substring(10));
            return code;
        }
    }
}
