﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Security.Cryptography;
using System.Text;

namespace TIG.Core.Common
{
    public enum ErrorCodeMessage
    {
        /// <summary>
        /// Thiếu thông tin yêu cầu
        /// </summary>
        [Description("Thiếu thông tin yêu cầu")]
        Code_1 = 1,
        /// <summary>
        /// Không tìm thấy thông tin mã số thuế
        /// </summary>
        [Description("Không tìm thấy thông tin mã số thuế")]
        Code_2 = 2,
        /// <summary>
        /// Mã số thuế chưa được phê duyệt
        /// </summary>
        [Description("Mã số thuế chưa được phê duyệt")]
        Code_3 = 3,
        /// <summary>
        /// Tài khoản hoặc mật khẩu không đúng
        /// </summary>
        [Description("Tài khoản hoặc mật khẩu không đúng")]
        Code_4 = 4,
        /// <summary>
        /// Không lấy được thông tin chữ ký số gửi lên theo dạng raw,
        /// không detect được thông tin
        /// </summary>
        [Description("Không tìm thấy thông tin ký số")]
        Code_5 = 5,
        /// <summary>
        /// Chứng thư số gửi lên có mst không khớp với mst công ty
        /// </summary>
        [Description("Mã số thuế của chứng thư không hợp lệ")]
        Code_6 = 6,
        /// <summary>
        /// Chữ ký số hết hạn
        /// </summary>
        [Description("Chứng thư số hết hạn")]
        Code_7 = 7,
        /// <summary>
        /// Thiếu thông tin mẫu, ký hiệu hóa đơn
        /// </summary>
        [Description("Thiếu thông tin mẫu, ký hiệu hóa đơn")]
        Code_8 = 8,
        /// <summary>
        /// Chứng thư số chưa được đăng ký
        /// </summary>
        [Description("Chứng thư số chưa được đăng ký")]
        Code_9 = 9,
        /// <summary>
        /// Chữ ký số không khớp với đăng ký
        /// </summary>
        [Description("Chứng thư số không hợp lệ")]
        Code_10 = 10,
        /// <summary>
        /// Ngày hóa đơn không hợp lệ
        /// </summary>
        [Description("Ngày hóa đơn không hợp lệ")]
        Code_11 = 11,
        /// <summary>
        /// Không tìm thấy thông tin mẫu số hóa đơn
        /// </summary>
        [Description("Không tìm thấy thông tin mẫu số hóa đơn")]
        Code_12 = 12,




    }
}
