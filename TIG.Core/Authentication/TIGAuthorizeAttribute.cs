﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using TIG.Core.Models.Customer;

namespace TIG.Core.Authentication
{
    /// <summary>
    /// Check permittion follow by:
    /// User and Role  --> OR
    /// Permission , [Object|Operation]   --> AND
    /// Object|Operation is couple define a Permission
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
    public class TIGAuthorizeAttribute : AuthorizeAttribute, IAuthorizationFilter
    {
        public string Permission { get; set; }
        private string[] Permissions
        {
            get => !string.IsNullOrEmpty(Permission) ? new string[] { } : Permission.Split(',');
        }

        public new virtual string Roles { get; set; }

        public void OnAuthorization(AuthorizationFilterContext filterContext)
        {
            try
            {
                if (filterContext.HttpContext.User == null || filterContext.HttpContext.User.Identity.IsAuthenticated == false)
                {
                    var UserLogon = JsonConvert.DeserializeObject<UserModel>(
                      ((ClaimsIdentity)filterContext.HttpContext.User.Identity).FindFirst("UserData").Value);
                    if (UserLogon != null)
                    {
                        //Lấy quyền ra theo cliams ở đây
                        if (UserLogon.UserName == "root" || UserLogon.UserName == "admin") return;

                        if (!string.IsNullOrEmpty(Permission))
                        {
                            if (UserLogon.Permissions.Any(x => Permissions.Contains(x))) return;
                        }
                        //Check quyền, role
                    }

                    //-------------------------
                    filterContext.Result = new UnauthorizedResult();
                    return;
                }
                return;
            }
            catch (Exception)
            {
                filterContext.Result = new UnauthorizedResult();
            }

        }
    }
}
