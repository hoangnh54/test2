﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Security.Claims;
using TIG.Core.Models.Customer;

namespace TIG.Core.Authentication
{

    public abstract class TIGController : Controller
    {
        protected UserModel UserLogon
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<UserModel>(
                    ((ClaimsIdentity)User.Identity).FindFirst("UserData").Value);
                }
                catch (Exception)
                {

                    return null;
                }

            }
        }
    }
}
