﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using TIG.Core.Authentication;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Admin.Provider;
using TIG.Core.Entities.Customer.Interfaces;
using TIG.Core.Models.Customer;
using TIG.Services.Admin.Imp;
using TIG.Services.Customer.Imp;

namespace TIG.Intergration
{
    [ApiController]
    [ApiVersion("2.0")]
    [Route("api/{v:apiVersion}/Intergration")]
    public class IntergrationController : TIGController
    {
        #region PROPERTIES
        private readonly ILogger<IntergrationController> _logger;
        private readonly IUserServices _UserServices;
        private readonly ITokenServices _TokenServices;
        private readonly ICompanyServices _CompanyServices;
        private readonly IKeyStoresServices _KeyStoresServices;
        private readonly IServiceProvider _ServiceProvider;
        private readonly IVATInvoiceServices _VATInvoiceServices;
        private readonly ILaunchInvoiceServices _LaunchInvoiceServices;
        private readonly IMemoryDataServices _MemoryDataServices;
        #endregion

        #region CONTRUCTOR
        public IntergrationController(ILogger<IntergrationController> logger, IUserServices UserServices,
            ITokenServices TokenServices, ICompanyServices CompanyServices, IKeyStoresServices KeyStoresServices,
            IServiceProvider ServiceProvider, IVATInvoiceServices VATInvoiceServices,
            ILaunchInvoiceServices LaunchInvoiceServices, MM_AContext mM_AContext, IMemoryDataServices MemoryDataServices)
        {
            _logger = logger;
            _UserServices = UserServices;
            _TokenServices = TokenServices;
            _CompanyServices = CompanyServices;
            _KeyStoresServices = KeyStoresServices;
            _ServiceProvider = ServiceProvider;
            _VATInvoiceServices = VATInvoiceServices;
            _LaunchInvoiceServices = LaunchInvoiceServices;
            _MemoryDataServices = MemoryDataServices;
        }

        #endregion

        /// <summary>
        /// Khách hàng đăng nhập lấy token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Token")]
        public IActionResult Token(LoginModel model)
        {
            try
            {
                #region VALIDATE

                #region + Kiểm tra nhập đầy đủ thông tin

                if (model == null || string.IsNullOrEmpty(model.UserName) || string.IsNullOrEmpty(model.Password)
                   || string.IsNullOrEmpty(model.TaxCode))
                {
                    return BadRequest(new
                    {
                        ResponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_1:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_1)
                        }
                    });
                }
                #endregion

                #region + Kiểm tra MST
                A_Company company = _CompanyServices.GetCompanyByTaxCode(model.TaxCode);
                if (company is null)
                {
                    return BadRequest(new
                    {
                        ResponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_2:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_2)
                        }
                    });
                }
                else if (!company.Approved)
                {
                    return BadRequest(new
                    {
                        ResponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_3:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_3)
                        }
                    });
                }
                #endregion

                #region + Kiểm tra tài khoản
                if (!_UserServices.CheckUserLogin(model, company.Id.ToString(), out UserModel data))
                {
                    return BadRequest(new
                    {
                        ResponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_4:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_4)
                        }
                    });
                }
                #endregion

                #endregion

                #region HANDLER
                TokenModel token = _TokenServices.GenUserToken(data);
                return Ok(new { ResponseValue = token });
                #endregion
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, null);
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Kiểm tra công ty đã đăng ký CA
        /// </summary>
        /// <returns></returns>
        [TIGAuthorize(Permission = "Release_invInList")]
        [HttpGet]
        [Route("CheckCARegister")]
        public IActionResult CheckCARegister()
        {
            return Ok(new
            {
                ResponseValue = new
                {
                    Result = _KeyStoresServices.CheckCARegister(UserLogon.Company.Id)
                }
            });
        }

        /// <summary>
        /// Khách hàng đăng ký chữ ký số
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("CARegister")]
        [HttpPost]
        [TIGAuthorize(Permission = "Release_invInList")]
        public IActionResult CARegister(RegisterCertificateModel model)
        {
            try
            {
                #region VALIDATE
                //Kiểm tra nhập đầy đủ thông tin
                if (string.IsNullOrEmpty(model.CertBase64) || string.IsNullOrEmpty(model.CertSerial))
                {
                    return BadRequest(new
                    {
                        ResponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_1:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_1)
                        }
                    });
                }
                //Kiểm tra chữ ký số
                byte[] raw = Convert.FromBase64String(model.CertBase64);
                X509Certificate2 x509 = new X509Certificate2(raw);
                if (x509 == null)
                {
                    return BadRequest(new
                    {
                        ReponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_5:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_5)
                        }
                    });
                }
                //Kiểm tra khớp mst
                if (!x509.SubjectName.Name.Contains(UserLogon.Company.TaxCode))
                {
                    return BadRequest(new
                    {
                        ReponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_6:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_6)
                        }
                    });
                }
                //Kiểm tra hạn sử dụng ký số
                if (DateTime.Parse(x509.GetExpirationDateString()) < DateTime.Now)
                {
                    return BadRequest(new
                    {
                        ReponseValue = new
                        {
                            Code = $"{(int)ErrorCodeMessage.Code_7:0000}",
                            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_7) + $": {x509.GetExpirationDateString()}"
                        }
                    }); ;
                }

                #endregion

                #region HANDLER

                A_KeyStores keystore = _KeyStoresServices.GetByComId(UserLogon.Company.Id);

                var checkFromDate = DateTime.TryParse(x509.GetEffectiveDateString(), out DateTime fromDate);
                var checkToDate = DateTime.TryParse(x509.GetExpirationDateString(), out DateTime toDate);
                A_Certificate certificate = new A_Certificate()
                {
                    Cert = x509.GetRawCertDataString(),
                    ComID = UserLogon.Company.Id,
                    Used = true,
                    SerialCert = x509.SerialNumber,
                    ValidFrom = checkFromDate ? fromDate : DateTime.Now,
                    ValidTo = checkToDate ? toDate : DateTime.Now,
                    OrganizationCA = x509.IssuerName.Name
                };

                if (keystore != null) _KeyStoresServices.Update(keystore, certificate);
                else _KeyStoresServices.Create(keystore, certificate);

                return Ok("Đăng ký chứng thư số thành công");
                #endregion
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, null);
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Kiểm tra hóa đơn trước khi xuất
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("VerifyInvoice")]
        [HttpPost]
        [TIGAuthorize(Permission = "Release_invInList")]
        public IActionResult KiemTraXuatHoaDon(LaunchModels model)
        {

            #region VALIDATE
            if (string.IsNullOrWhiteSpace(model.Serial) || string.IsNullOrWhiteSpace(model.Pattern)
                || model.Ids.Length < 0 || string.IsNullOrEmpty(model.Certificate))
            {
                return BadRequest(new
                {
                    ResponseValue = new
                    {
                        Code = $"{(int)ErrorCodeMessage.Code_1:0000}",
                        Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_1)
                    }
                });
            }
            byte[] raw = Convert.FromBase64String(model.Certificate);
            X509Certificate2 x509Cert = new X509Certificate2(raw);
            if (x509Cert is null)
            {
                return BadRequest(new
                {
                    ReponseValue = new
                    {
                        Code = $"{(int)ErrorCodeMessage.Code_5:0000}",
                        Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_5)
                    }
                });
            }

            A_KeyStores keystore = _KeyStoresServices.GetByComId(UserLogon.Company.Id);
            if (keystore == null)
            {
                return BadRequest(new
                {
                    ResponseValue = new
                    {
                        Code = $"{(int)ErrorCodeMessage.Code_9:0000}",
                        Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_9)
                    }
                });
            }
            //if (!x509Cert.GetSerialNumberString().ToUpper().Equals(keystore.SerialCert.ToUpper()))
            //{
            //    return BadRequest(new
            //    {
            //        ResponseValue = new
            //        {
            //            Code = $"{(int)ErrorCodeMessage.Code_10:0000}",
            //            Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_10)
            //        }
            //    });
            //}
            if (DateTime.Parse(x509Cert.GetExpirationDateString()) < DateTime.Today)
            {
                return BadRequest(new
                {
                    ResponseValue = new
                    {
                        Code = $"{(int)ErrorCodeMessage.Code_7:0000}",
                        Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_7)
                    }
                });
            }
            //Kiểm tra mẫu gửi lên
            var temp = UserLogon.RegisterTemps.Where(x => x.InvPattern == model.Pattern).FirstOrDefault();
            if (temp == null)
            {
                return BadRequest(new
                {
                    ResponseValue = new
                    {
                        Code = $"{(int)ErrorCodeMessage.Code_12:0000}",
                        Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_12)
                    }
                });
            }
            string serviceType = _MemoryDataServices.GetServiceTypeById(temp.TempId);
            if (string.IsNullOrEmpty(serviceType))
            {
                return BadRequest(new
                {
                    ResponseValue = new
                    {
                        Code = $"{(int)ErrorCodeMessage.Code_12:0000}",
                        Message = Utils.GetEnumDescription(ErrorCodeMessage.Code_12)
                    }
                });
            }
            #endregion

            #region HANDLER
            var itemeee = typeof(TIG.Services.Customer.Einvoice.InvoiceVATServices).Assembly;
            serviceType = "TIG.Services.Customer.Einvoice.InvoiceVATServices, TIG.Services";
            var tyyyy = Type.GetType(serviceType);
            var item = _ServiceProvider.GetService(tyyyy);
            IInvoiceServices _InvoiceServices = _ServiceProvider.GetService(Type.GetType(serviceType)) as IInvoiceServices;
            IInvoice[] listinvoice = _InvoiceServices.GetByID<IInvoice>(UserLogon.Company.Id, model.Ids).OrderBy(p => p.ArisingDate).ToArray();
            IList<HashData> data = new List<HashData>();
            //if (listinvoice != null && listinvoice.Count() > 0)
            //{
            //    #region Kiểm tra ngày hợp lệ của hóa đơn
            //    DateTime lastdate = DateTime.Today;
            //    if (!_LaunchInvoiceServices.AllowPublish(listinvoice, UserLogon.Company.Id, model.Pattern,
            //        model.Serial, out lastdate))
            //    {
            //        _logger.LogError("Invoice date:" + lastdate);
            //        return BadRequest(new
            //        {
            //            ResponseValue = new
            //            {
            //                Code = $"{(int)ErrorCodeMessage.Code_11:0000}",
            //                Message = $"{Utils.GetEnumDescription(ErrorCodeMessage.Code_11)}{$". Phải bắt đầu từ: {lastdate:dd/MM/yyyy}"}"
            //            }
            //        });
            //    }
            //    #endregion

            //    //IDictionary<string, string> dictHash = RemoteLauncher..GetDigests(model.Pattern, model.Serial, listinvoice, x509Cert);
            //    //foreach (KeyValuePair<string, string> pair in dictHash)
            //    //{
            //    //    data.Add(new HashData() { Key = pair.Key, Hash = pair.Value, KeepNo = false });
            //    //}
            //}

            //return Request.CreateResponse(HttpStatusCode.OK, new
            //{
            //    ResponseValue = new SignData()
            //    {
            //        CertSerial = x509Cert.GetSerialNumberString(),
            //        SignItems = data.Select(s => new SignItem() { HashData = s.Hash }).ToList(),
            //        Pattern = model.Pattern,
            //        Serial = model.Serial
            //    }
            //});


            #endregion

            return Ok();
        }
    }
}
