﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Admin;

namespace TIG.Services.Admin.Imp
{
    public interface IInvTemplateServices
    {
        /// <summary>        
        /// Tìm kiếm tất cả các mẫu hóa đơn. Có phân trang
        /// </summary>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns>Danh sách các InvTemplate phù hợp</returns>
        IList<A_InvTemplate> GetAllInvTemp(int PageIndex, int PageSize, out int totalRecords);


        /// <summary>        
        /// Tìm kiếm danh sách hóa đơn theo tên mẫu hóa đơn [temName], ID loại hóa đơn [catID]. Có phân trang
        /// </summary>
        /// <param name="temName">Tìm gần đúng theo Tên mẫu [Không bắt buộc]. Nếu rỗng thì tìm tất cả</param>
        /// <param name="catID">Tìm chính xác theo ID của loại hóa đơn [Category] [Không bắt buộc]. Nếu = 0 thì tìm tất cả </param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns>Danh sách các InvTemplate phù hợp</returns>
        IList<A_InvTemplate> SearchInvTemp(string temName, int catID, int PageIndex, int PageSize, out int totalRecords);
        /// <summary>
        /// Admin
        /// Truy vấn linq lấy mẫu hóa đơn
        /// Theo tên mẫu hóa đơn
        /// Theo id của loại hóa đơn
        /// </summary>
        /// <param name="temName"></param>
        /// <param name="catID"></param>
        /// <returns></returns>
        //IQueryable<InvTemplate> SearchLQ(string temName, int catID);

        /// <summary>
        /// Lấy về danh sách các InvTemplate theo ID của loại hóa đơn [cateId] 
        /// </summary>
        /// <param name="cateId">ID của InvCategory</param>
        /// <returns>Danh sách các InvTemplate phù hợp</returns>
        IList<A_InvTemplate> GetTempOfCate(int cateId);
        /// <summary>
        /// lấy Mẫu hóa đơn theo tên mẫu
        /// </summary>
        /// <param name="name">tên mẫu</param>
        /// <returns>đối tượng mẫu hóa đơn  InvTemplate</returns>
        A_InvTemplate GetByName(string name);

        A_InvTemplate GetByPattern(string pattern, int comid);
    }
}
