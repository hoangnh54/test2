﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TIG.Services.Admin.Imp
{
    public interface IMemoryDataServices
    {
        /// <summary>
        /// Khởi tạo dữ liệu từ db vật lý lên db memory
        /// </summary>
        void InitData();
        /// <summary>
        /// Lấy trường ServiceType của bảng InvTemplate theo id
        /// </summary>
        /// <param name="templateId">Id hóa đơn</param>
        /// <returns>Chuỗi services type</returns>
        string GetServiceTypeById(int templateId);

    }
}
