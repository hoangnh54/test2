﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Admin;
using X.PagedList;

namespace TIG.Services.Admin.Imp
{
    public interface IRegisterTempServices
    {
        /// <summary>        
        /// Lấy Tên mặc định của hóa đơn theo ký hiệu mẫu hóa đơn
        /// </summary>
        /// <param name="pattern">Mẫu hóa đơn</param>
        /// <returns>NameInvoice - Tên mặc định của hóa đơn</returns>
        string GetInvNamebyPattern(string pattern, int ComId);


        /// <summary>
        /// Lấy ra danh sách RegisterTemp theo TemplateName
        /// </summary>
        /// <param name="tempName">TemplateName</param>
        /// <returns>Danh sách các RegisterTemp phù hợp</returns>
        IList<A_RegisterTemp> GetRegOfInvTemp(string tempName);

        /// <summary>
        /// Lấy ra số thứ tự (PatternOrder) lớn nhất của RegisterTemp theo CateID
        /// </summary>
        /// <param name="cateId">ID của Category</param>
        /// <returns>PatternOrder lớn nhất</returns>
        decimal GetMaxPatternOrder(int cateId, int ComId);


        /// <summary>
        /// Lấy RegisterTemp theo ComID, PatternOrder, InvPattern
        /// </summary>
        /// <param name="ComId">ID của Company</param>
        /// <param name="pOrder">PatternOrder</param>
        /// <param name="pattern">InvPattern</param>
        /// <returns>RegisterTemp</returns>
        A_RegisterTemp GetReg(int ComId, decimal pOrder, string pattern);


        /// <summary>
        /// Lấy ra danh sách các RegisterTemp theo ComID. Có phân trang
        /// </summary>
        /// <param name="ComId">ID của Company</param>
        /// <param name="pageSize"></param>
        /// <param name="currentPageIndex"></param>
        /// <returns>Danh sách các RegisterTemp phù hợp</returns>
        PagedList<A_RegisterTemp> GetRegPageList(int ComId, int pageSize, int currentPageIndex);

        A_RegisterTemp GetbyPattern(string invPattern, int comId);
    }
}
