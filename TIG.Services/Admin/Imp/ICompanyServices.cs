﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Admin;

namespace TIG.Services.Admin.Imp
{
    public interface ICompanyServices
    {
        A_Company GetByDomain(string sitedomain);

        A_Company GetByTaxcode(string taxcode);

        bool ActiveCompany(A_Company company, string password, out string message);

        IList<A_Company> ListByDomain(string sitedomain);
        A_Company GetCompanyByTaxCode(string taxCode);


    }
}
