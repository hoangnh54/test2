﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIG.Core.Entities.Admin;

namespace TIG.Services.Admin.Imp
{
    public interface IKeyStoresServices
    {
        bool CheckCARegister(int ComId);

        /// <summary>
        /// Lấy về các KeyStores theo ComID
        /// </summary>
        /// <param name="ComId"></param>
        /// <returns></returns>
        A_KeyStores GetByComId(int ComId);

        /// <summary>
        /// Tạo mới keystore
        /// chú ý việc lưu vào bảng nkeystore không liên quan đến việc lưu và bảng Certificate
        /// </summary>
        /// <param name="Nkeystore"></param>
        /// <param name="Ncertificate"></param>
        /// <returns></returns>
        void Create(A_KeyStores Nkeystore, A_Certificate Ncertificate);
        /// <summary>
        /// Sửa cấu hình keyStore
        /// </summary>
        /// <param name="Nkeystore"></param>
        /// <param name="Ncertificate"></param>
        void Update(A_KeyStores Nkeystore, A_Certificate Ncertificate);
    }
}
