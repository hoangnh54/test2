﻿using System.Collections.Generic;
using TIG.Core.Entities.Admin;

namespace TIG.Services.Admin.Imp
{
    public interface ICertificateServices
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serial">Serial chứng thư</param>
        /// <returns></returns>
        A_Certificate GetCertFromSerial(string serial);

        /// <summary>
        /// Tìm kiếm certificate theo CN của subject of Certicate (tìm theo like)
        /// author: trường TX
        /// Update : 30/08
        /// </summary>
        /// <param name="OwnCA">chuỗi tìm kiếm</param>
        /// <param name="pageIndex">trang hiện hành</param>
        /// <param name="pageSize">số phần tử 1 trang</param>
        /// <param name="totalRecord">tổng số phần tử tìm thấy</param>
        /// <returns>danh sách các certificate</returns>
        IList<A_Certificate> LstCer(string OwnCA, int ComId, int pageIndex, int pageSize, out int totalRecord);
    }
}
