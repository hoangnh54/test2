﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Services.Admin.Imp;

namespace TIG.Services.Admin.Services
{
    public class CompanyServices : ICompanyServices
    {
        private readonly A_Context _A_Context;
        public CompanyServices(A_Context A_Context)
        {
            _A_Context = A_Context;
        }

        public bool ActiveCompany(A_Company company, string password, out string message)
        {
            message = "";
            //try
            //{
            //    string status = "";
            //    IRBACMembershipProvider _MemberShipProvider = ServiceFactory.Resolve<IRBACMembershipProvider>();
            //    user tmp = _MemberShipProvider.CreateUser(company.AccountName, password, company.Email, null, null, true, null, company.id.ToString(), out status);
            //    if (status != "Success" || tmp == null)
            //    {
            //        message = "Chưa kích hoạt được tài khoản, liên hệ để được hỗ trợ.";
            //        return false;
            //    }
            //    try
            //    {
            //        IRBACRoleProvider _RoleProvider = ServiceFactory.Resolve<IRBACRoleProvider>();
            //        IList<string> roles = new List<string>() { "Admin" };
            //        if (!_RoleProvider.RoleExists("Admin"))
            //            _RoleProvider.InstallRole("Admin", "Quản trị", true, null, company.id.ToString());
            //        if (company.Type == CompanyType.Tool)
            //        {
            //            _RoleProvider.InstallRole("QuanTriHT", "QuanTriHT", true, new string[] { }, company.id.ToString());
            //            roles.Add("QuanTriHT");
            //            _RoleProvider.InstallRole("QuanLyHD", "QuanLyHD", false, new string[] { "Search_inv", "Release_invInList", "Report" }, company.id.ToString());
            //            roles.Add("QuanLyHD");
            //            _RoleProvider.InstallRole("PhatHanh", "PhatHanh", false, new string[] { "Search_inv", "Release_invInList" }, company.id.ToString());
            //            roles.Add("PhatHanh");
            //            _RoleProvider.InstallRole("TaoLap", "TaoLap", false, new string[] { }, company.id.ToString());
            //            roles.Add("TaoLap");
            //            _RoleProvider.InstallRole("BaoCao", "BaoCao", false, new string[] { "Search_inv", "Release_invInList", "Report" }, company.id.ToString());
            //            roles.Add("BaoCao");
            //            _RoleProvider.InstallRole("Tool", "Tool", false, new string[] { "View_home" }, company.id.ToString());
            //            roles.Add("Tool");
            //            _RoleProvider.InstallRole("ServiceRole", "Gọi API", false, new string[] { "Search_inv", "Release_invInList", "Report" }, company.id.ToString());
            //            roles.Add("ServiceRole");
            //        }
            //        _RoleProvider.UpdateUsersToRoles(tmp.userid, roles.ToArray());
            //        company.IsUsed = company.Type == 0 ? true : false;
            //        Save(company);
            //        CommitChanges();
            //    }
            //    catch (Exception ex)
            //    {
            //        log.Error(ex);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    log.Error(ex);
            //    message = "Chưa kích hoạt được tài khoản, liên hệ để được hỗ trợ.";
            //    return false;
            //}
            return true;
        }

        public A_Company GetCompanyByTaxCode(string taxCode)
        {
            return _A_Context.Company.Where(x => x.TaxCode == taxCode).FirstOrDefault();
        }

        public A_Company GetByDomain(string sitedomain)
        {
            string sdomain = string.Format("({0})", sitedomain.Replace("http://", "").Replace("https://", ""));
            return _A_Context.Company.Where(p => p.Domain.Contains(sdomain) && p.Approved && p.Type != CompanyType.Tool).FirstOrDefault();
        }

        public A_Company GetByTaxcode(string taxcode)
        {
            return _A_Context.Company.Where(p => p.TaxCode == taxcode && p.Approved).FirstOrDefault();
        }

        public IList<A_Company> ListByDomain(string sitedomain)
        {
            string sdomain = string.Format("({0})", sitedomain.Replace("http://", "").Replace("https://", ""));
            var query = from s in _A_Context.Company
                        where (s.Domain.Contains(sdomain) && s.Approved && (s.Type != CompanyType.Tool
                        && s.Type != CompanyType.WebCommon))
                        select s;
            query = query.OrderBy(p => p.Name);
            return query.ToList();
        }
    }
}
