﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIG.Core.Entities.Admin;
using TIG.Services.Admin.Imp;

namespace TIG.Services.Admin.Services
{
    public class InvTemplateServices : IInvTemplateServices
    {
        private readonly A_Context _A_Context;
        public InvTemplateServices(A_Context A_Context)
        {
            _A_Context = A_Context;
        }
        //Tìm kiếm danh sách mẫu hóa đơn phân trang 
        public IList<A_InvTemplate> GetAllInvTemp(int PageIndex, int PageSize, out int totalRecords)
        {
            IQueryable<A_InvTemplate> qr = from it in _A_Context.InvTemplate select it;
            totalRecords = qr.Count();
            IList<A_InvTemplate> lst = qr.Skip(PageIndex * PageSize).Take(PageSize).ToList();
            return lst;
        }
        /// <summary>
        /// Tim kiem hoa don(truongtx sua)
        /// </summary>
        /// <param name="temName">ten mau hoa don</param>
        /// <param name="catID">id cua loai hoa don</param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns>danh sach cac mau hoa don</returns>
        public IList<A_InvTemplate> SearchInvTemp(string temName, int catID, int PageIndex, int PageSize, out int totalRecords)
        {
            IQueryable<A_InvTemplate> qr = from s in _A_Context.InvTemplate select s;
            if (!string.IsNullOrEmpty(temName))
            {
                qr = qr.Where(t => t.TemplateName.ToUpper().Contains(temName.ToUpper().Trim()));
            }
            if (catID != 0)
            {
                qr = qr.Where(c => c.InvCateID == catID);
            }
            totalRecords = qr.Count();
            if (PageSize > 0) return qr.OrderByDescending(i => i.Id).Skip(PageIndex * PageSize).Take(PageSize).ToList();
            else return qr.OrderByDescending(i => i.Id).ToList();
        }

        public IQueryable<A_InvTemplate> SearchLQ(string temName, int catID)
        {
            if (catID == 0)
            {
                if (String.IsNullOrEmpty(temName))
                    return (from it in _A_Context.InvTemplate select it).OrderByDescending(i => i.Id);
                else return (from it in _A_Context.InvTemplate where (it.TemplateName.ToUpper().Contains(temName.ToUpper().Trim())) select it).OrderByDescending(i => i.Id);
            }
            return (from it in _A_Context.InvTemplate where (it.TemplateName.ToUpper().Contains(temName.ToUpper().Trim()) && it.InvCateID == catID) select it).OrderByDescending(i => i.Id);
        }
        public IList<A_InvTemplate> GetTempOfCate(int cateId)
        {
            return (from temp in _A_Context.InvTemplate where temp.InvCateID == cateId select temp).OrderBy(p => p.TemplateName).ToList<A_InvTemplate>();
        }
        public A_InvTemplate GetByName(string name)
        {
            return _A_Context.InvTemplate.Where(item => item.TemplateName.ToUpper() == name.ToUpper()).SingleOrDefault();
        }

        public A_InvTemplate GetByPattern(string pattern, int comid)
        {
            A_RegisterTemp rtem = _A_Context.RegisterTemp.Where(item => item.ComId == comid && item.InvPattern == pattern).SingleOrDefault();
            return rtem?.InvoiceTemp;
        }
    }
}
