﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Admin.Provider;
using TIG.Core.Entities.Customer;
using TIG.Services.Admin.Imp;

namespace TIG.Services.Admin.Services
{
    public class MemoryDataServices : IMemoryDataServices
    {
        private readonly MM_AContext _MM_AContext;
        private readonly A_Context _A_Context;
        private readonly C_Context _C_Context;
        public MemoryDataServices(A_Context A_Context, MM_AContext MM_AContext, C_Context C_Context)
        {
            _MM_AContext = MM_AContext;
            _A_Context = A_Context;
            _C_Context = C_Context;
        }

        /// <summary>
        /// Khởi tạo dữ liệu từ db vật lý lên db memory
        /// </summary>
        public void InitData()
        {
            var item = _C_Context.InvoiceVAT.FirstOrDefault();
            var item2 = item.Products;

            Console.WriteLine("*** Init template data from database...");
            List<Core.Entities.Admin.Provider.A_InvTemplate> lst = _A_Context.InvTemplate
                .Select(s => new Core.Entities.Admin.Provider.A_InvTemplate()
                {
                    TemplateId = s.Id,
                    CssBackgr = s.CssBackgr,
                    CssData = s.CssData,
                    CssLogo = s.CssLogo,
                    IGenerator = s.IGenerator,
                    ImagePath = s.ImagePath,
                    InvCateID = s.InvCateID,
                    InvCateName = s.InvCateName,
                    InvoiceType = s.InvoiceType,
                    InvoiceView = s.InvoiceView,
                    IsCertify = s.IsCertify,
                    IsPub = s.IsPub,
                    IViewer = s.IViewer,
                    ParseService = s.ParseService,
                    ServiceType = s.ServiceType,
                    TemplateName = s.TemplateName,
                    XmlFile = s.XmlFile,
                    XsltFile = s.XsltFile
                }).ToList();
            Console.WriteLine("*** Number of template: " + lst.Count());
            if (lst != null && lst.Count > 0)
            {
                _MM_AContext.InvTemplate.AddRange(lst);
                _MM_AContext.SaveChanges();
            }
            Console.WriteLine("*** Init success...");
        }
        /// <summary>
        /// Lấy trường ServiceType của bảng InvTemplate theo id
        /// </summary>
        /// <param name="templateId">Id hóa đơn</param>
        /// <returns>Chuỗi services type</returns>
        public string GetServiceTypeById(int templateId)
        {
            var item = _MM_AContext.InvTemplate.Where(x => x.TemplateId == templateId).FirstOrDefault();
            if (item != null) return item.ServiceType;
            return string.Empty;
        }
    }
}

