﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using TIG.Core.Entities.Admin;
using TIG.Services.Admin.Imp;

namespace TIG.Services.Admin.Services
{
    public class KeyStoresServices : IKeyStoresServices
    {
        private readonly A_Context _A_Context;

        public KeyStoresServices(A_Context A_Context)
        {
            _A_Context = A_Context;
        }

        /// <summary>
        /// Tạo mới keystore
        /// chú ý việc lưu vào bảng nkeystore không liên quan đến việc lưu và bảng Certificate
        /// </summary>
        /// <param name="Nkeystore"></param>
        /// <param name="Ncertificate"></param>
        /// <returns></returns>
        public void Create(A_KeyStores Nkeystore, A_Certificate Ncertificate)
        {
            _A_Context.KeyStores.Add(Nkeystore);

            //Tạo mới Cert nếu có tồn tại
            List<A_Certificate> cers = _A_Context.Certificate.Where(x => x.ComID == Ncertificate.ComID).ToList();
            cers.ForEach(x => x.Used = false);
            //Tạo mới Cert
            _A_Context.Certificate.Add(Ncertificate);

            _A_Context.SaveChanges();

        }
        /// <summary>
        /// Sửa thông tin keystore
        /// </summary>
        /// <param name="Nkeystore"></param>
        /// <param name="Ncertificate"></param>
        public void Update(A_KeyStores Nkeystore, A_Certificate Ncertificate)
        {
            A_KeyStores keyStores = _A_Context.KeyStores.Where(x => x.ComID == Nkeystore.ComID).FirstOrDefault();
            if (keyStores != null)
                keyStores.SerialCert = Nkeystore.SerialCert;
            //Tạo mới Cert nếu có tồn tại
            List<A_Certificate> cers = _A_Context.Certificate.Where(x => x.ComID == Ncertificate.ComID).ToList();
            cers.ForEach(x => x.Used = false);
            //Tạo mới Cert
            _A_Context.Certificate.Add(Ncertificate);

            _A_Context.SaveChanges();


        }

        public bool CheckCARegister(int ComId)
        {
            return _A_Context.KeyStores.Any(x => x.ComID == ComId);
        }

        public A_KeyStores GetByComId(int ComId)
        {
            return _A_Context.KeyStores.Where(x => x.ComID == ComId).FirstOrDefault();
        }
    }
}
