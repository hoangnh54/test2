﻿using System;
using System.Collections.Generic;
using System.Linq;
using TIG.Core.Common;
using TIG.Core.Entities.Vendor;
using TIG.Core.Models.Vendor;
using TIG.Services.Admin.Imp;
using TIG.Services.Vendor.Imp;

namespace TIG.Services.Vendor.Services
{
    public class UserServices : IUserServices
    {
        #region PROPERTIES
        private readonly V_Context _V_Context;
        #endregion

        #region CONTRUCTOR
        public UserServices(V_Context V_Context)
        {
            _V_Context = V_Context;
        }
        #endregion


        public bool CheckUserLogin(VendorLoginModel model, out UserModel data)
        {
            data = null;
            var user = GetUserByUserName(model.UserName);
            if (user is null) return false;

            string _PassHash = GeneratorPassword.EncodePassword(model.Password, 1, user.PasswordSalt);
            if (_PassHash != user.Password) return false;


            #region Lấy danh sách role id
            //cần sửa lại chỗ này lấy theo lazyloading
            List<string> lstPermission = new List<string>();
            List<int> roleIds = (from a in user.UserRoles select a.Role.Id).ToList();
            if (roleIds != null && roleIds.Count > 0)
            {
                List<int> permisstionIds = (from s in _V_Context.Role_Permission where roleIds.Contains(s.RoleId) select s.PermissionId).ToList();
                if (permisstionIds != null && permisstionIds.Count > 0)
                {
                    lstPermission = (from s in _V_Context.Permission where permisstionIds.Contains(s.Id) select s.Name).ToList();
                }
            }
            #endregion

            data = new UserModel()
            {
                UserName = user.UserName,
                FullName = user.FullName,
                Id = user.Id,
                Permissions = lstPermission

            };
            return true;
        }
        public int InsertUser(V_Users userModel)
        {
            if(userModel == null)
            {
                return 0;
            }
            _V_Context.User.Add(userModel);
            _V_Context.SaveChanges();
            return 1;
        }
        public int UpdateUser(V_Users userModel)
        {
            if (userModel == null)
            {
                return 0;
            }
            var result = _V_Context.User.SingleOrDefault(b => b.Id == userModel.Id);
            if (result != null)
            {
                result.UserName = userModel.UserName;
                result.Email = userModel.Email;
            }
            _V_Context.User.Update(result);
            _V_Context.SaveChanges();
            return 1;
        }
        public bool DeleteUser(V_Users item)
        {
            try
            {
                V_Users tempUser = _V_Context.User.Where(x => x.Id == item.Id).FirstOrDefault();
                if (tempUser != null)
                    _V_Context.User.Remove(tempUser);
                _V_Context.User.Remove(item);
                _V_Context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        V_Users GetUserByUserName(string userName)
        {
            var a = _V_Context.User.Where(x => x.UserName == userName).FirstOrDefault();
            return _V_Context.User.Where(x => x.UserName == userName).FirstOrDefault();
        }
    }
}
