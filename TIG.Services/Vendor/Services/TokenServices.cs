﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using TIG.Core.Common;
using TIG.Core.Entities.Vendor;
using TIG.Core.Models.Vendor;
using TIG.Services.Vendor.Imp;

namespace TIG.Services.Vendor.Services
{
    public class TokenServices : ITokenServices
    {
        readonly V_Context _Context;
        private readonly IConfiguration Configuration;

        public TokenServices(V_Context vendorContext, IConfiguration configuration)
        {
            _Context = vendorContext;
            Configuration = configuration;
        }


        public TokenModel GetUserToken(UserModel user)
        {
            return this.GenUserToken(user);
        }


        public TokenModel Logon(VendorLoginModel user)
        {
            V_Users tempUser = _Context.User.Where(x => x.UserName == user.UserName).FirstOrDefault();
            if (tempUser == null ) return null;
            string _PassHash = GeneratorPassword.EncodePassword(user.Password,1, tempUser.PasswordSalt);
            if (tempUser.Password != _PassHash)
                return null;
            return this.GenUserToken(new UserModel()
            {
                UserName = user.UserName,
                FullName = tempUser.FullName,
                Id = tempUser.Id
            });
        }


        public TokenModel RefreshUserToken(string oldToken)
        {
            var principal = this.GetClaimsPrincipalByToken(oldToken);
            if (principal != null && principal.Identity.Name != null)
            {
                var user = _Context.User.Where(x => x.Id == int.Parse(principal.Identity.Name)).FirstOrDefault();
                if (user != null) return this.GenUserToken(new UserModel()
                {
                    FullName = user.FullName,
                    Id = user.Id,
                    UserName = user.UserName
                });
            }
            return null;

        }
        public ClaimsPrincipal GetClaimsPrincipalByToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("UserTokenSetting:Secret").Value)),
                ValidateLifetime = true
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            try
            {
                return tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public TokenModel GenUserToken(UserModel user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Configuration.GetSection("UserTokenSetting:Secret").Value);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim("UserData", JsonConvert.SerializeObject(user))
                }),

                Expires = DateTime.Now.AddHours(double.Parse(Configuration.GetSection("UserTokenSetting:Expires").Value)),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            TokenModel userToken = new TokenModel
            {
                Id = user.Id.ToString(),
                Accesstoken = tokenHandler.WriteToken(token),
                ExpiredAt = tokenDescriptor.Expires,
                Username = user.UserName
            };
            return userToken;
        }

        public bool VerifyAccessToken(string accesstoken)
        {
            var principal = this.GetClaimsPrincipalByToken(accesstoken);
            return principal != null;
        }
    }
}
