﻿using TIG.Core.Entities.Vendor;
using TIG.Core.Models.Vendor;

namespace TIG.Services.Vendor.Imp
{
    public interface IUserServices
    {
        /// <summary>
        /// Kiểm tra thông tin đăng nhập, trả về thông tin tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        bool CheckUserLogin(VendorLoginModel model, out UserModel data);
        int InsertUser(V_Users userModel);
        int UpdateUser(V_Users userModel);
        bool DeleteUser(V_Users userModel);
    }
}
