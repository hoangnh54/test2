﻿using System;
using System.Collections.Generic;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer.Interfaces;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Services.Customer.Imp
{
    public interface IInvoiceServices
    {
        bool isStateLess { get; set; }
        void CreateNew<T>(T entity);
        void Delete<T>(T entity);
        void Update<T>(T entity);

        IList<IInvoice> GetAllbyFkeys(int ComID, string[] fkeys);

        #region getinvoice
        /// <summary>
        /// Lấy về danh sách các IInvoice từ danh sách các id của hóa đơn
        /// </summary>
        /// <param name="InvoiceIDs">Danh sách các id của hóa đơn</param>
        /// <returns>Trả về danh sách các Invoice tương ứng</returns>
        IList<T> GetByID<T>(int ComId, int[] InvoiceIDs);

        IList<IInvoice> GetListByNo(int ComId, string pattern, string serial, decimal invNo);
        /// <summary>
        /// Lấy hóa đơn theo Mẫu, serial và số hóa đơn
        /// </summary>
        /// <param name="Pattern">Mẫu hóa đơn (Bắt buộc)</param>
        /// <param name="Serial">Serial hóa đơn (Bắt buộc)</param>
        /// <param name="InvNo">số hóa đơn (Bắt buộc)</param>
        /// <returns>Trả về hóa đơn phù hợp, trả về null nếu không có</returns>
        IInvoice GetByNo(int ComId, string Pattern, string Serial, decimal InvNo);
        /// <summary>
        /// Lấy hóa đơn theo pattern, serial và danh sách số hóa đơn
        /// </summary>
        /// <param name="ComId">Id công ty</param>
        /// <param name="Pattern">mấu hóa đơn</param>
        /// <param name="Serial">serial hóa đơn</param>
        /// <param name="InvNo">các số hóa đơn cần lấy</param>
        /// <returns></returns>
        IList<IInvoice> GetByNo(int ComId, string Pattern, string Serial, decimal[] InvNo);
        /// <summary>
        /// Lấy hóa đơn theo pattern và ID
        /// </summary>
        /// <param name="pattern">Mẫu hóa đơn (Bắt buộc)</param>
        /// <param name="id">ID hóa đơn (Bắt buộc)</param>
        /// <param name="comId">ID Of Company (Bắt buộc)</param>
        /// <returns>Trả về hóa đơn phù hợp, trả về null nếu không có</returns>
        IInvoice GetByID(int comId, string pattern, int id);
        /// <summary>
        ///Get Invoice by Fkey (All signed, adjust)
        /// </summary>
        /// <param name="Fkey"></param>
        /// <returns></returns>
        IInvoice GetByFkey(int ComID, string Fkey);

        /// <summary>
        ///Get Invoice by Fkey (NewInv)
        /// </summary>
        /// <param name="Fkey"></param>
        /// <returns></returns>
        IInvoice GetInvByFkey(int ComID, string Fkey, InvoiceStatus status = InvoiceStatus.NewInv);

        /// <summary>
        ///Get Invoice by Fkey (All signed, adjust)
        /// </summary>
        /// <param name="Fkey"></param>
        /// <returns></returns>
        IList<IInvoice> GetByFkey(int ComID, string[] Fkey);
        #endregion
        #region Search by create Date
        /// <summary>
        /// Tìm hóa đơn theo Tên KH, Mã KH, Mã số thuế, trạng thái HĐ
        /// </summary>
        /// <param name="Pattern">Mẫu hóa đơn (Bắt buộc)</param>
        /// <param name="Serial">Serial hóa đơn</param>
        /// <param name="FromDate">Nhập liệu Từ ngày </param>
        /// <param name="ToDate">Đến ngày</param>
        /// <param name="Status">Tràng thái hóa đơn</param>
        /// <param name="nameCus">Tên khách hàng(Gần đúng)</param>
        /// <param name="code">Mã khách hàng(Gần đúng)</param>
        /// <param name="CodeTax">Mã số thuế(gần đúng)</param>
        /// <returns>Trả về hóa đơn phù hợp, trả về null nếu không có</returns>
        IList<IInvoice> SearchByCustomer(int ComId, string Pattern, string Serial, string createby, DateTime? FromDate, DateTime? ToDate, InvoiceStatus Status, string nameCus, string code, string CodeTax, InvoiceType typeInvoice, int pageIndex, int pageSize, out int totalRecords);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ComId"></param>
        /// <param name="Pattern"></param>
        /// <param name="Serial"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <param name="nameCus"></param>
        /// <param name="code"></param>
        /// <param name="CodeTax"></param>
        /// <param name="typeInvoice"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        IList<IInvoice> SearchByStatus(int ComId, string Pattern, string Serial, string createby, DateTime? FromDate, DateTime? ToDate, string nameCus, string code, string CodeTax, InvoiceType typeInvoice, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status);

        /// <summary>
        /// Tìm hóa đơn theo các trạng thái hóa đơn        
        IList<IInvoice> SearchInvoice(int ComId, string Pattern, string Serial, string Branchcode, string transno, string createby, string cusname, string cuscode, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status);
        #endregion
        #region search publish invoice       
        /// <summary>
        /// Tìm hóa đơn đã phát hành theo Trạng thái HĐ
        /// </summary>
        /// <param name="Pattern">Mẫu hóa đơn (Bắt buộc)</param>
        /// <param name="Serial">Serial hóa đơn</param>
        /// <param name="FromDate">Phát hành từ ngày</param>
        /// <param name="ToDate">Phát hành đến ngày</param>
        /// <param name="Status">Các trạng thái hóa đơn</param>
        /// <returns>Trả về hóa đơn phù hợp, trả về null nếu không có</returns>
        IList<IInvoice> SearchPublish(int ComId, string Pattern, string Serial, DateTime? FromDate, DateTime? ToDate, string cusName, string cusCode, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status);
        /// <summary>
        /// Tìm hóa đơn đã phát hành theo trạng thái ký của khách hàng
        /// </summary>
        /// <param name="Pattern">Mẫu hóa đơn (Bắt buộc)</param>
        /// <param name="Serial">Serial hóa đơn</param>
        /// <param name="FromDate">Phát hành từ ngày</param>
        /// <param name="ToDate">Đến ngày</param>
        /// <param name="cusSign">Các trạng thái khách hàng ký hóa đơn</param>
        /// <returns>Trả về hóa đơn phù hợp, trả về null nếu không có</returns>        
        IList<IInvoice> SearchByCusSign(int ComID, string Pattern, string Serial, DateTime? FromDate, DateTime? ToDate,
            string statusExport, int pageIndex, int pageSize, out int totalRecords, params CusSignStatus[] cusSign);
        #endregion
        #region convert invoice
        /// <summary>
        /// Tìm hóa đơn theo trạng thái chuyển đổi HĐ
        /// </summary>
        /// <param name="Pattern">Mẫu hóa đơn (Bắt buộc)</param>
        /// <param name="Serial">Serial hóa đơn</param>
        /// <param name="FromDate">Phát hành từ ngày</param>
        /// <param name="ToDate">Đến ngày</param>
        /// <param name="ConvertStatus">Các trạng thái chuyển đổi hóa đơn</param>
        /// <returns>Trả về hóa đơn phù hợp, trả về null nếu không có</returns>
        IList<IInvoice> SearchByConvertStatus(int ComID, string Pattern, string Serial, bool Converted,
            DateTime? FromDate, DateTime? ToDate, string cusName, string cusCode, int pageIndex, int pageSize, out int totalRecords);
        /// <summary>
        /// Chuyển đổi hóa đơn chứng mình nguồn gốc xuất xứ, chỉ được chuyển đổi 1 lần
        /// </summary>
        /// <param name="inv">hóa đơn cần chuyển đổi</param>
        /// <param name="ErrorMessage">Nếu có lỗi trả về thông báo lỗi, nếu không thì có giá trị rỗng</param>
        /// <returns>trả về dữ liệu html thể hiện hóa đơn được chuyển đổi, nếu lỗi trả về rỗng</returns>     
        string ConvertForVerify(IInvoice inv, string converter, out string ErrorMessage);
        /// <summary>
        /// Chuyển đổi hóa đơn cho mục đích lưu trữ, tham khảo
        /// </summary>
        /// <param name="inv">hóa đơn cần chuyển đổi</param>
        /// <param name="ErrorMessage">Nếu có lỗi trả về thông báo lỗi, nếu không thì có giá trị rỗng</param>
        /// <returns>trả về dữ liệu html thể hiện hóa đơn được chuyển đổi, nếu lỗi trả về rỗng</returns>
        string ConvertForStore(IInvoice inv, string converter, out string ErrorMessage);
        #endregion
        #region payment invoice
        /// <summary>
        /// Tìm hóa đơn theo trạng thái thanh toán
        /// </summary>
        /// <param name="Pattern">Mẫu hóa đơn (Bắt buộc)</param>
        /// <param name="Serial">Serial hóa đơn</param>
        /// <param name="FromDate">Phát hành từ ngày</param>
        /// <param name="ToDate">Đến ngày</param>
        /// <param name="PaymentStatus">Các trạng thái thanh toán hóa đơn</param>
        /// <returns>Trả về hóa đơn phù hợp, trả về null nếu không có</returns>
        IList<IInvoice> SearchPayment(int ComId, string Pattern, string Serial, string CusName, string CusCode, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize, out int totalRecords, params Payment[] PaymentStatus);
        /// <summary>
        /// Xác nhận thanh toán cho danh sách hóa đơn. chuyển trạng thái sang "Paid"
        /// <param name="lstInvoice">Danh sách hóa đơn thanh toán</param>
        /// <returns>True nếu thành công, false nếu xảy ra lỗi</returns>
        bool ConfirmPayment(IList<IInvoice> lstInvoice, string noteappends);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="lstInvoice"></param>
        /// <returns></returns>
        bool ConfirmPayment(int[] ids, string noteappends);

        /// <summary>
        /// Bỏ Xác nhận thanh toán cho danh sách hóa đơn. chuyển trạng thái sang "UnPaid"
        /// <param name="lstInvoice">Danh sách hóa đơn bỏ thanh toán</param>
        /// <returns>True nếu thành công, false nếu xảy ra lỗi</returns>
        bool UnConfirmPayment(IList<IInvoice> lstInvoice);
        #endregion
        /// <summary>
        /// Tạo mới hóa đơn
        /// </summary>
        /// <param name="LstProductInv">Danh sách sản phẩm</param>
        /// <param name="NewInvoice">Đối tượng hóa đơn</param>
        /// <param name="ErrorMessage">Thông điệp lỗi</param>
        /// <returns>true(thành công) or false(thất bại)</returns>
        bool CreateInvoice(IList<ProductInv> LstProductInv, IInvoice NewInvoice, out string ErrorMessage);
        /// <summary>
        /// Tao hoa don theo lo
        /// </summary>
        /// <param name="invoices"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        bool CreateInvoice(IList<IInvoice> invoices, out string ErrorMessage);

        /// <summary>
        /// Tao hoa don theo lo upload
        /// </summary>
        /// <param name="company"></param>
        /// <param name="invoices"></param>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        bool CreateInvoice(A_Company company, IList<IInvoice> invoices, out string ErrorMessage);
        /// <summary>
        /// Sửa hóa đơn
        /// </summary>
        /// <param name="LstProductInv">Danh sách sản phẩm</param>
        /// <param name="OInvoice">Đối tượng Hóa đơn</param>
        /// <param name="ErrorMessage">Thông điệp lỗi</param>
        /// <returns>true(Thành công) or false(Thất bại)</returns>
        bool UpdateInvoice(IList<ProductInv> LstProductInv, IInvoice OInvoice, out string ErrorMessage);
        /// <summary>
        /// Xóa hóa đơn
        /// </summary>
        /// <param name="id">id xác định hóa đơn</param>
        /// <returns></returns>
        bool DeleteInvoice(IInvoice OInvoice, out string ErrorMessage);

        /// <summary>
        /// Lay DL Report cho Bang ke hang hoa ban ra hang thang theo mau thue
        /// </summary>
        /// <param name="comId"></param>
        /// <param name="pattern"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        IList<InvStatement> GetReportDetail(int comId, string pattern, int year, int month);

        IList<InvStatement> GetReportDetailQuarter(int comId, string pattern, int year, int quarter);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="comId"></param>
        /// <param name="pattern"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        IList<InvReportTax> GetReportDetailTax(int comId, string pattern, int year, int month);

        /// <summary>
        /// Bao cao tong hop ban hang
        /// </summary>
        /// <param name="comId"></param>
        /// <param name="pattern"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        IList<TongHopBanHang> ReportProductGeneral(int comId, string pattern, int year, int month);

        /// <summary>
        /// Báo cáo chi tiết bán hàng
        /// </summary>
        /// <param name="comId"></param>
        /// <param name="serial"></param>
        /// <param name="pattern"></param>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        IList<ChiTietBanHang> ReportProductDetails(int comId, string serial, string pattern, int year, int month);
    }
}
