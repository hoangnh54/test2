﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer;

namespace TIG.Services.Customer.Imp
{
    public interface IPublishInvoiceServices
    {
        /// <summary> Lấy danh sách các serial của pubInv đã được chấp nhận và chưa sử dụng hết theo Ký hiệu mẫu (Pattern)
        /// </summary>
        /// <param name="name">Ký hiệu mẫu</param>
        /// <returns>Danh sách các Serial phù hợp</returns>
        List<string> GetSerialByPattern(string name, int ComId);
        /// <summary>        
        /// Lấy  publishInvoice theo đơn vị, ký hiệu mẫu, ký hiệu  hóa đơn, trạng thái
        /// </summary>
        /// <param name="idCom">ID của Company [Bắt buộc]</param>
        /// <param name="pattern">Ký hiệu mẫu [Bắt buộc]</param>
        /// <param name="serial">Kí hiệu hóa đơn [Bắt buộc]</param>
        /// <param name="status">Trạng thái hóa đơn [Bắt buộc]</param>
        /// <returns>PublishInvoice</returns>
        C_PublishInvoice CurrentPubInv(int idCom, string pattern, string serial, int status);
        /// <summary>        
        /// Lấy ra các PublishInv đã được chấp nhận nhưng chưa sử dụng hết theo CompanyID, Pattern. Nếu tìm dc pubInv cùng Serial thì lấy, không thì chọn Pub có Serial khác.        
        /// </summary>
        /// <param name="comId">ID của company</param>
        /// <param name="pattern">Mẫu hóa đơn</param>
        /// <param name="Serial">Ký hiệu hóa đơn</param>
        /// <returns>PublishInvoice đầu tiên or mặc định được tìm thấy.</returns>
        C_PublishInvoice CurrentPubInv(int comId, string pattern, string Serial);

        /// <summary>        
        /// Lấy ra RegisterTemp PubInv hiện tại theo ComID, pattern, Serial. PublishInv hiện tại là PublishInv đã được chấp nhận nhưng chưa sử dụng hết theo CompanyID, Pattern. Nếu tìm dc pubInv cùng Serial thì lấy, không thì chọn Pub có Serial khác.                
        /// </summary>
        /// <param name="comId">ID của company</param>
        /// <param name="pattern">Mẫu hóa đơn</param>
        /// <param name="Serial">Ký hiệu hóa đơn</param>
        /// <returns>RegisterTemp</returns>
        A_RegisterTemp GetCurrentRegTemp(int comId, string pattern, string Serial);
        string GetInvName(int comId, string pattern, string serial);
        /// <summary>        
        /// Lấy ra danh sách List<string> các Kí hiệu mẫu của PubInv theo ComID, có trạng thái > status 
        /// (Nếu Status >1 - Kiểm tra đã có PubInv nào được chấp nhận chưa. Status >2 - Kiểm tra xem đã pubInv nào được sử dụng chưa)
        /// </summary>
        /// <param name="comId">ID của Company</param>
        /// <param name="status">Param để so sánh với Status</param>
        /// <returns>Danh sách String - Pattern</returns>
        List<string> GetPatternsByStatus(int comId, int status);

        /// <summary>
        /// Lấy ra danh sách PublishInvoice theo danh sách serials của tk đăng nhập
        /// </summary>
        /// <param name="username"></param>
        /// <param name="comId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<C_PublishInvoice> GetBySerials(int comId, string serials, params int[] status);

        IList<C_PublishInvoice> GetByStatuss(int comId, params int[] status);

        /// <summary>
        /// Lấy ra danh sách ký hiệu của PublishInvoice theo pattern và danh sách serials của tk đăng nhập
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="username"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        IList<string> GetSerials(int comID, string pattern, string serials, params int[] status);
        /// <summary>        
        /// Lấy ra danh sách các serial của PubInv đã được chấp nhận theo ComID, pattern        
        /// </summary>
        /// <param name="comId">ID của Company</param>
        /// <param name="pattern">Mẫu hóa đơn</param>
        /// <returns>Danh sách các Serial phù hợp</returns>
        List<string> LstBySerial(int comId, string pattern, int status = 0);

        List<string> GetSerialsByPattern(int comId, string pattern, params int[] status);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="regID"></param>
        /// <param name="ComId"></param>
        /// <returns></returns>
        IList<C_PublishInvoice> GetPubOfReg(int regID, int ComId);
        /// <summary>
        /// Lấy ra danh sách các PublishInvoice đang được sử dụng hoặc đã được chấp nhận nhưng chưa sử dụng theo CategoryID
        /// </summary>
        /// <param name="cateID">ID của Category</param>
        /// <returns>Danh sách các PublishInvoice phù hợp</returns>
        IList<C_PublishInvoice> GetbyInvCategory(int cateID, int ComId);
        /// <summary>
        /// Lấy ra PublishInvoice đầu tiên or mặc định theo ComID, Mẫu hóa đơn [invPattern], Serial, có CurrentNo +1 = fromNo, ToNo = toNo
        /// </summary>
        /// <param name="comID">ID của Company</param>
        /// <param name="invPattern">Mẫu hóa đơn</param>
        /// <param name="fromNo">Param để so sánh với currentNo+1</param>
        /// <param name="toNo">Param để so sánh với ToNo</param>
        /// <param name="invSerial">Ký hiệu hóa đơn</param>
        /// <returns>PublishInvoice phù hợp</returns>
        C_PublishInvoice GetPub(int comID, string invPattern, decimal fromNo, decimal toNo, string invSerial);
        /// <summary>
        /// lấy tất cả các publishInvoice của công ty [ComID] có mẫu là [invPattern] , có thể lọc theo một số trạng thái [status]
        /// </summary>
        /// <param name="comID">ID của công ty đã đăng ký</param>
        /// <param name="invPattern">tên mẫu</param>
        /// <param name="status">Option -> một số trạng thái của PublishInvoice mà bạn muốn lọc theo</param>
        /// <returns>danh sách các publish invoice phù hợp được sắp xếp theo trạng thái (PublishInvoice.Status)</returns>
        IList<C_PublishInvoice> GetbyPattern(int ComID, string invPattern, params int[] status);

        /// <summary>
        /// Tim kiếm các thông tin về chi tiết thông báo phát hành
        /// </summary>
        /// <param name="Pattern">ký hiệu mẫu</param>
        /// <param name="Serial">ký hiệu hóa đơn</param>
        /// <param name="DateFrom">từ ngày sử dụng</param>
        /// <param name="DateTo">đến ngày sử dung<param>
        /// <param name="ComId">xác định công ty phát hành hóa đơn</param>
        /// <param name="pageIndex">Index so trang</param>
        /// <param name="pageSize">Size kich co trang</param>
        /// <param name="recordItem">tổng số bản ghi</param>
        /// <returns>trả về danh sách các chi tiết thông báo phát hành</returns>
        IList<C_PublishInvoice> SeachPubInvInfo(string Pattern, string Serial, DateTime? DateFrom, DateTime? DateTo, int ComId, int pageIndex, int pageSize, out int recordItem);

        C_PublishInvoice GetFirst(int comId, int[] status);
    }
}
