﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using TIG.Core.Models.Customer;

namespace TIG.Services.Customer.Imp
{
    public interface ITokenServices
    {
        bool VerifyAccessToken(string token);
        TokenModel GetUserToken(UserModel user);
        TokenModel RefreshUserToken(string oldToken);
        ClaimsPrincipal GetClaimsPrincipalByToken(string token);
        TokenModel Logon(LoginModel user);
        /// <summary>
        /// Tạo token
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        TokenModel GenUserToken(UserModel user);
    }
}
