﻿using System;
using System.Collections.Generic;
using TIG.Core.Entities.Customer.Interfaces;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Services.Customer.Imp
{
    public interface IVATInvoiceServices
    {
        /// <summary>
        /// Lấy ds invoice theo id vs công ty
        /// </summary>
        /// <param name="ComId"></param>
        /// <param name="InvoiceIDs"></param>
        /// <returns>Trả về danh sách invoice</returns>
        IList<InvoiceVAT> GetByID(int ComId, int[] InvoiceIDs);
        /// <summary>
        /// Lấy ds invoice theo id vs công ty
        /// </summary>
        /// <param name="ComId">Công ty</param>
        /// <param name="InvoiceIDs">danh sách id hóa đơn</param>
        /// <param name="IsRelease">Đã phát hành hay chưa</param>
        /// <returns>Trả về danh sách invoice</returns>
        IList<IInvoice> GetByID(int ComId, int[] InvoiceIDs, bool IsRelease);
    }
}
