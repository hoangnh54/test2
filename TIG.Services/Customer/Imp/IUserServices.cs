﻿using TIG.Core.Models.Customer;

namespace TIG.Services.Customer.Imp
{
    public interface IUserServices
    {
        /// <summary>
        /// Kiểm tra thông tin đăng nhập, trả về thông tin tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="group"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        bool CheckUserLogin(LoginModel model, string group, out UserModel data);
    }
}
