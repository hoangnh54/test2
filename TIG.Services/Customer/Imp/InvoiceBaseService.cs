﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer.Interfaces;
using TIG.Core.Entities.Customer.Models;

namespace TIG.Services.Customer.Imp
{
    public abstract class InvoiceBaseService<I> : IInvoiceServices where I : IInvoice
    {
        #region MyRegion

        private readonly ILogger<InvoiceBaseService<I>> _logger;

        public abstract bool isStateLess { get; set; }

        public InvoiceBaseService(ILogger<InvoiceBaseService<I>> logger)
        {
            _logger = logger;
        }
        #endregion

        public abstract IList<T> GetByID<T>(int ComId, int[] InvoiceIDs);

        #region "Base service"

        public abstract void CreateNew<T>(T entity);
        public abstract void Delete<T>(T entity);
        public abstract void Update<T>(T entity);
        #endregion

        #region Search by create Date
        public abstract IList<InvStatement> GetReportDetail(int comId, string pattern, int year, int month);

        public abstract IList<InvStatement> GetReportDetailQuarter(int comId, string pattern, int year, int quarter);

        public abstract IList<InvReportTax> GetReportDetailTax(int comId, string pattern, int year, int month);

        public abstract IList<TongHopBanHang> ReportProductGeneral(int comId, string pattern, int year, int month);

        public abstract IList<ChiTietBanHang> ReportProductDetails(int comId, string serial, string pattern, int year, int month);

        public IList<IInvoice> SearchByCusSign(int ComID, string Pattern, string Serial, DateTime? FromDate, DateTime? ToDate, string statusExport, int pageIndex, int pageSize, out int totalRecords, params CusSignStatus[] cusSign)
        {
            throw new NotImplementedException();
        }

        public bool CreateInvoice(A_Company company, IList<IInvoice> invoices, out string ErrorMessage)
        {
            throw new NotImplementedException();
        }

        public abstract IList<IInvoice> SearchByCustomer(int ComId, string Pattern, string Serial, string createby, DateTime? FromDate, DateTime? ToDate, InvoiceStatus Status, string nameCus, string code, string CodeTax, InvoiceType typeInvoice, int pageIndex, int pageSize, out int totalRecords);
        public abstract IList<IInvoice> SearchByStatus(int ComId, string Pattern, string Serial, string createby, DateTime? FromDate, DateTime? ToDate, string nameCus, string code, string CodeTax, InvoiceType typeInvoice, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status);
        public abstract IList<IInvoice> SearchInvoice(int ComId, string Pattern, string Serial, string Branchcode, string transno, string createby, string cusname, string cuscode, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status);
        public abstract IList<IInvoice> SearchPublish(int ComId, string Pattern, string Serial, DateTime? FromDate, DateTime? ToDate, string cusName, string cusCode, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status);
        public abstract IList<IInvoice> SearchByConvertStatus(int ComID, string Pattern, string Serial, bool Converted, DateTime? FromDate, DateTime? ToDate, string cusName, string cusCode, int pageIndex, int pageSize, out int totalRecords);
        public abstract string ConvertForVerify(IInvoice inv, string converter, out string ErrorMessage);
        public abstract string ConvertForStore(IInvoice inv, string converter, out string ErrorMessage);
        public abstract IList<IInvoice> SearchPayment(int ComId, string Pattern, string Serial, string CusName, string CusCode, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize, out int totalRecords, params Payment[] PaymentStatus);
        public abstract bool ConfirmPayment(IList<IInvoice> lstInvoice, string noteappends);
        public abstract bool ConfirmPayment(int[] ids, string noteappends);
        public abstract bool UnConfirmPayment(IList<IInvoice> lstInvoice);
        public abstract bool CreateInvoice(IList<ProductInv> LstProductInv, IInvoice NewInvoice, out string ErrorMessage);
        public abstract bool CreateInvoice(IList<IInvoice> invoices, out string ErrorMessage);
        public abstract bool UpdateInvoice(IList<ProductInv> LstProductInv, IInvoice OInvoice, out string ErrorMessage);
        public abstract bool DeleteInvoice(IInvoice OInvoice, out string ErrorMessage);
        public abstract IList<IInvoice> GetAllbyFkeys(int ComID, string[] fkeys);
        public abstract IList<IInvoice> GetByID(int ComId, int[] InvoiceIDs);
        public abstract IList<IInvoice> GetListByNo(int ComId, string pattern, string serial, decimal invNo);
        public abstract IInvoice GetByNo(int ComId, string Pattern, string Serial, decimal InvNo);
        public abstract IList<IInvoice> GetByNo(int ComId, string Pattern, string Serial, decimal[] InvNo);
        public abstract IInvoice GetByID(int comId, string pattern, int id);
        public abstract IInvoice GetByFkey(int ComID, string Fkey);
        public abstract IInvoice GetInvByFkey(int ComID, string Fkey, InvoiceStatus status = InvoiceStatus.NewInv);
        public abstract IList<IInvoice> GetByFkey(int ComID, string[] Fkey);

        #endregion

    }
}