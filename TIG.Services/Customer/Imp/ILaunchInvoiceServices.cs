﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Customer;

namespace TIG.Services.Customer.Imp
{
    public interface ILaunchInvoiceServices
    {
        bool AllowPublish(IList<C_VATInvoice> invoices, int comID, string pattern, string serial, out DateTime lastdate);
    }
}
