﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer;
using TIG.Core.Models.Customer;
using TIG.Services.Admin.Imp;
using TIG.Services.Customer.Imp;

namespace TIG.Services.Customer.Services
{
    public class UserServices : IUserServices
    {
        #region PROPERTIES
        private readonly C_Context _C_Context;
        private readonly A_Context _A_Context;
        private readonly ICompanyServices _CompanyServices;
        #endregion

        #region CONTRUCTOR
        public UserServices(C_Context C_Context, A_Context A_Context, ICompanyServices CompanyServices)
        {
            _C_Context = C_Context;
            _A_Context = A_Context;
            _CompanyServices = CompanyServices;
        }
        #endregion


        public bool CheckUserLogin(LoginModel model, string group, out UserModel data)
        {
            data = null;
            var user = GetUserByUserNameGroupName(model.UserName, group);
            if (user is null) return false;

            string _PassHash = GeneratorPassword.EncodePassword(model.Password, user.PasswordFormat, user.PasswordSalt);
            if (_PassHash != user.Password) return false;


            #region Lấy danh sách role id
            //cần sửa lại chỗ này lấy theo lazyloading
            List<string> lstPermission = new List<string>();
            List<int> roleIds = (from a in user.UserRoles select a.Role.Id).ToList();
            if (roleIds != null && roleIds.Count > 0)
            {
                List<int> permisstionIds = (from s in _C_Context.Role_Permission where roleIds.Contains(s.RoleId) select s.PermissionId).ToList();
                if (permisstionIds != null && permisstionIds.Count > 0)
                {
                    lstPermission = (from s in _C_Context.Permission where permisstionIds.Contains(s.Id) select s.Name).ToList();
                }
            }
            #endregion

            #region Lấy thông tin công ty
            A_Company Company = null;
            bool checkCom = int.TryParse(group, out int Comid);
            if (checkCom) Company = _A_Context.Company.Where(x => x.Id == Comid).FirstOrDefault();
            #endregion

            data = new UserModel()
            {
                UserName = user.UserName,
                FullName = user.FullName,
                Id = user.Id,
                Permissions = lstPermission,
                Company = Company is null ? new Company_UserModel() : new Company_UserModel()
                {
                    Id = Company.Id,
                    Name = Company.Name,
                    TaxCode = Company.TaxCode
                },
                RegisterTemps = Company is null ? new List<Company_RegisterTemp>() :
                    Company.RegisterTemps.Where(x => x != null && x.IsUsed).Select(s => new Company_RegisterTemp()
                    {
                        Id = s.Id,
                        InvPattern = s.InvPattern,
                        TempId = s.TempID
                    }).ToList()
            };
            return true;
        }

        C_Users GetUserByUserNameGroupName(string userName, string group)
        {
            return _C_Context.User.Where(x => x.UserName == userName && x.GroupName == group).FirstOrDefault();
        }
    }
}
