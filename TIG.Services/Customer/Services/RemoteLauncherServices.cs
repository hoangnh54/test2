﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using TIG.Core.Common;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer;
using TIG.Core.Entities.Customer.Interfaces;
using TIG.Core.Models.Customer;
using TIG.Services.Admin.Imp;
using TIG.Services.Customer.Einvoice;
using TIG.Services.Customer.Imp;

namespace TIG.Services.Customer.Services
{
    public class RemoteLauncherServices
    {
        private Hashtable LockTable = new Hashtable();
        private Dictionary<string, string> invNameDic = new Dictionary<string, string>();

        private readonly ILogger<RemoteLauncherServices> _logger;
        private readonly IRepositoryINV _RepositoryINV;
        private readonly IPublishInvoiceServices _PublishInvoiceServices;
        private readonly IKeyStoresServices _KeyStoresServices;
        public RemoteLauncherServices(ILogger<RemoteLauncherServices> logger, IRepositoryINV RepositoryINV,
            IPublishInvoiceServices PublishInvoiceServices, IKeyStoresServices KeyStoresServices)
        {
            _logger = logger;
            _RepositoryINV = RepositoryINV;
            _PublishInvoiceServices = PublishInvoiceServices;
            _KeyStoresServices = KeyStoresServices;
        }


        #region "Phát hành hóa đơn"
        public IList<C_VATInvoice> Launch(string pattern, string serial, IDictionary<string,
            byte[]> dictSigned, UserModel UserLogon)
        {
            IList<C_VATInvoice> invoices = new List<C_VATInvoice>();
            if (!LockTable.Contains(pattern + "$" + UserLogon.Company.Id))
            {
                object lockobj = new object();
                LockTable.Add(pattern + "$" + UserLogon.Company.Id, lockobj);
            }
            lock (LockTable[pattern + "$" + UserLogon.Company.Id])
            {
                IPublishInvoiceService pubinvSrv = ServiceFactory.Resolve<IPublishInvoiceService>();
                IInvoiceServices service = InvServiceFactory.GetService(pattern, UserLogon.Company.Id);
                A_KeyStores keyStore = _KeyStoresServices.GetByComId(UserLogon.Company.Id);
                INoTransaction Notran = NoFactory.Instance.OpenTran(pattern, serial, UserLogon.Company.Id, pubinvSrv);
                try
                {
                    foreach (KeyValuePair<string, byte[]> pair in dictSigned)
                    {
                        if (HttpRuntime.Cache.Get(pair.Key) == null) continue;
                        IInvoice item = HttpRuntime.Cache[pair.Key] as IInvoice;
                        if (item.ArisingDate <= Enumerations.MinDate.AddDays(2) || item.ArisingDate > DateTime.Today)
                            item.ArisingDate = DateTime.Today;
                        Launch(item, Notran, service, pair.Value, currentUser, currentCompany, keyStore.SerialCert);
                        item.ModifiedDate = DateTime.Today;
                        item.Fkey = item.Fkey ?? currentCompany.TaxCode + pattern.Replace("/", "") + item.Serial.Replace("/", "") + item.No.ToString("0000000");
                        invoices.Add(item);
                        HttpRuntime.Cache.Remove(pair.Key);
                    }
                    bool rv = true;
                    string errormessage = "";
                    if (invoices.Count > 100)
                        service.isStateLess = true;
                    service.BeginTran();
                    rv = service.CreateInvoice(invoices.ToArray(), out errormessage);

                    if (!rv || !string.IsNullOrEmpty(errormessage))
                        throw new Exception(errormessage);
                    Notran.CommitTran();
                    service.CommitTran();
                    try
                    {
                        IDeliver deliverService = currentCompany.Config.ContainsKey("IDeliver") ? InvServiceFactory.GetDeliver(currentCompany.Config["IDeliver"]) : IoC.Resolve<IDeliver>();
                        deliverService.PrepareDeliver(invoices.ToArray(), currentCompany);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, ex.Message, null);
                    }
                }
                catch (Exception ex)
                {
                    Notran.RolBackTran();
                    service.RolbackTran();
                    _logger.LogError(ex, ex.Message, null);
                    throw ex;
                }
                finally
                {
                    keyStore.CloseSession();
                    NoFactory.Instance.Close(pattern, currentCompany.id);
                    service.isStateLess = false;
                }
                return invoices;
            }
        }

        private void Launch(IInvoice inv, INoTransaction Notran, IInvoiceServices _InvSrv, byte[] CustomerSign, FXUser currentUser, Company currentCompany, string certSerial)
        {
            if (inv.Status != InvoiceStatus.NewInv) return;

            lock (Notran)
            {
                inv.No = Notran.GetNextNo();
                inv.Serial = Notran.InvSerial;
            }

            if (string.IsNullOrEmpty(inv.Name))
                inv.Name = GetInvoiceName(inv.Pattern, inv.Serial, currentCompany.id);

            IGeneratorINV generator = new DsigGenerator();
            string key = SetCache(inv.Pattern, inv.Serial, inv.No, currentCompany.id);
            byte[] invData = (byte[])HttpRuntime.Cache[key];

            byte[] invSignedData = generator.WrapRemoteSign(invData, CustomerSign, certSerial);

            inv.Data = _RepositoryINV.Store(invSignedData, inv);
            inv.Status = InvoiceStatus.SignedInv;// thay doi trang thai va du lieu (data, status)
            inv.PublishDate = DateTime.Now;
            inv.PublishBy = currentUser.username;
        }

        public IDictionary<string, string> GetDigests(string pattern, string serial, C_VATInvoice[] mInvoiceList,
            X509Certificate2 cert, UserModel UserLogon)
        {
            IDictionary<string, string> ret = new Dictionary<string, string>();
            if (!LockTable.Contains(pattern + "$" + UserLogon.Company.Id))
            {
                object lockobj = new object();
                LockTable.Add(pattern + "$" + UserLogon.Company.Id, lockobj);
            }
            lock (LockTable[pattern + "$" + UserLogon.Company.Id])
            {
                try
                {
                    IPublishInvoiceService PSrv = ServiceFactory.Resolve<IPublishInvoiceService>();
                    INoTransaction Notran = NoFactory.Instance.OpenTran(pattern, serial, UserLogon.Company.Id, PSrv);
                    IGeneratorINV generator = new DsigGenerator();
                    foreach (IInvoice item in mInvoiceList)
                    {
                        if (item.Status != InvoiceStatus.NewInv)
                            continue;
                        lock (Notran)
                        {
                            item.No = Notran.GetNextNo();
                            item.Serial = Notran.InvSerial;
                        }

                        _logger.LogError("RemoteLauncher -> GetDigests: " + JsonConvert.SerializeObject(item));
                        _logger.LogError("RemoteLauncher -> GetDigests: " + Enumerations.MinDate.AddDays(2));
                        _logger.LogError("RemoteLauncher -> GetDigests: " + JsonConvert.SerializeObject(item));


                        if (item.ArisingDate <= Enumerations.MinDate.AddDays(2) || item.ArisingDate > DateTime.Today)
                            item.ArisingDate = DateTime.Today;
                        string XMLData = item.GetXMLData(currentCompany);
                        _logger.LogError("RemoteLauncher -> GetDigests: " + XMLData);
                        byte[] invDataWithCus;
                        byte[] invhash = generator.GetDigestForRemote(Encoding.UTF8.GetBytes(XMLData), out invDataWithCus, cert, item);
                        string hashbase64 = Convert.ToBase64String(invhash);
                        string hashToken = string.Format("{0}|{1}", hashbase64, UserLogon.Company.Id);
                        string invoicetoken = SetCache(item.Pattern, item.Serial, item.No, UserLogon.Company.Id);
                        HttpRuntime.Cache.Insert(invoicetoken, invDataWithCus, null, DateTime.Now.AddMinutes(2), Cache.NoSlidingExpiration);
                        HttpRuntime.Cache.Insert(hashToken, item, null, DateTime.Now.AddMinutes(2), Cache.NoSlidingExpiration);

                        //HttpRuntime.Cache.Insert(invoicetoken, invDataWithCus, null, item.ArisingDate, Cache.NoSlidingExpiration);
                        //HttpRuntime.Cache.Insert(hashToken, item, null, item.ArisingDate, Cache.NoSlidingExpiration);
                        ret.Add(hashToken, hashbase64);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, ex.Message, null);
                    throw ex;
                }
                finally { NoFactory.Instance.Close(pattern, currentCompany.id); }
            }
            return ret;
        }
        #endregion

        #region "Điều chỉnh hóa đơn"
        public IInvoice AdjustInv(IInvoice OriINV, string pattern, string serial, IDictionary<string, byte[]> invoiceSigns, FXContext Context = null, string attachfile = "")
        {
            Company currentCompany = (Company)FXContext.Current.CurrentCompany;
            //chuan bi du lieu
            AdjustInv ad = new AdjustInv();
            ad.InvId = Convert.ToInt32(OriINV.id);
            ad.ComID = OriINV.ComID;
            ad.Pattern = OriINV.Pattern;
            ad.Status = ProcessingStatus.AdjustedInv;
            ad.Description = "Hóa đơn điều chỉnh";

            ad.Attachefile = attachfile;
            DateTime dt = OriINV.ArisingDate;

            //phat hanh hoa don dieu chinh                        
            IInvoice inv = Launch(pattern, serial, invoiceSigns).FirstOrDefault();

            if (inv != null)
            {
                OriINV.Status = InvoiceStatus.AdjustedInv;
                OriINV.Note = String.Format("Hóa đơn bị điều chỉnh bởi số: {0}. Thời gian điều chỉnh:{1}", inv.No.ToString("0000000"), DateTime.Now);
                IInvoiceServices invoiSrc = InvServiceFactory.GetService(pattern, OriINV.ComID);
                try
                {
                    invoiSrc.BeginTran();
                    OriINV.ModifiedDate = DateTime.Now;
                    invoiSrc.Update(OriINV);
                    InvoiceBase invUpdate = invoiSrc.Getbykey<InvoiceBase>(inv.id);
                    invUpdate.CertifiedOriginalID = OriINV.CertifiedID;
                    invUpdate.Adjusts.Add(ad);
                    invoiSrc.Save(invUpdate);
                    invoiSrc.CommitTran();
                    return inv;
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    invoiSrc.RolbackTran();
                    return null;
                }
            }
            return null;
        }
        #endregion

        #region "Thay thế hóa đơn"
        public IInvoice ReplaceInv(IInvoice OriINV, string pattern, string serial, IDictionary<string, byte[]> invoiceSigns, FXContext Context = null, string attachfile = "")
        {
            Company currentCompany = (Company)FXContext.Current.CurrentCompany;

            DateTime dt = OriINV.ArisingDate;
            //chuan bi du lieu
            AdjustInv ad = new AdjustInv();
            ad.InvId = Convert.ToInt32(OriINV.id);
            ad.ComID = OriINV.ComID;
            ad.Pattern = OriINV.Pattern;
            ad.Status = ProcessingStatus.ReplacedInv;
            ad.Description = "Hóa đơn thay thế";
            ad.Attachefile = attachfile;

            //phat hanh hd thay the            
            IInvoice inv = Launch(pattern, serial, invoiceSigns).FirstOrDefault();

            if (inv != null)
            {
                OriINV.Status = InvoiceStatus.ReplacedInv;
                OriINV.Note = String.Format("Hóa đơn bị thay thế bởi số: {0}. Thời gian thay thế:{1}", inv.No.ToString("0000000"), DateTime.Now);
                IInvoiceServices invoiSrc = InvServiceFactory.GetService(pattern, OriINV.ComID);
                try
                {
                    invoiSrc.BeginTran();
                    OriINV.ModifiedDate = DateTime.Now;
                    invoiSrc.Update(OriINV);
                    InvoiceBase invUpdate = invoiSrc.Getbykey<InvoiceBase>(inv.id);
                    invUpdate.CertifiedOriginalID = OriINV.CertifiedID;
                    invUpdate.Type = InvoiceType.ForReplace;
                    invUpdate.Adjusts.Add(ad);
                    invoiSrc.Save(invUpdate);
                    invoiSrc.CommitTran();
                    return invUpdate;
                }
                catch (Exception ex)
                {
                    log.Error(ex);
                    invoiSrc.RolbackTran();
                    return null;
                }
            }
            return null;
        }
        #endregion

        private string GetInvoiceName(string pattern, string serial, int ComID)
        {
            string key = pattern + "$" + ComID + "$" + serial;
            if (!invNameDic.ContainsKey(key))
            {
                IPublishInvoiceService service = ServiceFactory.Resolve<IPublishInvoiceService>();
                string invname = service.GetInvName(ComID, pattern, serial);
                invNameDic.Add(key, invname);
            }
            return invNameDic[key];
        }

        private string SetCache(string pattern, string serial, decimal no, int comid)
        {
            return string.Format("{0};{1};{2}_{3}", pattern, serial, no, comid);
        }
    }
}
