﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIG.Core.Common;
using TIG.Core.Entities.Customer;
using TIG.Services.Customer.Imp;

namespace TIG.Services.Customer.Services
{
    public class LaunchInvoiceServices : ILaunchInvoiceServices
    {
        private Hashtable LockTable = new Hashtable();
        private readonly ILogger<LaunchInvoiceServices> _logger;
        private readonly IPublishInvoiceServices _PublishInvoiceServices;
        private readonly C_Context _C_Context;

        public LaunchInvoiceServices(ILogger<LaunchInvoiceServices> logger, IPublishInvoiceServices PublishInvoiceServices,
            C_Context C_Context)
        {
            _logger = logger;
            _PublishInvoiceServices = PublishInvoiceServices;
            _C_Context = C_Context;
        }

        public bool AllowPublish(IList<C_VATInvoice> invoices, int comID, string pattern,
            string serial, out DateTime lastdate)
        {
            _logger.LogError("LOG AllowPublish: ");
            lastdate = DateTime.Today;
            try
            {
                if (!LockTable.Contains(String.Format("{0}", comID))) //fix
                {
                    object lockobj = new object();
                    LockTable.Add(String.Format("{0}", comID), lockobj);
                }
                lock (LockTable[String.Format("{0}", comID)])
                {
                    if (string.IsNullOrWhiteSpace(serial))
                        serial = invoices[0].Serial;
                    //Lấy ra TBPH theo pattern, serial của đơn vị
                    var publishinvoice = _PublishInvoiceServices.GetbyPattern(comID, pattern, 1, 2).FirstOrDefault(p => p.InvSerial == serial);
                    //Chưa có TBPH nao -> Không hợp lệ
                    if (publishinvoice == null) return false;

                    //Nếu ngày trên lô hóa đơn < ngày bắt đầu sử dụng -> Lỗi
                    if (invoices.Any(p => (p.ArisingDate > Enumerations.MinDate.AddDays(2)) && p.ArisingDate < publishinvoice.StartDate))
                    {
                        lastdate = publishinvoice.StartDate;
                        return false;
                    }

                    //Lấy hóa đơn cuối cùng trên dải hóa đơn
                    var invoice = _C_Context.InvoiceVAT.FirstOrDefault(inv => inv.ComID == comID
                        && inv.Pattern == pattern && inv.Serial == serial
                        && inv.Status != InvoiceStatus.NewInv && inv.No == publishinvoice.CurrentNo);
                    //Chưa có hd nào -> OK;
                    if (invoice == null) return true;
                    //Nếu ngày trên lô hóa đơn < ngày hd gần nhất -> Lỗi
                    if (invoices.Any(p => (p.ArisingDate > Enumerations.MinDate.AddDays(2)) && p.ArisingDate <= invoice.ArisingDate.AddDays(-1))) return false;
                    return true;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, null);
                return true;
            }
        }
    }
}
