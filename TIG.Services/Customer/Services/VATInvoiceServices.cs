﻿using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Customer;
using TIG.Services.Customer.Imp;
using System.Linq;
using TIG.Core.Entities.Customer.Models;
using TIG.Core.Entities.Customer.Interfaces;

namespace TIG.Services.Customer.Services
{
    public class VATInvoiceServices : IVATInvoiceServices
    {
        private readonly C_Context _C_Context;

        public VATInvoiceServices(C_Context C_Context)
        {
            _C_Context = C_Context;
        }

        /// <summary>
        /// Lấy ds invoice theo id vs công ty
        /// </summary>
        /// <param name="ComId"></param>
        /// <param name="InvoiceIDs"></param>
        /// <returns>Trả về danh sách invoice</returns>
        public IList<InvoiceVAT> GetByID(int ComId, int[] InvoiceIDs)
        {
            return (from inv in _C_Context.InvoiceVAT
                    where InvoiceIDs.Contains(inv.Id) && inv.ComID == ComId
                    select inv).ToList();
        }
        /// <summary>
        /// Lấy ds invoice theo id vs công ty
        /// </summary>
        /// <param name="ComId">Công ty</param>
        /// <param name="InvoiceIDs">danh sách id hóa đơn</param>
        /// <param name="IsRelease">Đã phát hành hay chưa</param>
        /// <returns>Trả về danh sách invoice</returns>
        public IList<IInvoice> GetByID(int ComId, int[] InvoiceIDs, bool IsRelease)
        {
            //IList<IInvoice> lstInv = (from inv in Query where InvoiceIDs.Contains(inv.id) && inv.ComID == ComId select (IInvoice)inv).ToList();
            //return lstInv;


            var query = from inv in _C_Context.InvoiceVAT
                        where InvoiceIDs.Contains(inv.Id) && inv.ComID == ComId
                        select (IInvoice)inv;
            if (IsRelease) return query.Where(x => x.No > 0).ToList();
            else return query.Where(x => x.No == 0).ToList();
        }
    }
}
