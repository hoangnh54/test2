﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TIG.Core.Entities.Admin;
using TIG.Core.Entities.Customer;
using TIG.Services.Admin.Imp;
using TIG.Services.Customer.Imp;

namespace TIG.Services.Customer.Services
{
    public class PublishInvoiceServices : IPublishInvoiceServices
    {
        private readonly ILogger<PublishInvoiceServices> _logger;
        private readonly C_Context _C_Context;
        private readonly A_Context _A_Context;
        public PublishInvoiceServices(ILogger<PublishInvoiceServices> logger, C_Context C_Context, A_Context A_Context)
        {
            _logger = logger;
            _C_Context = C_Context;
            _A_Context = A_Context;
        }


        public List<string> GetSerialByPattern(string name, int ComId)
        {
            var query = _C_Context.PublishInvoice.Where(c => (c.InvPattern == name) && (c.ComId == ComId) && (c.Status == 2 || c.Status == 1) && (c.CurrentNo != c.ToNo));
            List<string> result = query.Select(p => p.InvSerial).Distinct().ToList();
            return result;
        }

        public C_PublishInvoice CurrentPubInv(int idCom, string pattern, string serial, int status)
        {
            IQueryable<C_PublishInvoice> qr;
            qr = _C_Context.PublishInvoice.Where(p => p.ComId == idCom && p.InvPattern == pattern && p.InvSerial == serial && p.Status == status);
            return qr.OrderBy(a => a.StartDate).FirstOrDefault();
        }
        public C_PublishInvoice CurrentPubInv(int comId, string pattern, string Serial)
        {
            return _C_Context.PublishInvoice.Where(pi => pi.ComId == comId && (pi.InvPattern == pattern) && (pi.InvSerial == Serial) && (pi.Status == 1 || pi.Status == 2)).OrderBy(pi => pi.StartDate).OrderBy(pi => pi.Id).FirstOrDefault();
        }

        public A_RegisterTemp GetCurrentRegTemp(int comId, string pattern, string Serial)
        {
            try
            {
                C_PublishInvoice pub = _C_Context.PublishInvoice.Where(pi => pi.ComId == comId && (pi.InvPattern == pattern) && (pi.InvSerial == Serial) && (pi.Status == 1 || pi.Status == 2)).OrderBy(pi => pi.StartDate).OrderBy(pi => pi.Id).FirstOrDefault();
                A_RegisterTemp regis = _A_Context.RegisterTemp.Where(r => r.Id == pub.RegisterID).SingleOrDefault();
                return regis;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetInvName(int comId, string pattern, string serial)
        {
            try
            {
                var qr = _C_Context.PublishInvoice.Where(pi => pi.ComId == comId && pi.InvPattern == pattern);
                if (!string.IsNullOrWhiteSpace(serial))
                    qr = qr.Where(pi => pi.InvSerial == serial);
                qr = qr.OrderBy(pi => pi.Id);
                C_PublishInvoice pub = qr.FirstOrDefault();
                A_RegisterTemp regis = _A_Context.RegisterTemp.Where(r => r.Id == pub.RegisterID).FirstOrDefault();
                return regis.NameInvoice;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, null);
                throw;
            }
        }

        public List<string> GetPatternsByStatus(int comId, int status)
        {
            try
            {
                var qr = _C_Context.PublishInvoice.Where(p => p.ComId == comId && p.Status >= status).OrderBy(p => p.Status).ToList();
                List<string> results = qr.Select(p => p.InvPattern).Distinct().ToList();
                return results;
            }
            catch (Exception ex)
            {
                throw new Exception("Chưa có mẫu phát hành được chấp nhận");
            }
        }

        public List<string> LstBySerial(int comId, string pattern, int status)
        {
            return (from p in _C_Context.PublishInvoice where p.ComId == comId && p.InvPattern == pattern && p.Status > status select p.InvSerial).Distinct().ToList();
        }

        public List<string> GetSerialsByPattern(int comId, string pattern, params int[] status)
        {
            var qr = _C_Context.PublishInvoice.Where(p => p.ComId == comId && p.InvPattern == pattern);
            if (status != null && status.Length > 0)
                qr = qr.Where(p => status.Contains(p.Status));
            return qr.Select(p => p.InvSerial).ToList();
        }

        public IList<C_PublishInvoice> GetPubOfReg(int regID, int ComId)
        {
            return (from p in _C_Context.PublishInvoice where p.RegisterID == regID && p.ComId == ComId select p).ToList<C_PublishInvoice>();
        }

        public IList<C_PublishInvoice> GetbyInvCategory(int cateID, int ComId)
        {
            int[] ids = _A_Context.RegisterTemp.Where(p => p.InvCateID == cateID).Select(p => p.Id).ToArray();
            var qr = _C_Context.PublishInvoice.Where(pi => pi.ComId == ComId && (pi.Status == 1 || pi.Status == 2) && pi.CurrentNo < pi.ToNo && ids.Contains(pi.RegisterID));
            return qr.ToList();
        }

        public C_PublishInvoice GetPub(int comID, string invPattern, decimal fromNo, decimal toNo, string invSerial)
        {
            return (from p in _C_Context.PublishInvoice where p.ComId == comID && p.InvPattern == invPattern && (p.FromNo == fromNo || p.CurrentNo + 1 == fromNo) && p.ToNo == toNo && p.InvSerial == invSerial && p.Status != 3 select p).FirstOrDefault<C_PublishInvoice>();
        }

        public IList<C_PublishInvoice> GetbyPattern(int ComID, string invPattern, params int[] status)
        {
            var query = _C_Context.PublishInvoice.Where(p => p.ComId == ComID && p.InvPattern == invPattern && p.StartDate <= DateTime.Now);
            if (status != null && status.Length > 0)
                return query.Where(p => status.Contains(p.Status)).OrderBy(p => p.Id).ThenBy(p => p.StartDate).ThenByDescending(p => p.Status).ToList();
            return query.OrderBy(p => p.Id).ThenBy(p => p.StartDate).ThenByDescending(p => p.Status).ToList();
        }

        public IList<C_PublishInvoice> SeachPubInvInfo(string Pattern, string Serial, DateTime? DateFrom, DateTime? DateTo, int ComId, int pageIndex, int pageSize, out int recordItem)
        {
            IQueryable<C_PublishInvoice> qr = from p in _C_Context.PublishInvoice where p.ComId == ComId && p.Status >= 0 select p;
            if (!string.IsNullOrEmpty(Pattern)) qr = from p in qr where p.InvPattern == Pattern select p;
            if (!string.IsNullOrEmpty(Serial)) qr = from p in qr where p.InvSerial == Serial select p;
            if (DateFrom.HasValue) qr = qr.Where(inv => inv.StartDate >= DateFrom.Value);
            if (DateTo.HasValue) qr = qr.Where(inv => inv.StartDate < DateTo.Value.AddDays(1));
            qr = qr.OrderByDescending(c => c.Id);
            recordItem = qr.Count();
            IList<C_PublishInvoice> lst = qr.Skip(pageIndex * pageSize).Take(pageSize).ToList();
            return lst;
        }

        public C_PublishInvoice GetFirst(int comId, int[] status)
        {
            C_PublishInvoice publishInvoice = _C_Context.PublishInvoice.Where(pi => pi.ComId == comId && status.Contains(pi.Status)).FirstOrDefault();
            return publishInvoice;
        }

        public IList<C_PublishInvoice> GetBySerials(int comId, string serials, params int[] status)
        {
            if (string.IsNullOrWhiteSpace(serials))
                return new List<C_PublishInvoice>();
            var qr = _C_Context.PublishInvoice.Where(p => p.ComId == comId && status.Contains(p.Status) && serials.Contains(p.InvSerial));
            return qr.OrderBy(p => p.InvPattern).ToList();
        }

        public IList<C_PublishInvoice> GetByStatuss(int comId, params int[] status)
        {
            var qr = _C_Context.PublishInvoice.Where(p => p.ComId == comId && status.Contains(p.Status));
            return qr.OrderBy(p => p.InvPattern).ToList();
        }

        public IList<string> GetSerials(int comID, string pattern, string serials, params int[] status)
        {
            if (string.IsNullOrWhiteSpace(serials))
                return new List<string>();
            var qr = _C_Context.PublishInvoice.Where(p => p.ComId == comID && p.InvPattern == pattern && serials.Contains(p.InvSerial));
            if (status != null && status.Length > 0)
                qr = qr.Where(p => status.Contains(p.Status));
            return qr.Select(p => p.InvSerial).Distinct().ToList();
        }
    }
}
