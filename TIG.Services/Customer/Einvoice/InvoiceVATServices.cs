﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using TIG.Core.Entities.Customer;
using TIG.Core.Entities.Customer.Models;
using TIG.Services.Customer.Imp;
using System.Linq;
using TIG.Core.Common;
using TIG.Core.Entities.Customer.Interfaces;

namespace TIG.Services.Customer.Einvoice
{
    public class InvoiceVATServices : InvoiceBaseService<InvoiceVAT>
    {
        private readonly ILogger<InvoiceVATServices> _logger;
        private readonly C_Context _C_Context;

        public override bool isStateLess { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public InvoiceVATServices(ILogger<InvoiceVATServices> logger, C_Context C_Context) : base(logger)
        {
            _logger = logger;
            _C_Context = C_Context;
        }
        public override void CreateNew<T>(T entity)
        {
            var invoice = entity as InvoiceVAT;
            _C_Context.InvoiceVAT.Add(invoice);
            _C_Context.SaveChanges();
        }

        public override void Delete<T>(T entity)
        {
            var invoice = entity as InvoiceVAT;
            InvoiceVAT invoiceVAT = _C_Context.InvoiceVAT.Where(x => x.Id == invoice.Id).FirstOrDefault();
            if (invoiceVAT != null)
            {
                if (invoiceVAT.Status != InvoiceStatus.NewInv)
                {
                    throw new Exception("Can't delete published Invoice.");
                }
                else
                {
                    _C_Context.InvoiceVAT.Remove(invoiceVAT);
                    _C_Context.SaveChanges();
                }
            }
        }

        public override void Update<T>(T entity)
        {
            var invoice = entity as InvoiceVAT;
            InvoiceVAT invoiceVAT = _C_Context.InvoiceVAT.Where(x => x.Id == invoice.Id).FirstOrDefault();
            if (invoiceVAT != null)
            {
                if (invoiceVAT.Status != InvoiceStatus.NewInv)
                {
                    throw new Exception("Can't update published Invoice.");
                }
                else
                {

                }
            }
        }

        public override IList<InvStatement> GetReportDetail(int comId, string pattern, int year, int month)
        {
            List<InvStatement> listInvStatement = new List<InvStatement>();
            //danh sách các báo cáo không thuế
            var report0vat = from item in _C_Context.InvoiceVAT
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.VATRate == -1
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     Note = item.Note,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     Grossvalue = item.GrossValue,
                                     VatRate = -1
                                 }
                                 );
            listInvStatement.AddRange(report0vat.ToList());
            //danh sách các báo cáo thuế 0%
            var reportvat0 = from item in _C_Context.InvoiceVAT
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.VATRate == 0
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     Note = item.Note,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount0,
                                     Grossvalue = item.GrossValue0,
                                     VatRate = 0
                                 }
                                 );
            listInvStatement.AddRange(reportvat0.ToList());
            //danh sách các báo cáo thuế 5%
            var reportvat5 = from item in _C_Context.InvoiceVAT
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.VATRate == 5
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     Note = item.Note,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount5,
                                     Grossvalue = item.GrossValue5,
                                     VatRate = 5
                                 }
                                 );
            listInvStatement.AddRange(reportvat5.ToList());
            //danh sách các báo cáo thuế 10%
            var reportvat10 = from item in _C_Context.InvoiceVAT
                              where item.ComID == comId && item.Pattern == pattern &&
                                  item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                  item.Status != InvoiceStatus.NewInv &&
                                  item.VATRate == 10
                              select (
                                  new InvStatement
                                  {
                                      kyHieuMauHDon = item.Pattern,
                                      kyHieuHDon = item.Serial,
                                      Note = item.Note,
                                      status = item.Status,
                                      type = item.Type,
                                      soHoaDon = item.No,
                                      ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                      tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                      masoThueNguoiMua = item.CusTaxCode,
                                      VATAmount = item.VatAmount10,
                                      Grossvalue = item.GrossValue10,
                                      VatRate = 10
                                  }
                                  );
            listInvStatement.AddRange(reportvat10.ToList());
            return listInvStatement;
        }

        public override IList<InvStatement> GetReportDetailQuarter(int comId, string pattern, int year, int quarter)
        {
            DateTime FirstDate = new DateTime();
            DateTime LastDate = new DateTime();
            if (quarter == 1)
            {
                FirstDate = DateTime.ParseExact("01/01/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("31/03/" + year, "dd/MM/yyyy", null);
            }
            else if (quarter == 2)
            {
                FirstDate = DateTime.ParseExact("01/04/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("30/06/" + year, "dd/MM/yyyy", null);
            }
            else if (quarter == 3)
            {
                FirstDate = DateTime.ParseExact("01/07/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("30/09/" + year, "dd/MM/yyyy", null);
            }
            else if (quarter == 4)
            {
                FirstDate = DateTime.ParseExact("01/10/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("31/12/" + year, "dd/MM/yyyy", null);
            }

            //khai bao danh sách báo cáo 
            List<InvStatement> listInvStatement = new List<InvStatement>();
            //danh sách các báo cáo không thuế
            var report0vat = from item in _C_Context.InvoiceVAT
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.VATRate == -1
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     Note = item.Note,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     //VATAmount = item.VATAmount,
                                     Grossvalue = item.GrossValue,
                                     VatRate = -1
                                 }
                                 );
            listInvStatement.AddRange(report0vat.ToList());
            //danh sách các báo cáo thuế 0%
            var reportvat0 = from item in _C_Context.InvoiceVAT
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.VATRate == 0
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     Note = item.Note,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount0,
                                     Grossvalue = item.GrossValue0,
                                     VatRate = 0
                                 }
                                 );
            listInvStatement.AddRange(reportvat0.ToList());
            //danh sách các báo cáo thuế 5%
            var reportvat5 = from item in _C_Context.InvoiceVAT
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.VATRate == 5
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     Note = item.Note,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount5,
                                     Grossvalue = item.GrossValue5,
                                     VatRate = 5
                                 }
                                 );
            listInvStatement.AddRange(reportvat5.ToList());
            //danh sách các báo cáo thuế 10%
            var reportvat10 = from item in _C_Context.InvoiceVAT
                              where item.ComID == comId && item.Pattern == pattern &&
                                  item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                  item.Status != InvoiceStatus.NewInv &&
                                  item.VATRate == 10
                              select (
                                  new InvStatement
                                  {
                                      kyHieuMauHDon = item.Pattern,
                                      kyHieuHDon = item.Serial,
                                      Note = item.Note,
                                      status = item.Status,
                                      type = item.Type,
                                      soHoaDon = item.No,
                                      ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                      tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                      masoThueNguoiMua = item.CusTaxCode,
                                      VATAmount = item.VatAmount10,
                                      Grossvalue = item.GrossValue10,
                                      VatRate = 10
                                  }
                                  );
            listInvStatement.AddRange(reportvat10.ToList());
            return listInvStatement;
        }

        public override IList<InvReportTax> GetReportDetailTax(int comId, string pattern, int year, int month)
        {
            IList<InvReportTax> listinv = new List<InvReportTax>();
            try
            {
                listinv = (from item in _C_Context.InvoiceVAT
                           join itempro in _C_Context.ProductInv on item.Id equals itempro.InvID
                           where item.ComID == comId && item.Pattern == pattern &&
                               item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                               item.Status != InvoiceStatus.NewInv
                           select (
                               new InvReportTax
                               {
                                   id = item.Id,
                                   Status = item.Status,
                                   Type = item.Type,
                                   IsSum = itempro.IsSum,
                                   Pattern = item.Pattern,
                                   Serial = item.Serial,
                                   No = item.No,
                                   InvoiceDate = item.ArisingDate,
                                   CusName = item.CusName,
                                   CusTaxCode = item.CusTaxCode,
                                   ProdName = itempro.Name,
                                   ProdTotal = itempro.Total,
                                   ProdVatAmount = itempro.VATAmount,
                                   VATRate = itempro.VATRate,
                               }
                               )).OrderBy(p => p.No).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, null);
            }
            return listinv;
        }

        public override IList<TongHopBanHang> ReportProductGeneral(int comId, string pattern, int year, int month)
        {
            IList<TongHopBanHang> listinv = new List<TongHopBanHang>();
            try
            {
                listinv = (from item in _C_Context.InvoiceVAT
                           join itempro in _C_Context.ProductInv on item.Id equals itempro.InvID
                           where item.ComID == comId && item.Pattern == pattern &&
                               item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                               item.Status != InvoiceStatus.NewInv
                           select (
                               new TongHopBanHang
                               {
                                   id = item.Id,
                                   Type = item.Type,
                                   Status = item.Status,
                                   IsSum = itempro.IsSum,
                                   MaHang = itempro.Code,
                                   TenHang = itempro.Name,
                                   DonVi = itempro.Unit,
                                   SoLuong = itempro.Quantity,
                                   DonGia = itempro.Price,
                                   ProdTotal = itempro.Total,
                                   ProdVatAmount = itempro.VATAmount,
                                   ProdAmount = itempro.Amount,
                                   VATRate = itempro.VATRate,
                               }
                               )).OrderBy(item => item.MaHang).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, null);
            }
            return listinv;
        }

        public override IList<ChiTietBanHang> ReportProductDetails(int comId, string serial, string pattern, int year, int month)
        {

            IList<ChiTietBanHang> listinv = new List<ChiTietBanHang>();
            try
            {
                listinv = (from item in _C_Context.InvoiceVAT
                           join itempro in _C_Context.ProductInv on item.Id equals itempro.InvID
                           where item.ComID == comId && item.Pattern == pattern && item.Serial == serial &&
                               item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                               item.Status != InvoiceStatus.NewInv
                           select (
                               new ChiTietBanHang
                               {
                                   id = item.Id,
                                   Status = item.Status,
                                   Type = item.Type,
                                   IsSum = itempro.IsSum,
                                   Pattern = item.Pattern,
                                   Serial = item.Serial,
                                   No = item.No,
                                   InvoiceDate = item.ArisingDate,
                                   Buyer = item.Buyer,
                                   CusName = item.CusName,
                                   CusTaxCode = item.CusTaxCode,
                                   MaHang = itempro.Code,
                                   TenHang = itempro.Name,
                                   DonGia = itempro.Price,
                                   SoLuong = itempro.Quantity,
                                   DonVi = itempro.Unit,
                                   ProdTotal = itempro.Total,
                                   ProdVatAmount = itempro.VATAmount,
                                   VATRate = item.VATRate,
                                   ProdAmount = itempro.Amount
                               }
                               )).OrderBy(item => item.No).ToList();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, null);
            }
            return listinv;
        }

        public override IList<IInvoice> SearchByCustomer(int ComId, string Pattern, string Serial, string createby, DateTime? FromDate, DateTime? ToDate, InvoiceStatus Status, string nameCus, string code, string CodeTax, InvoiceType typeInvoice, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> SearchByStatus(int ComId, string Pattern, string Serial, string createby, DateTime? FromDate, DateTime? ToDate, string nameCus, string code, string CodeTax, InvoiceType typeInvoice, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> SearchInvoice(int ComId, string Pattern, string Serial, string Branchcode, string transno, string createby, string cusname, string cuscode, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> SearchPublish(int ComId, string Pattern, string Serial, DateTime? FromDate, DateTime? ToDate, string cusName, string cusCode, int pageIndex, int pageSize, out int totalRecords, params InvoiceStatus[] Status)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> SearchByConvertStatus(int ComID, string Pattern, string Serial, bool Converted, DateTime? FromDate, DateTime? ToDate, string cusName, string cusCode, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override string ConvertForVerify(IInvoice inv, string converter, out string ErrorMessage)
        {
            throw new NotImplementedException();
        }

        public override string ConvertForStore(IInvoice inv, string converter, out string ErrorMessage)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> SearchPayment(int ComId, string Pattern, string Serial, string CusName, string CusCode, DateTime? FromDate, DateTime? ToDate, int pageIndex, int pageSize, out int totalRecords, params Payment[] PaymentStatus)
        {
            throw new NotImplementedException();
        }

        public override bool ConfirmPayment(IList<IInvoice> lstInvoice, string noteappends)
        {
            throw new NotImplementedException();
        }

        public override bool ConfirmPayment(int[] ids, string noteappends)
        {
            throw new NotImplementedException();
        }

        public override bool UnConfirmPayment(IList<IInvoice> lstInvoice)
        {
            throw new NotImplementedException();
        }

        public override bool CreateInvoice(IList<ProductInv> LstProductInv, IInvoice NewInvoice, out string ErrorMessage)
        {
            throw new NotImplementedException();
        }

        public override bool CreateInvoice(IList<IInvoice> invoices, out string ErrorMessage)
        {
            throw new NotImplementedException();
        }

        public override bool UpdateInvoice(IList<ProductInv> LstProductInv, IInvoice OInvoice, out string ErrorMessage)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteInvoice(IInvoice OInvoice, out string ErrorMessage)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> GetAllbyFkeys(int ComID, string[] fkeys)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> GetByID(int ComId, int[] InvoiceIDs)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> GetListByNo(int ComId, string pattern, string serial, decimal invNo)
        {
            throw new NotImplementedException();
        }

        public override IInvoice GetByNo(int ComId, string Pattern, string Serial, decimal InvNo)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> GetByNo(int ComId, string Pattern, string Serial, decimal[] InvNo)
        {
            throw new NotImplementedException();
        }

        public override IInvoice GetByID(int comId, string pattern, int id)
        {
            throw new NotImplementedException();
        }

        public override IInvoice GetByFkey(int ComID, string Fkey)
        {
            throw new NotImplementedException();
        }

        public override IInvoice GetInvByFkey(int ComID, string Fkey, InvoiceStatus status = InvoiceStatus.NewInv)
        {
            throw new NotImplementedException();
        }

        public override IList<IInvoice> GetByFkey(int ComID, string[] Fkey)
        {
            throw new NotImplementedException();
        }

        //public override IList<IInvoice> GetByID<IInvoice>(int ComId, int[] InvoiceIDs)
        //{
        //    var query = from inv in _C_Context.InvoiceVAT
        //                where InvoiceIDs.Contains(inv.Id) && inv.ComID == ComId
        //                select (IInvoice)inv;
        //    else return query.Where(x => x.No == 0).ToList();
        //}

        public IList<IInvoice> GetByID(int ComId, int[] InvoiceIDs, bool IsRelease)
        {
            //IList<IInvoice> lstInv = (from inv in Query where InvoiceIDs.Contains(inv.id) && inv.ComID == ComId select (IInvoice)inv).ToList();
            //return lstInv;


            var query = from inv in _C_Context.InvoiceVAT
                        where InvoiceIDs.Contains(inv.Id) && inv.ComID == ComId
                        select (IInvoice)inv;
            if (IsRelease) return query.Where(x => x.No > 0).ToList();
            else return query.Where(x => x.No == 0).ToList();
        }

        public override IList<T> GetByID<T>(int ComId, int[] InvoiceIDs)
        {
            throw new NotImplementedException();
        }
    }
}
