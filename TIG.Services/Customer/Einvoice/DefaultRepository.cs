﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using TIG.Core.Entities.Customer;

namespace TIG.Services.Customer.Einvoice
{
    /// <summary>
    /// DefaultRepository store InvoiceData to Data Field of Table Invoice in database, format of data is XML string
    /// </summary>
    public class DefaultRepository : IRepositoryINV
    {
        private readonly IGetInvData _GetInvData;
        public DefaultRepository(IGetInvData IGetInvData)
        {
            _GetInvData = IGetInvData;
        }

        public string Store(byte[] INVData, C_VATInvoice inv)
        {
            XmlDocument xd = new XmlDocument();
            xd.XmlResolver = null;
            xd.PreserveWhitespace = true;
            xd.LoadXml(Encoding.UTF8.GetString(INVData));

            if (xd.GetElementsByTagName("ds:Signature")[0] != null)
                xd.GetElementsByTagName("ds:X509Certificate")[0].InnerText = new X509Certificate2(Convert.FromBase64String(xd.GetElementsByTagName("ds:X509Certificate")[0].InnerText)).SerialNumber;
            if (xd.GetElementsByTagName("ds:Signature")[1] != null)
                xd.GetElementsByTagName("ds:X509Certificate")[1].InnerText = new X509Certificate2(Convert.FromBase64String(xd.GetElementsByTagName("ds:X509Certificate")[1].InnerText)).SerialNumber;
            inv.Data = xd.OuterXml;
            return inv.Data;
        }
        public byte[] GetData(C_VATInvoice inv)
        {
            return _GetInvData.GetData(inv.Data);
        }
    }

    /// <summary>
    /// interface cho việc lưu trữ hóa đơn
    /// </summary>
    public interface IRepositoryINV
    {
        /// <summary>
        /// lưu trữ dữ liệu hóa đơn điện tử sau khi nó được tạo ra. phải truyền thêm tham số inv để biết được dữ liệu này của hóa đơn nào để lưu trữ vào vị trí hợp lý.
        /// </summary>
        /// <param name="INVData">Dữ liệu hóa đơn hoàn chỉnh</param>
        /// <param name="inv">hóa đơn được lưu trữ</param>
        /// <returns>trả về chuỗi định danh vị trí lưu trữ, mục đích để lưu vào trường data của inv</returns>
        string Store(byte[] INVData, C_VATInvoice inv);
        /// <summary>
        /// Lấy dữ liệu hóa đơn từ kho lưu trữ của hóa đơn inv
        /// </summary>
        /// <param name="inv">hóa đơn cần lấy dữ liệu</param>
        /// <returns>Hóa đơn hoàn chỉnh dưới dạng khối byte[]</returns>
        byte[] GetData(C_VATInvoice inv);
    }
}
