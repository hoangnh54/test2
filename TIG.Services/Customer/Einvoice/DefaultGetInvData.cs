﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using TIG.Services.Admin.Imp;

namespace TIG.Services.Customer.Einvoice
{
    public class DefaultGetInvData : IGetInvData
    {
        public ICertificateServices _CertificateService;

        public DefaultGetInvData(ICertificateServices CertificateService)
        {
            _CertificateService = CertificateService;
        }

        public byte[] GetData(string data)
        {
            XmlDocument xd = new XmlDocument();
            xd.XmlResolver = null;
            xd.PreserveWhitespace = true;
            if (data.StartsWith("File:"))
                xd.Load(data.Substring(data.IndexOf("File:")));
            else
                xd.LoadXml(data);
            if (xd.GetElementsByTagName("ds:Signature")[0] != null)
                xd.GetElementsByTagName("ds:X509Certificate")[0].InnerText = _CertificateService.GetCertFromSerial(xd.GetElementsByTagName("ds:X509Certificate")[0].InnerText).Cert;
            if (xd.GetElementsByTagName("ds:Signature")[1] != null)
                xd.GetElementsByTagName("ds:X509Certificate")[1].InnerText = _CertificateService.GetCertFromSerial(xd.GetElementsByTagName("ds:X509Certificate")[1].InnerText).Cert;

            //if (xd.GetElementsByTagName("Signature")[0] != null)
            //    xd.GetElementsByTagName("X509Certificate")[0].InnerText = iCert.GetCertFromSerial(xd.GetElementsByTagName("X509Certificate")[0].InnerText).Cert;
            //if (xd.GetElementsByTagName("Signature")[1] != null)
            //    xd.GetElementsByTagName("X509Certificate")[1].InnerText = iCert.GetCertFromSerial(xd.GetElementsByTagName("X509Certificate")[1].InnerText).Cert;

            System.Text.UTF8Encoding _encoding = new System.Text.UTF8Encoding();
            return _encoding.GetBytes(xd.OuterXml);
        }
    }

    public interface IGetInvData
    {
        byte[] GetData(string data);
    }
}
