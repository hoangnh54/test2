﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using TIG.Core.Entities.Customer;
using TIG.Core.Entities.Customer.Models;
using TIG.Services.Customer.Imp;

namespace TIG.Services.Customer.Einvoice
{
    public class ExtendsVATInvoiceServices : InvoiceBaseService<ExtendsVATInvoice>
    {
        private readonly ILogger<ExtendsVATInvoiceServices> _logger;
        private readonly C_Context _C_Context;

        public ExtendsVATInvoiceServices(ILogger<InvoiceVATServices> logger, C_Context C_Context)
            : base(logger)
        {
            _logger = logger;
            _C_Context = C_Context;
        }

        public override IList<InvStatement> GetReportDetail(int comId, string pattern, int year, int month)
        {
            List<InvStatement> listInvStatement = new List<InvStatement>();
            //danh sách các báo cáo không thuế
            var report0vat = from item in GetQuery<ExtendsVATInvoice>()
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.GrossValue > 0
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     Grossvalue = item.GrossValue,
                                     VatRate = -1,
                                     Note = item.Note
                                 }
                                 );
            listInvStatement.AddRange(report0vat.ToList());
            //danh sách các báo cáo thuế 0%
            var reportvat0 = from item in GetQuery<ExtendsVATInvoice>()
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 (item.GrossValue0 > 0 || item.VatAmount0 > 0)
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount0,
                                     Grossvalue = item.GrossValue0,
                                     VatRate = 0,
                                     Note = item.Note
                                 }
                                 );
            listInvStatement.AddRange(reportvat0.ToList());
            //danh sách các báo cáo thuế 5%
            var reportvat5 = from item in GetQuery<ExtendsVATInvoice>()
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 (item.GrossValue5 > 0 || item.VatAmount5 > 0)
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount5,
                                     Grossvalue = item.GrossValue5,
                                     VatRate = 5,
                                     Note = item.Note
                                 }
                                 );
            listInvStatement.AddRange(reportvat5.ToList());
            //danh sách các báo cáo thuế 10%
            var reportvat10 = from item in GetQuery<ExtendsVATInvoice>()
                              where item.ComID == comId && item.Pattern == pattern &&
                                  item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                                  item.Status != InvoiceStatus.NewInv &&
                                  (item.GrossValue10 > 0 || item.VatAmount10 > 0)
                              select (
                                  new InvStatement
                                  {
                                      kyHieuMauHDon = item.Pattern,
                                      kyHieuHDon = item.Serial,
                                      status = item.Status,
                                      type = item.Type,
                                      soHoaDon = item.No,
                                      ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                      tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                      masoThueNguoiMua = item.CusTaxCode,
                                      VATAmount = item.VatAmount10,
                                      Grossvalue = item.GrossValue10,
                                      VatRate = 10,
                                      Note = item.Note
                                  }
                                  );
            listInvStatement.AddRange(reportvat10.ToList());
            return listInvStatement;
        }

        public override IList<InvStatement> GetReportDetailQuarter(int comId, string pattern, int year, int quarter)
        {
            DateTime FirstDate = new DateTime();
            DateTime LastDate = new DateTime();
            if (quarter == 1)
            {
                FirstDate = DateTime.ParseExact("01/01/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("31/03/" + year, "dd/MM/yyyy", null);
            }
            else if (quarter == 2)
            {
                FirstDate = DateTime.ParseExact("01/04/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("30/06/" + year, "dd/MM/yyyy", null);
            }
            else if (quarter == 3)
            {
                FirstDate = DateTime.ParseExact("01/07/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("30/09/" + year, "dd/MM/yyyy", null);
            }
            else if (quarter == 4)
            {
                FirstDate = DateTime.ParseExact("01/10/" + year, "dd/MM/yyyy", null);
                LastDate = DateTime.ParseExact("31/12/" + year, "dd/MM/yyyy", null);
            }

            //khai bao danh sách báo cáo 
            List<InvStatement> listInvStatement = new List<InvStatement>();
            //danh sách các báo cáo không thuế
            var report0vat = from item in GetQuery<ExtendsVATInvoice>()
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 item.GrossValue > 0
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     //VATAmount = item.VATAmount,
                                     Grossvalue = item.GrossValue,
                                     VatRate = -1,
                                     Note = item.Note
                                 }
                                 );
            listInvStatement.AddRange(report0vat.ToList());
            //danh sách các báo cáo thuế 0%
            var reportvat0 = from item in GetQuery<ExtendsVATInvoice>()
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 (item.GrossValue0 > 0 || item.VatAmount0 > 0)
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount0,
                                     Grossvalue = item.GrossValue0,
                                     VatRate = 0,
                                     Note = item.Note
                                 }
                                 );
            listInvStatement.AddRange(reportvat0.ToList());
            //danh sách các báo cáo thuế 5%
            var reportvat5 = from item in GetQuery<ExtendsVATInvoice>()
                             where item.ComID == comId && item.Pattern == pattern &&
                                 item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                 item.Status != InvoiceStatus.NewInv &&
                                 (item.GrossValue5 > 0 || item.VatAmount5 > 0)
                             select (
                                 new InvStatement
                                 {
                                     kyHieuMauHDon = item.Pattern,
                                     kyHieuHDon = item.Serial,
                                     status = item.Status,
                                     type = item.Type,
                                     soHoaDon = item.No,
                                     ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                     tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                     masoThueNguoiMua = item.CusTaxCode,
                                     VATAmount = item.VatAmount5,
                                     Grossvalue = item.GrossValue5,
                                     VatRate = 5,
                                     Note = item.Note
                                 }
                                 );
            listInvStatement.AddRange(reportvat5.ToList());
            //danh sách các báo cáo thuế 10%
            var reportvat10 = from item in GetQuery<ExtendsVATInvoice>()
                              where item.ComID == comId && item.Pattern == pattern &&
                                  item.ArisingDate >= FirstDate && (item.ArisingDate < LastDate.AddDays(1) || item.ArisingDate == LastDate) && item.ArisingDate.Year == year &&
                                  item.Status != InvoiceStatus.NewInv &&
                                  (item.GrossValue10 > 0 || item.VatAmount10 > 0)
                              select (
                                  new InvStatement
                                  {
                                      kyHieuMauHDon = item.Pattern,
                                      kyHieuHDon = item.Serial,
                                      status = item.Status,
                                      type = item.Type,
                                      soHoaDon = item.No,
                                      ngayLapHD = item.ArisingDate.ToString("dd/MM/yyyy"),
                                      tenNguoiMua = !string.IsNullOrEmpty(item.CusName) ? item.CusName : item.Buyer,
                                      masoThueNguoiMua = item.CusTaxCode,
                                      VATAmount = item.VatAmount10,
                                      Grossvalue = item.GrossValue10,
                                      VatRate = 10,
                                      Note = item.Note
                                  }
                                  );
            listInvStatement.AddRange(reportvat10.ToList());
            return listInvStatement;
        }

        public override IList<InvReportTax> GetReportDetailTax(int comId, string pattern, int year, int month)
        {
            IList<InvReportTax> listinv = new List<InvReportTax>();
            try
            {
                IProductInvService _productSer = ServiceFactory.Resolve<IProductInvService>();
                listinv = (from item in IQuery<ExtendsVATInvoice>()
                           join itempro in _productSer.Query on item.id equals itempro.InvID
                           where item.ComID == comId && item.Pattern == pattern &&
                               item.PublishDate.Month == month && item.PublishDate.Year == year &&
                               item.Status != InvoiceStatus.NewInv
                           select (
                               new InvReportTax
                               {
                                   id = item.id,
                                   Status = item.Status,
                                   Type = item.Type,
                                   IsSum = itempro.IsSum,
                                   Pattern = item.Pattern,
                                   Serial = item.Serial,
                                   No = item.No,
                                   InvoiceDate = item.ArisingDate,
                                   CusName = item.CusName,
                                   CusTaxCode = item.CusTaxCode,
                                   ProdName = itempro.Name,
                                   ProdTotal = itempro.Total,
                                   ProdVatAmount = itempro.VATAmount,
                                   VATRate = itempro.VATRate,
                               }
                               )).OrderBy(p => p.No).ToList();
            }
            catch (Exception ex) { log.Error(ex); }
            return listinv;
        }

        public override IList<TongHopBanHang> ReportProductGeneral(int comId, string pattern, int year, int month)
        {
            IList<TongHopBanHang> listinv = new List<TongHopBanHang>();
            try
            {
                IProductInvService _productSer = ServiceFactory.Resolve<IProductInvService>();
                listinv = (from item in IQuery<ExtendsVATInvoice>()
                           join itempro in _productSer.Query on item.id equals itempro.InvID
                           where item.ComID == comId && item.Pattern == pattern &&
                               item.PublishDate.Month == month && item.PublishDate.Year == year &&
                               item.Status != InvoiceStatus.NewInv
                           select (
                               new TongHopBanHang
                               {
                                   id = item.id,
                                   Type = item.Type,
                                   Status = item.Status,
                                   IsSum = itempro.IsSum,
                                   MaHang = itempro.Code,
                                   TenHang = itempro.Name,
                                   DonVi = itempro.Unit,
                                   SoLuong = itempro.Quantity,
                                   DonGia = itempro.Price,
                                   ProdTotal = itempro.Total,
                                   ProdVatAmount = itempro.VATAmount,
                                   ProdAmount = itempro.Amount,
                                   VATRate = itempro.VATRate,
                               }
                               )).OrderBy(item => item.MaHang).ToList();
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return listinv;
        }

        public override IList<ChiTietBanHang> ReportProductDetails(int comId, string serial, string pattern, int year, int month)
        {
            IList<ChiTietBanHang> listinv = new List<ChiTietBanHang>();
            try
            {
                IProductInvService _productSer = ServiceFactory.Resolve<IProductInvService>();
                listinv = (from item in IQuery<ExtendsVATInvoice>()
                           join itempro in _productSer.Query on item.id equals itempro.InvID
                           where item.ComID == comId && item.Pattern == pattern && item.Serial == serial &&
                               item.ArisingDate.Month == month && item.ArisingDate.Year == year &&
                               item.Status != InvoiceStatus.NewInv
                           select (
                               new ChiTietBanHang
                               {
                                   id = item.id,
                                   Status = item.Status,
                                   Type = item.Type,
                                   IsSum = itempro.IsSum,
                                   Pattern = item.Pattern,
                                   Serial = item.Serial,
                                   No = item.No,
                                   InvoiceDate = item.ArisingDate,
                                   Buyer = item.Buyer,
                                   CusName = item.CusName,
                                   CusTaxCode = item.CusTaxCode,
                                   MaHang = itempro.Code,
                                   TenHang = itempro.Name,
                                   DonGia = itempro.Price,
                                   SoLuong = itempro.Quantity,
                                   DonVi = itempro.Unit,
                                   ProdTotal = itempro.Total,
                                   ProdVatAmount = itempro.VATAmount,
                                   VATRate = item.VATRate,
                                   ProdAmount = itempro.Amount,
                                   VATAmount = item.VATAmount
                               }
                               )).OrderBy(item => item.No).ToList();

            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return listinv;
        }
    }
}
